
#ifndef __Ucosdriver_H
#define __Ucosdriver_H

#ifdef __cplusplus 
extern "C"
{
#endif

#if defined WIN32 || defined _WIN32 
#include "../sys/pc_type.h"
#else
#include "stm32f10x.h"
#endif



#ifdef __cplusplus 
}
#endif



#endif
