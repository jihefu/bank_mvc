#include "ucos.h"



unsigned char OSTCBCur;

unsigned char  os_task_index = 0;
OSTCB_TypeDef ucos_task[config_max_tasks];



void os_init()
{


}



unsigned char OSTaskCreate(void (*task)(), void* task_args)
{
	unsigned char taskinx = os_task_index++;
	ucos_task[taskinx].task = task;
	ucos_task[taskinx].one.enable = 1;
	ucos_task[taskinx].indata = 0;
	ucos_task[taskinx].outdata = 0;
	return taskinx;
}



void OSTimeTick()
{
	for (int i = 0; i < os_task_index; i++) {
		if (ucos_task[i].delay == 0) ucos_task[i].one.rdy = 1;
		else ucos_task[i].delay--;
		if (ucos_task[i].one.rdy && ucos_task[i].one.enable)
		{
			OSTCBCur = i;
			ucos_task[i].task();
		}
	}

}


void OSTimeDly(unsigned char timeTick) {
	ucos_task[OSTCBCur].one.rdy = 0;
	ucos_task[OSTCBCur].delay = timeTick - 1;

}