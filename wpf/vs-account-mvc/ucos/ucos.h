#ifndef __Ucos_H
#define __Ucos_H

#ifdef __cplusplus 



extern "C"
{
#endif

#include <stdint.h>
#define config_max_tasks 5

#define  WHILE(a) 

	void   OSTimeTick();
	unsigned char    OSTaskCreate(void (*task)(), void* task_args);
	void   OSTimeDly(unsigned char timeTick);



	typedef struct
	{
		struct struct_tcb
		{
			unsigned rdy : 1;     //就绪
			unsigned enable : 1;  //启用禁用任务
		}one;
		unsigned char delay;  //延时计数
		unsigned char step; //状态机步骤
		uint32_t indata; //输入数据
		uint32_t outdata; //输出数据
		void  (*task)();
	}OSTCB_TypeDef;


	extern unsigned char OSTCBCur;
	extern OSTCB_TypeDef ucos_task[config_max_tasks];


#ifdef __cplusplus 
}
#endif



#endif

