#include "stmflash.h"





u16 STMFLASH_ReadHalfWord(u32 faddr)
{
	
	return 0;
}



void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)	
{
	u16 i;
	for (i = 0; i < NumToWrite; i++)
	{
		set_ecbm_modbus_pmd_flash(WriteAddr, pBuffer[i]);
		WriteAddr += 1;
	}
}


//从指定地址开始读出指定长度的数据
//ReadAddr:起始地址
//pBuffer:数据指针
//NumToWrite:半字(16位)数
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)   	
{
	u16 i;
	for(i=0;i<NumToRead;i++)
	{
		pBuffer[i]=STMFLASH_ReadHalfWord(ReadAddr);//读取2个字节.
		ReadAddr+=1;//偏移2个字节.	
	}
}

//////////////////////////////////////////测试用///////////////////////////////////////////
//WriteAddr:起始地址
//WriteData:要写入的数据
void Test_Write(u32 WriteAddr,u16 WriteData)   	
{
	STMFLASH_Write(WriteAddr,&WriteData,1);//写入一个字 
}



u8 STMFLASH_PMD_READ_U16(u16 addr, u16* dat){
	u32 addr32=(u32)(addr);
	u16 data_u16 = STMFLASH_ReadHalfWord(addr32);
	*dat= data_u16;
	return 0;
}


void STMFLASH_PMD_WRITE_U16(u16 addr, u16 dat){
	  u32 addr32=(u32)(FLASH_SAVE_ADDR+addr);
		STMFLASH_Write(addr32,&dat,1);
}


void STMFLASH_PMD_READ(u16 addr,u16 *pBuffer,u16 NumToRead)   	
{
	u16 i;
	u32 addr32=(u32)(FLASH_SAVE_ADDR+addr);
	for(i=0;i<NumToRead;i++)
	{
		pBuffer[i]=STMFLASH_ReadHalfWord(addr32);//读取2个字节.
		addr32+=1;//偏移2个字节.	
	}
}












