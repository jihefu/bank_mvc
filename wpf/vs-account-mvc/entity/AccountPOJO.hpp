#pragma once
#include <string>
#include <iostream>
#include "../common/BasePOJO.hpp"
using namespace std;


class AccountQuery {
public:
	string username;
};



struct AccountPOJO : BasePOJO
{
	string username;
	string password;
	uint32_t balance;
};

