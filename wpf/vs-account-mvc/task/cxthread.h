#ifndef CXTHREAD_H_INC
#define CXTHREAD_H_INC

#define MSOUT_IN_UI			500
#define MSOUT_IN_RT			10

#include <windows.h>

// 多任务间互锁关键资源
class CxLock {
protected:
	HANDLE	m_hMutex;

public:
	CxLock(void);
	virtual ~CxLock();

	virtual BOOL WaitFor(DWORD msout);
	virtual void Release(void);
};

// 多任务关键区，无条件互锁
class CxSection {
protected:
	CRITICAL_SECTION m_cs;

public:
	CxSection(void);
	virtual ~CxSection();

	virtual void Enter(void);
	virtual void Leave(void);
};

// 多对1跨任务调用
class CxCall {
protected:
	HANDLE	m_hCallSem;
	WORD	m_cmdId;					// 调用命令号
	//...其它调用参数

	HANDLE	m_hRetSem;
	DWORD	m_ret;						// 返回值，可能是DVB码，32位句柄/时间/计数/掩码
	//...其它返回参数

public:
	CxCall(void);
	virtual ~CxCall();

	virtual BOOL WaitLastReturn(DWORD *pRet, DWORD msout=100);	// 发起调用方
	virtual void Post(WORD cmdId);				// 发起调用方，如果有参数，需要扩展此函数
	virtual BOOL Received(WORD *pCmdId);		// 执行方
	virtual void PostReturn(DWORD dvRet);		// 执行方，如果带参数，需要扩展此函数
};

// CxThread持续运行
// 建议构造后Start()，析构前PostStopSignal()并检查IsActive
class CxThread {
private:
	static DWORD WINAPI SysThreadRun(LPVOID lpParam);
protected:
	HANDLE	m_hSysThread;				// 对应的系统线程句柄，委托运行
	BOOL	m_running;
	BOOL	m_stopSignal;
	DWORD   m_dwParam;

public:
	CxThread(void);
	virtual ~CxThread();

	virtual BOOL IsActive(void) {return(m_running);}
	virtual void Start(void);
	virtual void PostStopSignal(void);

	// 被线程系统调用
	virtual void Run(void);				// 必须重载实现，参考本类实现框架
public:
	virtual void SetThreadData(DWORD dwParam);//额外参数
	virtual DWORD GetThreadData();
};

//#ifdef USE_CXTASK // ----------------------------------------------------------
// CxTask周期运行
// 任何CxTask类(适配器形式)可以随时添加到RealTimer来启动周期分时任务，也可以随时移除
// 建议在在CxTask对象构造后添加，析构前移除
#ifndef RT_DELAY_MS
#define RT_DELAY_MS		1
#endif

class CxTask {
public:
	virtual void OnTick(DWORD ms) = 0;
};

void RealTimerStart(int intv);			// 特殊intv==0，表示接力模式运行
DWORD RealTimerGetRunMs(void);
BOOL RealTimerAddTask(CxTask *pTask);
BOOL RealTimerRemoveTask(CxTask *pTask);
void RealTimerQuit(DWORD msout);
//#endif // USE_CXTASK ----------------------------------------------------------

#endif 
