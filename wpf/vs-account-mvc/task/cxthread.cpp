#include "cxthread.h"
#include <mmsystem.h>
#include <process.h>
#include <stdlib.h>

CxLock::CxLock(void)
{
	m_hMutex = CreateMutex(NULL, FALSE, NULL);
}

CxLock::~CxLock()
{
	CloseHandle(m_hMutex);
}

BOOL CxLock::WaitFor(DWORD msout)
{
	return((WaitForSingleObject(m_hMutex, msout) == WAIT_OBJECT_0));
}

void CxLock::Release(void)
{
	ReleaseMutex(m_hMutex);
}

CxSection::CxSection(void)
{
	InitializeCriticalSection(&m_cs);
}

CxSection::~CxSection()
{
	DeleteCriticalSection(&m_cs);
}

void CxSection::Enter(void)
{
	EnterCriticalSection(&m_cs);
}

void CxSection::Leave(void)
{
	LeaveCriticalSection(&m_cs);;
}

CxCall::CxCall(void)
{
	m_hCallSem = CreateSemaphore(NULL, 0, 1, NULL);
	m_cmdId = 0x0000;

	m_hRetSem = CreateSemaphore(NULL, 1, 1, NULL);
	m_ret = 0;
}

CxCall::~CxCall()
{
	CloseHandle(m_hCallSem);
	CloseHandle(m_hRetSem);
}


BOOL CxCall::WaitLastReturn(DWORD* pRet, DWORD msout)
{
	if (WaitForSingleObject(m_hRetSem, msout) == WAIT_OBJECT_0) {
		if (pRet != NULL) {
			*pRet = m_ret;
		}
		return(TRUE);
	}
	else {
		return(FALSE);
	}
}

void CxCall::Post(WORD cmdId)
{
	m_cmdId = cmdId;
	ReleaseSemaphore(m_hCallSem, 1, NULL);
}


BOOL CxCall::Received(WORD* pCmdId)
{
	if (WaitForSingleObject(m_hCallSem, 0) == WAIT_OBJECT_0) {
		*pCmdId = m_cmdId;
		return(TRUE);
	}
	else {
		return(FALSE);
	}
}

void CxCall::PostReturn(DWORD dvRet)
{
	m_ret = dvRet;
	ReleaseSemaphore(m_hRetSem, 1, NULL);
}

DWORD WINAPI CxThread::SysThreadRun(LPVOID lpParam)
{
	CxThread* pCxThread = (CxThread*)lpParam;
	pCxThread->Run();
	return(0);
}

CxThread::CxThread(void)
{
	m_hSysThread = 0;
	m_running = FALSE;
	m_stopSignal = FALSE;
	m_dwParam = 0;
}

CxThread::~CxThread()
{
	if (m_running) {						// 万一线程没有自行终止，那么强行中止
		TerminateThread(m_hSysThread, 0);
		m_running = FALSE;
	}
	if (m_hSysThread != 0) {
		CloseHandle(m_hSysThread);
	}
}

void CxThread::Start(void)
{
	if (m_hSysThread == 0) {
		DWORD dwThreadId;
		m_hSysThread = CreateThread(NULL, 0, SysThreadRun, this, 0, &dwThreadId);
	}
}

void CxThread::PostStopSignal(void)
{
	m_stopSignal = TRUE;
}

void CxThread::Run(void)
{
	m_running = TRUE;					// 必须第一行
	while (TRUE) {
		Sleep(10);						// 必须间歇一会

		if (m_stopSignal) {				// break the while loop，自然终结Run
			m_stopSignal = FALSE;
			break;
		}
	}
	m_running = FALSE;					// 必须最后一行
}



void CxThread::SetThreadData(DWORD dwParam)
{
	if (m_dwParam != dwParam)
	{
		m_dwParam = dwParam;
	}
}


DWORD CxThread::GetThreadData()
{
	return m_dwParam;
}


//#ifdef USE_CXTASK // ----------------------------------------------------------

// ------RealTimer实现所需静态变量开始------
static BOOL		_inService = FALSE;
static MMRESULT	_hRealTimer = 0;
static BOOL		_bRunInitted = FALSE;
static DWORD	_runMs0 = 0;
static BOOL		_relayMode = FALSE;		// 多个子任务接力模式轮流运行
static HANDLE	_hListLock = NULL;
static CxTask* _taskList[32];			// 顺序排列
static DWORD	_taskNum = 0;
static DWORD	_relayIndex = 0;		// 接力模式下当前棒
static BOOL		_rtIdle = FALSE;
static BOOL		_rtIdleSignal = FALSE;
// ------RealTimer实现所需静态变量结束------

static void CALLBACK RealTimerExecute(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2)
{

	if (!_bRunInitted) {
		_bRunInitted = TRUE;
		//SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
		_runMs0 = timeGetTime();
	}

	if (_rtIdleSignal) {
		_rtIdleSignal = FALSE;
		_rtIdle = TRUE;
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_IDLE);
	}

	if (_rtIdle) {						// 空闲状态啥也不干
		return;
	}

	DWORD runMs = RealTimerGetRunMs();
	DWORD i;
	if (WaitForSingleObject(_hListLock, RT_DELAY_MS) == WAIT_OBJECT_0) {
		if (_relayMode) {
			if (_taskNum >= 1) {
				_taskList[_relayIndex]->OnTick(runMs);
				if (_relayIndex == _taskNum - 1) {
					_relayIndex = 0;
				}
				else {
					_relayIndex++;
				}
			}
		}
		else {
			for (i = 0; i < _taskNum; i++) {
				_taskList[i]->OnTick(runMs);
			}
		}
		ReleaseMutex(_hListLock);
	}
}

void RealTimerStart(int intv)
{
	if (!_inService) {
		if (intv == 0) {
			_relayMode = TRUE;
			_hRealTimer = timeSetEvent(1, 0, RealTimerExecute, 0, TIME_PERIODIC);
		}
		else {
			_relayMode = FALSE;
			_hRealTimer = timeSetEvent(intv, 0, RealTimerExecute, 0, TIME_PERIODIC);
		}
		timeBeginPeriod(RT_DELAY_MS);

		_hListLock = CreateMutexA(NULL, FALSE, "TaskListMutex");
		_taskNum = 0;
		_relayIndex = 0;

		_inService = TRUE;
	}
}

DWORD RealTimerGetRunMs(void)
{
	return(timeGetTime() - _runMs0);
}

BOOL RealTimerAddTask(CxTask* pTask)
{
	if (!_inService) {
		return(FALSE);
	}

	BOOL added = FALSE;
	if (WaitForSingleObject(_hListLock, 200) == WAIT_OBJECT_0) {
		if (_taskNum < 32) {
			_taskList[_taskNum] = pTask;
			_taskNum++;
			added = TRUE;
		}
		ReleaseMutex(_hListLock);
	}
	return(added);
}

BOOL RealTimerRemoveTask(CxTask* pTask)
{
	if (!_inService) {
		return(FALSE);
	}

	BOOL removed = FALSE;
	DWORD i;
	if (WaitForSingleObject(_hListLock, 200) == WAIT_OBJECT_0) {
		for (i = 0; i < _taskNum; i++) {
			if (removed) {				// 如果已经发生了移除，后面往前移
				_taskList[i - 1] = _taskList[i];
			}
			else if (_taskList[i] == pTask) {
				removed = TRUE;
			}
		}
		if (removed) {
			_taskNum--;
			if (_relayIndex >= _taskNum) {
				_relayIndex = 0;
			}
		}
		ReleaseMutex(_hListLock);
	}
	return(removed);
}

void RealTimerQuit(DWORD msout)
{
	if (_inService) {
		_inService = FALSE;

		_rtIdleSignal = TRUE;			// 发出空闲申请信号
		timeEndPeriod(RT_DELAY_MS);
		for (DWORD i = 0; i < msout / 10; i++) {
			Sleep(10);
			if (_rtIdle) {
				break;
			}
		}
		timeKillEvent(_hRealTimer);
		_hRealTimer = 0;

		_relayIndex = 0;
		_taskNum = 0;
		CloseHandle(_hListLock);
		_hListLock = NULL;
	}
}

//#endif // USE_CXTASK ----------------------------------------------------------