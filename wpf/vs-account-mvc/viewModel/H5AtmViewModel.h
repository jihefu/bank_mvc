/**
 * @file H5AtmViewModel.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-11-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "../model/Bank.h"
#include "../common/KeyEvent.hpp"
#include <iostream>
using namespace std;

/**
 * @brief 浏览器view
 * 
 */
class H5AtmViewModel :public Object {
protected:
	uint32_t	m_money;  /*!< 展示的余额 */
	Bank* m_Bank; /*!< 引用的银行账户 */
public:
    /**
     * @brief 关联账户
     * 
     * @param acount 
     */
	void setBank(Bank* Bank);
	H5AtmViewModel() {}
	/**
	 * @brief 处理来自账户的事件
	 * 
	 * @param evt 
	 * @return true  success
	 * @return false  fail
	 */
	bool onAccountEvent(AcountEventArgs evt);
	/**
	 * @brief 处理来自网页的事件
	 * 
	 * @param evt 
	 * @return true   success
	 * @return false  fail
	 */
	bool onH5Event(KeyEventArgs evt);
	/**
	 * @brief 浏览器渲染
	 * 
	 */
	void render();
};