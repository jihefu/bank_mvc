#include "../model/Bank.h"
#include "H5AtmViewModel.h"
#include <nlohmann/json.hpp>   
#include "Webserver.h"

using json = nlohmann::json;

void H5AtmViewModel::setBank(Bank* Bank) {
	m_Bank = Bank;
	//账户事件注册回调
	m_Bank->m_Event.addEventListener(this, &H5AtmViewModel::onAccountEvent);
	render();
}



void H5AtmViewModel::render() {
	json resJson;
	resJson["method"] = "moneyChange";
	resJson["param"] = m_money;
	string s = resJson.dump();
	sseSend(s.c_str());
}



bool H5AtmViewModel::onAccountEvent(AcountEventArgs evt) {

	switch (evt.mType) {
		case MONEY_CHANGE: {
			m_money = evt.mVal;
			render();
			break;
		}
	}
	return true;
}


bool H5AtmViewModel::onH5Event(KeyEventArgs evt) {

	switch (evt.mType)
	{
		case KEY_EVENT_CLICK: {
			if (evt.mVal == KEY_VAL_KEY1) {
				AccountServerMsg req = AccountServerMsg(evt.mId, AccountServerType::ACCOUNTSERVER_REQ_PLUS, 1);
				m_Bank->m_accountServerQueue.push(req);
			}
			else
			{
				AccountServerMsg req = AccountServerMsg(evt.mId, AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT, 1);
				m_Bank->m_accountServerQueue.push(req);
			}
			AccountServerMsg req = AccountServerMsg(evt.mId, AccountServerType::ACCOUNTSERVER_REQ_QUIT, 0);
			m_Bank->m_accountServerQueue.push(req);
			break;
		}
		default:
			break;
	}
	return true;
}