#include "../model/Bank.h"
#include "LcdAtmViewModel.h"
#include "user_lcd.h" 

using namespace std;

extern uint32_t cur_accountId;

 void LcdAtmViewModel::setBank(Bank* Bank) {
	 m_Bank = Bank;
	//银行事件注册回调
	 m_Bank->m_Event.addEventListener(this, &LcdAtmViewModel::onAccountEvent);
	 render();
}



void LcdAtmViewModel::render() {
	lcdDisPlayMoney(m_money);
}



bool LcdAtmViewModel::onAccountEvent(AcountEventArgs evt) {

	switch (evt.mType) {
		case MONEY_CHANGE: {
			m_money = evt.mVal;
			render();
			break;
		}
	}
	return true;
}


bool LcdAtmViewModel::onKeyEvent(KeyEventArgs evt) {

	switch (evt.mType)
	{
		case KEY_EVENT_CLICK: {
			//存1元
			if (evt.mVal == KEY_VAL_KEY1) {
				//m_acount->plusMoney(1);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_PLUS, 1);
				m_Bank->m_accountServerQueue.push(req);
			}//取1元
			else
			{
				//	m_acount->subtractMoney(1);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT, 1);
				m_Bank->m_accountServerQueue.push(req);
			}
			break;
			break;
		}
		case KEY_EVENT_DBL_CLICK: {
			//存10元
			if (evt.mVal == KEY_VAL_KEY1) {
				//m_acount->plusMoney(10);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_PLUS, 10);
				m_Bank->m_accountServerQueue.push(req);
			}//取10元
			else
			{
				//m_acount->subtractMoney(10);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT, 10);
				m_Bank->m_accountServerQueue.push(req);
			}
			break;
		}
		case KEY_EVENT_PRESSING: {
			//存1元
			if (evt.mVal == KEY_VAL_KEY1) {
				//m_acount->plusMoney(1);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_PLUS, 1);
				m_Bank->m_accountServerQueue.push(req);
			}//取1元
			else
			{
				//m_acount->subtractMoney(1);
				AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT, 1);
				m_Bank->m_accountServerQueue.push(req);
			}
			break;

		}
		//抬起退出					     
		case KEY_EVENT_UP: {
			AccountServerMsg req = AccountServerMsg(cur_accountId,AccountServerType::ACCOUNTSERVER_REQ_QUIT, 0);
			m_Bank->m_accountServerQueue.push(req);
			break;
		}
	  default:
		break;
	}

	//cout << evt.mVal;
	return true;
}