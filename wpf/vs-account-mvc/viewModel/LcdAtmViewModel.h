/**
 * @file LcdPage.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-11-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "../model/Bank.h"
#include "../common/KeyEvent.hpp"
#include <iostream>
using namespace std;

/**
 * @brief 液晶的view
 * 
 */
class LcdAtmViewModel :public Object {
  protected:
	uint32_t	m_money;  /*!< 展示的余额 */
	Bank*   m_Bank; /*!< 引用的银行 */
 public:
    /**
     * @brief 关联银行
     * 
     * @param acount 
     */
	void setBank(Bank* Bank);


	LcdAtmViewModel(){}
	/**
	 * @brief 处理来自账户的事件
	 * 
	 * @param evt 
	 * @return true  success
	 * @return false  fail
	 */
	bool onAccountEvent(AcountEventArgs evt);
		/**
	 * @brief 处理来自按键的事件
	 * 
	 * @param evt 
	 * @return true   success
	 * @return false  fail
	 */
	bool onKeyEvent(KeyEventArgs evt);
		/**
	 * @brief lcd渲染
	 * 
	 */
	void render();
};