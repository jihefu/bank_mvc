#pragma once
#include "../entity/AccountPOJO.hpp"
#include<vector>


class AccountDao
{
    public:
       virtual  vector<AccountPOJO>  listAll(const AccountQuery& accountQuery)=0 ;
       virtual  AccountPOJO  getByUserName(const string& username) = 0;
       virtual  AccountPOJO  getByUserId(const uint32_t id) = 0;
       virtual  uint32_t  inster(const AccountPOJO& AccountPOJO) = 0;
       virtual  uint32_t  update(const AccountPOJO& AccountPOJO) = 0;
       virtual   uint32_t  getTotalMoney() = 0;

};

