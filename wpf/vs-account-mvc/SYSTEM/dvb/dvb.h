#define _CRT_SECURE_NO_WARNINGS
#if !defined(DVB_INC)
#define DVB_H_INC

/*
  Copyright (c) 2017 Langjie Testing Co. Ltd.
  本源代码知识版权属于杭州朗杰测控技术开发有限公司，www.langjie.com
  
  在满足以下条件下，朗杰公司允许任何人（开发者）免费复制到其C/C++工程，编译生成应用软件：
  1> 朗杰公司保留修改权，开发者必须完整复制。
  2> 生成可执行的应用软件必须与朗杰公司控制器一同使用。
  3> 不得擅自以源文件形式散发到第三方。即，任何开发者获得本文件必须直接来自朗杰公司。
  4> 其它编译语言版本必须经过朗杰公司测试通过并另行授权。

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include "window/windows.h"
#include "dvb_type.h"
// DVB=Display Value in BCD, 示值码
// IDR=Integer of Decimal Resolution, 分辨数
typedef DWORD DVB;
typedef long IDR;

#define NEGABIT		0x80000000	// 最高为作为负数标志
#define EODVB		0x80000000	// 负0作为DVB码串结束标志
#define IDRRANGE	10000000	// IDR范围
#define MAXIDR		(IDRRANGE-1)
#define MINIDR		(1-IDRRANGE)

#ifdef __cplusplus
extern "C"  {
#endif
	
BOOL DvbIsPositive(DWORD dvb);
BOOL DvbIsInteger(DWORD dvb);
int DotOfDvb(DWORD dvb);
long Dvb2Int(DWORD dvb);
DWORD Int2Dvb(long iv);
double Dvb2Float(DWORD dvb);
DWORD Float2Dvb(double fv, int dot);
DWORD FreeFloat2Dvb(double fv);
DWORD ValidFloat2Dvb(double fv, int wd);
DWORD DvbReform(DWORD dvb, int dot);
int DvbCompare(DWORD dvb1, DWORD dvb2);
void Dvb2String(char buf[], DWORD dvb);
DWORD String2Dvb(const char *str);
int GetDvbCount(DWORD dvbs[]);
int Dvbs2String(char buf[], DWORD dvbs[]);
int String2Dvbs(DWORD dvbs[], const char *str);

long Float2Idr(double fv, int dot);
double Idr2Float(long idr, int dot);
long Dvb2Idr(DWORD dvb);
DWORD Idr2Dvb(long idr, int dot);
DWORD DvbInc(DWORD dvb, long idr);		// idr可正负
DWORD DvbCountInc(DWORD dvbc, long step, BOOL *pbWrap);	 // DVB计数以满7位数为界绕回
long DvbMinus2Idr(DWORD dvb1, DWORD dvb2);
DWORD DvbCountMinus(DWORD dvbc1, DWORD dvbc2);
DVB DvbMin(DWORD dvb1, DWORD dvb2);		// 针对相同格式的DVB比较
DVB DvbMax(DVB dvb1, DVB dvb2);

// 16位BCD可以作为正整数的紧凑DVB
WORD Int2Bcd(int iv);
int Bcd2Int(WORD bcd);
void Bcd2String(char buf[], WORD bcd);
WORD String2Bcd(char *str);

#ifdef __cplusplus
}
#endif

#endif // DVB_H_INC 