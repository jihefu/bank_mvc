/*
  Copyright (c) 2017 Langjie Testing Co. Ltd.
  本源代码知识版权属于杭州朗杰测控技术开发有限公司，www.langjie.com

  在满足以下条件下，朗杰公司允许任何人（开发者）免费复制到其C/C++工程，编译生成应用软件：
  1> 朗杰公司保留修改权，开发者必须完整复制。
  2> 生成可执行的应用软件必须与朗杰公司控制器一同使用。
  3> 不得擅自以源文件形式散发到第三方。即，任何开发者获得本文件必须直接来自朗杰公司。
  4> 其它编译语言版本必须经过朗杰公司测试通过并另行授权。

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include "dvb.h"
#include <stdlib.h>
#include <math.h>

// 2017/04/14: 改进FreeFloat2Dvb，预放大纯小数(支持0.000001)，小数点后连续3个0截断
// 2016/05/04: Dvb2Float引用了Dvb2Int，应该先去小数点
// 2016/04/15: Dvb2Int考虑有小数点的情况
// 2016/01/07: Dvb2String特例为0时，格式出dot指定的0个数
// 2015/09/29: Dvb2String增加ABCDEF处理，可打印十六进制；增加ValidFloat2Dvb()
// 2015/08/05: IDR定义为long，去掉IdrCountInc()函数
// 2015/05/20: 自由浮点转化为DVB时+0.0005后fmod()判断0.001
// 2014/04/03: 浮点转整数时+-0.4靠近整数
// 2013/05/01: BCD码转换时强制大小
// 2011/08/18: 优化Dvb2Int(),用常量数组DECIMUL[]替代deciMul循环乘
BOOL DvbIsPositive(DWORD dvb)
{
	return((dvb & NEGABIT) == 0 ? TRUE : FALSE);
}

BOOL DvbIsInteger(DWORD dvb)
{
	return((dvb & 0x70000000) == 0 ? TRUE : FALSE);
}

int DotOfDvb(DWORD dvb)
{
	return((dvb & 0x70000000) >> 28);
}

long Dvb2Int(DWORD dvb)
{
	static long DECIMUL[] = { 1,10,100,1000,10000,100000,1000000 };
	int dot = DotOfDvb(dvb);
	if (dot != 0) {
		dvb >>= (dot * 4);
	}
	long iv = 0;
	for (int i = 0; i < (7 - dot); i++) {
		iv += (dvb & 0x0000000f) * DECIMUL[i];
		dvb >>= 4;
	}
	return((dvb & 0x00000008) ? -iv : iv);
}

DWORD Int2Dvb(long iv)
{
	BOOL isNega = (iv < 0);
	if (isNega) {
		iv = -iv;
	}
	int shift = 0;
	DWORD dvb = 0;
	for (int i = 0; i < 7; i++) {
		long deci = iv % 10;
		if (deci != 0) {
			dvb |= (deci << shift);
		}
		iv /= 10;
		shift += 4;
	}
	if (isNega) {
		dvb |= NEGABIT;
	}
	return(dvb);
}


double Dvb2Float(DWORD dvb)
{
	static double FMULTI[] = { 1.0, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001 };

	int dot = ((dvb & 0x70000000) >> 28);
	long iv = Dvb2Int(dvb & 0x8FFFFFFF);
	return(iv * FMULTI[dot]);
}

static long Round2Int(double f)
{
	if (f >= 0.0) {
		return((long)(f + 0.4));
	}
	else {
		return((long)(f - 0.4));
	}
}

DWORD Float2Dvb(double fv, int dot)
{
	static long MULTI[] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000 };

	dot = __max(0, dot);
	dot = __min(7, dot);
	double mf = fv * MULTI[dot];
	mf = __max(-9999999.0, mf);
	mf = __min(9999999.0, mf);
	DWORD dvb = Int2Dvb(Round2Int(mf));
	return(dvb | (dot << 28));
}

DWORD FreeFloat2Dvb(double fv)
{
	int dot = 0;
	double afv = fabs(fv);

	if (afv < 0.000001) {
		return(0);
	}

	// 对于纯小数，先放大一点，最大放大1000倍
	for (int i = 0; i < 3; i++) {
		if (afv > 1.0) {
			break;
		}
		afv *= 10.0;
		dot++;
	}

	while ((afv < 1000000.0) && (dot < 6)) {
		if (fmod(afv, 1.0) < 0.001) {
			break;
		}
		afv *= 10.0;
		dot++;
	}

	return(Float2Dvb(fv, dot));
}

DWORD ValidFloat2Dvb(double fv, int wd)
{
	double afv = fabs(fv);
	int intWd = 1;
	while (afv > 9.9999) {
		intWd++;
		afv /= 10;
	}

	int dot;
	if (intWd >= wd) {
		dot = 0;
	}
	else {
		dot = wd - intWd;
	}
	return(Float2Dvb(fv, dot));
}

DWORD DvbReform(DWORD dvb, int dot)
{
	dot = __max(0, dot);
	dot = __min(7, dot);
	int oldDot = DotOfDvb(dvb);
	DWORD newDvb = dvb;
	if (dot < oldDot) {
		int rightShift = 4 * (oldDot - dot);
		DWORD newDeci = (newDvb & 0x0fffffff) >> rightShift;
		newDvb = (newDvb & NEGABIT) | (dot << 28) | newDeci;
	}
	else if (dot > oldDot) {
		int leftShift = 4 * (dot - oldDot);
		DWORD newDeci = (newDvb << leftShift) & 0x0fffffff;
		newDvb = (newDvb & NEGABIT) | (dot << 28) | newDeci;
	}
	return(newDvb);
}

static int DwordCompare(DWORD dw1, DWORD dw2)
{
	if (dw1 > dw2) {
		return(1);
	}
	else if (dw1 < dw2) {
		return(-1);
	}
	else {
		return(0);
	}
}

int DvbCompare(DWORD dvb1, DWORD dvb2)
{
	if (dvb1 == dvb2) {
		return(0);
	}
	BOOL p1 = DvbIsPositive(dvb1);
	BOOL p2 = DvbIsPositive(dvb2);
	if (p1 && (!p2)) {
		return(1);
	}
	if ((!p1) && p2) {
		return(-1);
	}

	// 至此两个数符号相同，然后统一成正数，移位对齐小数点方便比较
	int compFactor = 1;					// 比较符号因子
	int dot1 = DotOfDvb(dvb1);
	int dot2 = DotOfDvb(dvb2);
	if (!p1) {
		compFactor *= -1;
	}
	dvb1 &= 0x0FFFFFFF;					// 清除符号位和小数点位
	dvb2 &= 0x0FFFFFFF;
	if (dot1 < dot2) {					// 移位向dot2对齐
		DWORD oldDvb1 = dvb1;
		int shift1 = (dot2 - dot1) * 4;
		dvb1 <<= shift1;
		int big1;
		if ((dvb1 >> shift1) != oldDvb1) {
			big1 = 1;					// dvb1在左移过程中高位溢出，所以较大
		}
		else {
			big1 = DwordCompare(dvb1, dvb2);
		}
		compFactor *= big1;
	}
	else if (dot1 > dot2) {				// 移位向dot1对齐
		DWORD oldDvb2 = dvb2;
		int shift2 = (dot1 - dot2) * 4;
		dvb2 <<= shift2;
		int big2;
		if ((dvb2 >> shift2) != oldDvb2) {
			big2 = 1;					// dvb2在左移过程中高位溢出，所以大
		}
		else {
			big2 = DwordCompare(dvb2, dvb1);
		}
		compFactor *= (-big2);
	}
	else {
		compFactor *= DwordCompare(dvb1, dvb2);
	}
	return(compFactor);
}

void Dvb2String(char buf[], DWORD dvb)
{
	BOOL negative = !DvbIsPositive(dvb);
	int dot = DotOfDvb(dvb);
	int dwidth = 7;
	DWORD mask = 0x0f000000;
	int i;
	for (i = 0; i < 7; i++) {				// 确定有效数字宽度
		if (mask & dvb) {
			break;
		}
		dwidth--;
		mask >>= 4;
	}
	if (dwidth == 0) {					// 特例全0
		int p = 0;
		buf[p++] = '0';
		if (dot >= 1) {
			buf[p++] = '.';
			for (int i = 0; i < dot; i++) {
				buf[p++] = '0';
			}
		}
		buf[p++] = '\0';
		return;
	}
	int stringLen;
	if (dot == 0) {
		stringLen = dwidth;
	}
	else {
		stringLen = __max(dwidth + 1, dot + 2);
	}
	if (negative) {
		stringLen++;
	}
	buf[stringLen] = '\0';				// 从末尾开始形成字符串
	int dcount = 0;
	BOOL needDot = (dot != 0) ? TRUE : FALSE;
	for (i = stringLen - 1; i >= 0; i--) {
		if (needDot && dcount == dot) {
			needDot = FALSE;
			buf[i] = '.';
		}
		else if (dcount < dwidth) {
			int d = (dvb & 0x0000000f);
			if (d < 10) {
				buf[i] = (char)(d + '0');
			}
			else {
				buf[i] = (char)(d - 10 + 'A');
			}
			dvb >>= 4;
			dcount++;
		}
		else if (i == 0 && negative) {
			buf[i] = '-';
		}
		else {
			buf[i] = '0';
			dcount++;
		}
	}
}

// 隐含条件：str是一个格式良好的字符串，没有非法字符，顶多有一个小数点，负号一定在首位，有效数字不超过7个
DWORD String2Dvb(const char* str)
{
	int rightIndex = (int)strlen(str) - 1;// 字符串数字右端位置
	BOOL negative = (str[0] == '-') ? TRUE : FALSE;
	int leftIndex = negative ? 1 : 0;		// 字符串数字左端位置
	DWORD dvb = 0x00000000;
	BOOL dotFound = FALSE;
	int dot = 0;
	for (int i = leftIndex; i <= rightIndex; i++) {		// 从左端高位开始转换，方便移位
		char c = str[i];
		if (c == '.') {					// 遇到小数点
			dotFound = TRUE;
		}
		else {							// 遇到数字
			dvb <<= 4;
			dvb |= (c - '0');
			if (dotFound) {
				dot++;
			}
		}
	}
	if (negative) {
		dvb |= NEGABIT;
	}
	if (dot > 0) {
		dvb |= (dot << 28);
	}
	return(dvb);
}

int GetDvbCount(DWORD dvbs[])
{
	int c = 0;
	while (dvbs[c] != EODVB) {
		c++;
	}
	return(c);
}

int Dvbs2String(char buf[], DWORD dvbs[])
{
	char dvbBuf[10];
	int c = 0;
	buf[0] = '\0';
	while (dvbs[c] != EODVB) {
		if (c > 0) {
			strcat(buf, ",");
		}
		Dvb2String(dvbBuf, dvbs[c]);
		strcat(buf, dvbBuf);
		c++;
	}
	return(c);
}

int String2Dvbs(DWORD dvbs[], const char* str)
{
	char dvbBuf[10];
	int i = 0;
	int bi = 0;
	int c = 0;
	while (TRUE) {
		if (str[i] == ',' || str[i] == '\0') {
			dvbBuf[bi] = '\0';
			dvbs[c] = String2Dvb(dvbBuf);
			c++;
			if (str[i] == '\0') {
				break;
			}
			else {
				i++;
				bi = 0;
			}
		}
		else {
			dvbBuf[bi] = str[i];
			bi++;
			i++;
		}
	}
	dvbs[c] = EODVB;
	return(c);
}

const long IDRMUL[] = { 10,100,1000,10000,100000,1000000 };
const double IDRDIV[] = { 0.1,0.01,0.001,0.0001,0.00001,0.000001 };
long Float2Idr(double fv, int dot)
{
	if (dot > 0 && dot < 7) {
		return(Round2Int(fv * IDRMUL[dot - 1]));
	}
	else {
		return(Round2Int(fv));
	}
}

double Idr2Float(long idr, int dot)
{
	if (dot > 0 && dot < 7) {
		return(idr * IDRDIV[dot - 1]);
	}
	else {
		return((double)idr);
	}
}

// 其实相当于Dvb2Int,忽略小数点信息,保留正负
long Dvb2Idr(DWORD dvb)
{
	DWORD numDvb = (dvb & 0x0FFFFFFF);	// 纯数部分
	long lv = (numDvb & 0x0000000F);	// 最右侧4-bit数，十进制与二进制相等
	for (int i = 0; i < 6; i++) {
		numDvb >>= 4;					// 逐个右移
		if (numDvb == 0) {
			break;
		}
		long deci = (numDvb & 0x0000000F);
		if (deci != 0) {
			lv += deci*IDRMUL[i];
		}
	}
	return((dvb & NEGABIT) ? -lv : lv);
}

DWORD Idr2Dvb(long idr, int dot)
{
	BOOL isNega = (idr < 0);
	long absIdr = isNega ? -idr : idr;
	DWORD dvb = absIdr % 10;
	int shift = 4;
	for (int i = 0; i < 6; i++) {
		absIdr /= 10;
		if (absIdr == 0) {
			break;
		}
		long deci = absIdr % 10;
		if (deci != 0) {
			dvb |= (deci << shift);
		}
		shift += 4;
	}

	dvb |= (dot << 28);
	if (isNega) {
		dvb |= NEGABIT;
	}
	return(dvb);
}

DWORD DvbInc(DWORD dvb, long idr)
{
	int dot = DotOfDvb(dvb);
	long sum = Dvb2Idr(dvb) + idr;
	sum = __min(sum, MAXIDR);
	sum = __max(sum, MINIDR);
	return(Idr2Dvb(sum, dot));
}

DWORD DvbCountInc(DWORD dvbc, long step, BOOL* pbWrap)
{
	if (pbWrap != NULL) {
		*pbWrap = FALSE;
	}
	int dot = DotOfDvb(dvbc);
	long nextc = Dvb2Idr(dvbc) + step;
	if (nextc > MAXIDR) {
		nextc -= IDRRANGE;
		if (pbWrap != NULL) {
			*pbWrap = TRUE;
		}
	}
	return(Idr2Dvb(nextc, dot));
}

// 前提是两个DVB具有相同的小数点
long DvbMinus2Idr(DWORD dvb1, DWORD dvb2)
{
	return(Dvb2Idr(dvb1) - Dvb2Idr(dvb2));
}

// 逻辑上dvbc2>dvbc1，常用于求时间差
DWORD DvbCountMinus(DWORD dvbc1, DWORD dvbc2)
{
	int dot = DotOfDvb(dvbc1);
	DWORD idrc1 = Dvb2Idr(dvbc1);
	DWORD idrc2 = Dvb2Idr(dvbc2);
	DWORD intv;
	if (idrc2 > idrc1) {
		intv = idrc2 - idrc1;
	}
	else {
		intv = (idrc2 + IDRRANGE) - idrc1;
	}
	return(Idr2Dvb(intv, dot));
}

DVB DvbMin(DWORD dvb1, DWORD dvb2)
{
	BOOL posi1 = DvbIsPositive(dvb1);
	BOOL posi2 = DvbIsPositive(dvb2);

	if (posi1) {
		if (posi2) {
			return(__min(dvb1, dvb2));
		}
		else {
			return(dvb2);
		}
	}
	else {
		if (posi2) {
			return(dvb1);
		}
		else {
			return(__max(dvb1, dvb2));
		}
	}
}

DVB DvbMax(DVB dvb1, DVB dvb2)
{
	BOOL posi1 = DvbIsPositive(dvb1);
	BOOL posi2 = DvbIsPositive(dvb2);

	if (posi1) {
		if (posi2) {
			return(__max(dvb1, dvb2));
		}
		else {
			return(dvb1);
		}
	}
	else {
		if (posi2) {
			return(dvb2);
		}
		else {
			return(__min(dvb1, dvb2));
		}
	}
}

WORD Int2Bcd(int iv)
{
	WORD bcd = 0;

	iv = __min(iv, 9999);
	iv = __max(iv, 0);
	for (int i = 0; i < 4; i++) {
		bcd += (iv % 10) << (4 * i);
		iv /= 10;
	}
	return(bcd);
}

int Bcd2Int(WORD bcd)
{
	int multi = 1;
	int iv = 0;
	WORD digi;

	for (int i = 0; i < 4; i++) {
		digi = (bcd & 0x000F);
		digi = __min(digi, 9);
		iv += digi*multi;
		bcd >>= 4;
		multi *= 10;
	}
	return(iv);
}

void Bcd2String(char buf[], WORD bcd)
{
	WORD digi;

	for (int i = 0; i < 4; i++) {
		digi = (bcd & 0x000F);
		digi = __min(digi, 9);
		buf[3 - i] = (char)(digi + '0');
		bcd >>= 4;
	}
	buf[4] = '\0';
}

WORD String2Bcd(char* str)
{
	int len = (int)strlen(str);
	WORD bcd = 0;
	WORD dchar;

	for (int i = 0; i < len; i++) {
		dchar = str[i];
		dchar = __min(dchar, '9');
		dchar = __max(dchar, '0');
		bcd = (bcd << 4) + dchar - '0';
	}
	return(bcd);
}