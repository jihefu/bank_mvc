#ifndef __DVB_TYPE_H
#define __DVB_TYPE_H
#include <stdint.h>
#include "string.h"
#include "stdbool.h"


//typedef enum {TRUE = 1, FALSE = 0} BOOL;
//typedef enum{FALSE = 0,TRUE = 1} bool;

#define __min(a, b)	((a)>(b)?(b):(a))
#define __max(a, b)	((a)>(b)?(a):(b))
#define TRUE 		1 
#define FALSE   0 

#endif
