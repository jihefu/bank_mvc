#ifndef __SYS_H
#define __SYS_H	

//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32开发板		   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2019/9/17
//版本：V1.7
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved
////////////////////////////////////////////////////////////////////////////////// 	 

//0,不支持ucos
//1,支持ucos
#define SYSTEM_SUPPORT_OS		0		//定义系统文件夹是否支持UCOS
	///////////////////////////////////////////////////////////////////////////////////
//定义一些常用的数据类型短关键字 
typedef  long long s64;
typedef  long  s32;
typedef  short int  s16;
typedef char  s8;

typedef unsigned long  u32;
typedef unsigned short int u16;
typedef unsigned char  u8;
typedef unsigned  long long u64;



																    
	 
//位带操作,实现51类似的GPIO控制功能
//具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).
//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//IO口地址映射
#define GPIOA_ODR_Addr    1
#define GPIOB_ODR_Addr    1
#define GPIOC_ODR_Addr    1
#define GPIOD_ODR_Addr    1
#define GPIOE_ODR_Addr   1
#define GPIOF_ODR_Addr    1
#define GPIOG_ODR_Addr    1  

#define GPIOA_IDR_Addr    1
#define GPIOB_IDR_Addr    1
#define GPIOC_IDR_Addr    1
#define GPIOD_IDR_Addr    1
#define GPIOE_IDR_Addr    1
#define GPIOF_IDR_Addr    1
#define GPIOG_IDR_Addr   1
 
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

void Stm32_Clock_Init(u32 PLL);					//时钟系统配置

//以下为汇编函数
void WFI_SET(void);		//执行WFI指令
void INTX_DISABLE(void);//关闭所有中断
void INTX_ENABLE(void);	//开启所有中断
void MSR_MSP(u32 addr);	//设置堆栈地址


typedef struct _16_Bits_Struct
{
	u16 bit0 : 1;
	u16 bit1 : 1;
	u16 bit2 : 1;
	u16 bit3 : 1;
	u16 bit4 : 1;
	u16 bit5 : 1;
	u16 bit6 : 1;
	u16 bit7 : 1;
	u16 bit8 : 1;
	u16 bit9 : 1;
	u16 bit10 : 1;
	u16 bit11 : 1;
	u16 bit12 : 1;
	u16 bit13 : 1;
	u16 bit14 : 1;
	u16 bit15 : 1;
} Bits_16_TypeDef;


#endif
