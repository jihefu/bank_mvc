#ifndef	_KEY_H_
#define _KEY_H_
#include "stdint.h" 

#define KEY0        1
#define KEY1       1
#define KEY2       1
#define KEY3       1
#define KEY4      1
#define KEY5       1
#define KEY6      1
#define KEY7       1
 
#define KEY_DATA 	(~((KEY7 << 7) | (KEY6 << 6) | (KEY5 <<5) | (KEY4 << 4) | (KEY3 << 3) | (KEY2 << 2) | (KEY1 << 1) | KEY0))  

#define KEY0_PRES 	1
#define KEY1_PRES		2  
#define KEY2_PRES		3 
#define KEY3_PRES 	4
#define KEY4_PRES		5 
#define KEY5_PRES 	6
#define KEY6_PRES		7 
#define KEY7_PRES 	8 


extern void KEY_Init(void);
extern uint8_t KEY_Scan(uint8_t mode);
#endif