#ifndef __LCD_H
#define __LCD_H		
#include "../../SYSTEM/sys/sys.h"	

#include "stdlib.h"
//////////////////////////////////////////////////////////////////////////////////	 
//////////////////////////////////////////////////////////////////////////////////
 
#ifdef __cplusplus
extern "C" {
#endif
 
  
//LCD重要参数集
typedef struct  
{										    
	u16 width;			//LCD 宽度
	u16 height;			//LCD 高度
	u16 id;				//LCD ID
	u8  dir;			//横屏还是竖屏控制：0，竖屏；1，横屏。	
	u8	wramcmd;		//开始写gram指令
	u8  setxcmd;		//设置x坐标指令
	u8  setycmd;		//设置y坐标指令	 
}_lcd_dev; 	  

//LCD参数
extern _lcd_dev lcddev;	//管理LCD重要参数
//LCD的画笔颜色和背景色	   
extern u16  POINT_COLOR;//默认红色    
extern u16  BACK_COLOR; //背景颜色.默认为白色


//////////////////////////////////////////////////////////////////////////////////	 
//-----------------LCD端口定义---------------- 

#define	LCD_RESET   g_nop_port
#define LCD_BL      g_nop_port
#define	LCD_CS		g_nop_port  //片选端口  	    
#define	LCD_RS		g_nop_port  //数据/命令     

//扫描方向定义
#define L2R_U2D  0 //从左到右,从上到下
#define L2R_D2U  1 //从左到右,从下到上
#define R2L_U2D  2 //从右到左,从上到下
#define R2L_D2U  3 //从右到左,从下到上

#define U2D_L2R  4 //从上到下,从左到右
#define U2D_R2L  5 //从上到下,从右到左
#define D2U_L2R  6 //从下到上,从左到右
#define D2U_R2L  7 //从下到上,从右到左	 

extern u8 DFT_SCAN_DIR;


//PC0~15,作为数据口
#define DATAOUT(x) GPIOC->ODR=x; //数据输出
#define DATAIN     GPIOC->IDR;   //数据输入

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GRAY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
 
#define LIGHTGREEN     	 0X841F //浅绿色
//#define LIGHTGRAY        0XEF5B //浅灰色(PANNEL)
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)
	    															  
void LCD_Init(void);													   	//初始化
void LCD_DisplayOn(void);													//开显示
void LCD_DisplayOff(void);													//关显示
void LCD_Clear(u16 Color);	 												//清屏
void LCD_SetCursor(u16 Xpos, u16 Ypos);										//设置光标
void LCD_DrawPoint(u16 x,u16 y);											//画点
void LCD_Fast_DrawPoint(u16 x,u16 y,u16 color);								//快速画点
u16  LCD_ReadPoint(u16 x,u16 y); 											//读点 
void Draw_Circle(u16 x0,u16 y0,u8 r);										//画圆
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);							//画线
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);		   				//画矩形
void LCD_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color);		   				//填充单色

void LCD_ShowPic_FromFlash_DMA(u16 sx,u16 sy,u16 ex,u16 ey, u32 flash_addr, u32 size);
void LCD_Fill_DMA(u16 sx,u16 sy,u16 ex,u16 ey,u16 color);		   				//填充单色
void LCD_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 *color);				//填充指定颜色
void LCD_ShowChar(u16 x,u16 y,u8 num,u8 size,u8 mode);						//显示一个字符
void LCD_ShowNum(u16 x,u16 y,u32 num,u8 len,u8 size);  						//显示一个数字
void LCD_ShowxNum(u16 x,u16 y,u32 num,u8 len,u8 size,u8 mode);				//显示 数字
void LCD_ShowString(u16 x,u16 y,u16 width,u16 height,u8 size,u8 *p);		//显示一个字符串,12/16字体
void LCD_Set_Window(u16 sx,u16 sy,u16 width,u16 height);
void IO_init(void);
void User_LCD_Write_Prepare(u16 sx,u16 sy,u16 ex,u16 ey);
void User_LCD_WriteRAM(u8 *buf, u32 size);

void LCD_WR_REG(u8);
void LCD_WR_DATA(u16);
void LCD_WriteReg(u8 LCD_Reg, u16 LCD_RegValue);
u16 LCD_ReadReg(u8 LCD_Reg);
void LCD_WriteRAM_Prepare(void);
void LCD_WriteRAM(u16 RGB_Code);		  
void LCD_Scan_Dir(u8 dir);							//设置屏扫描方向
void LCD_Display_Dir(u8 dir);						//设置屏幕显示方向

// 图片操作

//图片头结构体
typedef struct    
{
   char scan;   //图片扫描模式
   char gray;    //灰度值
   u16  w;      //图像的宽度
   u16  h;			//图像的高度
   char is565;		//是否为565模式
   char rgb;			//RGB排列顺序
} HeadColor;

#define HEADCOLOR_SIZE sizeof(HeadColor)
	
//////////////////////手控盒自定义函数类型	//////////////////////////////////////////////////////////////////////
#define EN			//初始化背景，如果定义了EN初始化为英文，如果没有定义，初始化中文
#define DZONE0_PFONTHEAD	&FontHeadSmallYellow
#define	DZONE0_WIDTH			90
#define	DZONE0_HEIGHT			NumberZone0->pFontHead->height
#define DZONE0_XS					48
#define	DZONE0_YS					34
#define DZONE0_BUFSIZE 		2 * DZONE0_WIDTH * NumberZone0->pFontHead->height

#define DZONE1_PFONTHEAD	&FontHeadSmallBlue
#define	DZONE1_WIDTH			90
#define	DZONE1_HEIGHT			NumberZone1->pFontHead->height
#define DZONE1_XS					48
#define	DZONE1_YS					188
#define DZONE1_BUFSIZE 		2 * DZONE1_WIDTH * NumberZone1->pFontHead->height

#define DZONE2_PFONTHEAD	&FontHeadBigYellow
#define	DZONE2_WIDTH			182
#define	DZONE2_HEIGHT			NumberZone2->pFontHead->height
#define DZONE2_XS					95
#define	DZONE2_YS					78
#define DZONE2_BUFSIZE 		2 * DZONE2_WIDTH * NumberZone2->pFontHead->height

#define DZONE3_PFONTHEAD	&FontHeadBigBlue
#define	DZONE3_WIDTH			182
#define	DZONE3_HEIGHT			NumberZone3->pFontHead->height  
#define DZONE3_XS					160
#define	DZONE3_YS					78
#define DZONE3_BUFSIZE 		2	*	DZONE3_WIDTH	*	NumberZone1->pFontHead->height

#define PZONE0_PFONTHEAD		&IconPicHead0
#define	PZONE0_WIDTH				40
#define	PZONE0_HEIGHT				40  
#define PZONE0_XS						40
#define	PZONE0_YS						135
#define PZONE0_BUFSIZE 			40*40 

#define PZONE1_PFONTHEAD		&IconPicHead1
#define	PZONE1_WIDTH				48
#define	PZONE1_HEIGHT				24  
#define PZONE1_XS						116
#define	PZONE1_YS						265
#define PZONE1_BUFSIZE 			48*24

#define PZONE2_PFONTHEAD		&IconPicHead2
#define	PZONE2_WIDTH				64
#define	PZONE2_HEIGHT				24  
#define PZONE2_XS						6
#define	PZONE2_YS						88
#define PZONE2_BUFSIZE 			64*24

#define PZONE3_PFONTHEAD		&IconPicHead3
#define	PZONE3_WIDTH				80
#define	PZONE3_HEIGHT				24
#define PZONE3_XS						6
#define	PZONE3_YS						233
#define PZONE3_BUFSIZE 			80*24

#define PZONE4_PFONTHEAD		&IconPicHead4
#define	PZONE4_WIDTH				48
#define	PZONE4_HEIGHT				24
#define PZONE4_XS						180
#define	PZONE4_YS						265
#define PZONE4_BUFSIZE 			48*24

#define PZONE11_PFONTHEAD		&IconPicHead11
#define	PZONE11_WIDTH				48
#define	PZONE11_HEIGHT			24
#define PZONE11_XS					116
#define	PZONE11_YS					16
#define PZONE11_BUFSIZE 		48*24

#define PZONE12_PFONTHEAD		&IconPicHead12
#define	PZONE12_WIDTH				72
#define	PZONE12_HEIGHT			24
#define PZONE12_XS					6
#define	PZONE12_YS					16
#define PZONE12_BUFSIZE 		72*24

#define PZONE13_PFONTHEAD		&IconPicHead13
#define	PZONE13_WIDTH				64
#define	PZONE13_HEIGHT			24
#define PZONE13_XS					6
#define	PZONE13_YS					169
#define PZONE13_BUFSIZE 		64*24

#define PZONE14_PFONTHEAD		&IconPicHead14
#define	PZONE14_WIDTH				48
#define	PZONE14_HEIGHT			24
#define PZONE14_XS					180
#define	PZONE14_YS					16
#define PZONE14_BUFSIZE 		48*24



#define	PICBACKGROUND_WIDTH				320
#define PICBACKGROUND_HEIGHT			240
#define PICBACKGROUND_XS					0
#define PICBACKGROUND_YS					0
#define PICBACKGROUND_BUFSIZE 		320*240

#define	PICLOGO_WIDTH							160
#define PICLOGO_HEIGHT						120
#define PICLOGO_XS								60
#define PICLOGO_YS								80
#define PICLOGO_BUFSIZE 					160*120


// 图片存取策略
// 背景图片大小320*240*word=150k字节
// 大数字图片320*48*word=30k，包含0123456789-.:，变宽数字符号
// 小数字图片是大数字的1/4，7.5k
// 全部图片存在闪存里

// 数字图片描述
//结构定义		 
//width				:	  字符图片宽度
//height			:	  字符图片高度
//chars[13]		:	  字符
//charBgnCol[13]: 字符开始列
//charWidth[13] : 字符宽度
//pFontPic:	  		对应字体内存地址
typedef struct {
	//char name[16];
	u16  	width;
	u16  	height;
	char 	chars[13];
	u16  	charBgnCol[13];
	u16   charWidth[13]; 
	u8		*pFontPic;
} FontHead;

//切换图片描述
//结构定义		 
//width				:	  ICON图片宽度
//height			:	  ICON图片高度
//pointPerPic :		每个小图片对应的点数
//iconCount[3]:	  ICON图片内部小图片个数，最多8个
//pIconPic:	  		ICON对应内存地址
typedef struct {
	//char name[16];
	u16  	width;
	u16  	height;
	u16   pointPerIcon;
	u8 		iconCount;
	u8		*pIconPic;
} IconPicHead;


//背景图片开始排
//然后逐个按FontHead+FontPix持续排列大小数字字体

#define ALIGN_LEFT  				0         //左对齐
#define ALIGN_RIGHT					1					//右对齐
#define ALIGN_CENTER   			2					//居中
// 4个数字刷新区域
// pFontHead 	:   字体结构指针
// x,y,				:		显示区域的起始地址
// width			:		显示区域的宽度
// pixBuffer	:		显示区域大小的内存地址
// alignDir 	:   显示对齐方向：左对齐，右对齐，居中
typedef struct {
	//u16 fontHeadAddr;
	FontHead *pFontHead;
	u16 x, y;
	u16 width;
	u8 *pixBuffer;
	u8	alignDir;    
	//void Init(const char *fontName, u16 x, u16 y, u16 width);
//	void Init(int *fontIndex, u16 x, u16 y, u16 width);
//	void Show(u32 dvb);
} NumberZone;
// NumberZone::Init
// 在内存初始化数字图片缓存区，大数字需要200*48*word约不到20K，小数字5k
// 整个程序需要2大2小，共50k内存
// NumberZone::Show()
// 先将DVB码转换成字符串，根据字体计算字符串宽度，按居中计算起始x坐标
// memset()清除缓存区
// 从左到右将字符从闪存转移到缓存区
// 调用LCD_DrawPicture()将缓存区画到液晶屏


//图片刷新区域
// x,y,				:		显示区域的起始地址
// wd					:		显示区域的宽度
// ht					:		显示区域的高度
// pixBuffer	:		显示区域大小的内存地址
// PicHead  ：  对应图片的信息
typedef struct {
	u16 x, y;
	u16 height, width;
	char *pixBuffer; 
	IconPicHead *pIconPicHead;	
} PicZone;


#ifdef __cplusplus 
}
#endif



#endif  
	 
	 



