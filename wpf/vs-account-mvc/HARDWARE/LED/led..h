#ifndef	_LED_H_
#define _LED_H_
#include "../../SYSTEM/sys/sys.h"
#define KEY0_Pin GPIO_PIN_3
#define KEY0_GPIO_Port GPIOE
#define KEY1_Pin GPIO_PIN_4
#define KEY1_GPIO_Port GPIOE
#define WAKE_UP_Pin GPIO_PIN_0
#define WAKE_UP_GPIO_Port GPIOA


 
#define KEY0        HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_4)  //KEY0??PE4
#define KEY1        HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_3)  //KEY1??PE3
#define WK_UP       HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)  //WKUP??PA0

#define KEY0_PRES 	1
#define KEY1_PRES		2
#define WKUP_PRES   3

extern void KEY_Init(void);
extern u8 KEY_Scan(u8 mode);
#endif