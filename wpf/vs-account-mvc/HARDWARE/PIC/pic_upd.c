#include "pic_upd.h"
#include "../LCD/lcd.h"
#include "../../SYSTEM/dvb/dvb.h"

_PIC_INFO picD0 = { //黄色小字体
	'0',
	160,
	24,
	13,
	0,16,28,41,54,67,80,93,106,119,131,141,147,0,0,
	16,12,13,13,13,13,13,13,13,12,10,6,13,0,0,
	0x3C980,
	0x1E00
}; 
_PIC_INFO picD1 = { //蓝色小字体
	'0',
	160,
	24,
	13,
	0,16,28,41,54,67,80,93,106,119,131,141,147,0,0,
	16,12,13,13,13,13,13,13,13,12,10,6,13,0,0,
	0x3E780,
	0x1E00
}; 
_PIC_INFO picD2 = { //黄色大字体
	'0',
	320,
	48,
	13,
	0,29,56,83,110,137,164,192,218,244,270,288,301,0,0,
	29,27,27,27,27,27,28,26,26,26,18,13,19,0,0,
	0x40580,
	0x7800
}; 
_PIC_INFO picD3 = { //蓝色大字体
	'0',
	320,
	48,
	13,
	0,29,56,83,110,137,164,192,218,244,270,288,301,0,0,
	29,27,27,27,27,27,28,26,26,26,18,13,19,0,0,
	0x47D80,
	0x7800
}; 
_PIC_INFO picP0 = {  //状态图标
	'0',
	280,
	40,
	7,
	0,40,80,120,160,200,240,0,0,0,0,0,0,0,0,
	40,40,40,40,40,40,40,0,0,0,0,0,0,0,0,
	0x25800,
	0x5780
}; 
_PIC_INFO picP1 = {  	//力单位切换
	'0',
	336,
	24,
	7,
	0,48,96,144,192,240,288,0,0,0,0,0,0,0,0,
	48,48,48,48,48,48,48,0,0,0,0,0,0,0,0,
	0x2AF80,
	0x3F00
}; 
_PIC_INFO picP2 = {   //力峰值单位切换
	'0',
	448,
	24,
	7,
	0,64,128,192,256,320,384,0,0,0,0,0,0,0,0,
	64,64,64,64,64,64,64,0,0,0,0,0,0,0,0,
	0x2EE80,
	0x5400
}; 
_PIC_INFO picP3 = { //速度单位切换
	'0',
	160,
	24,
	2,
	0,80,0,0,0,0,0,0,0,0,0,0,0,0,0,
	80,80,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x34280,
	0x1E00
}; 
_PIC_INFO picP4 = { //位移单位切换
	'0',
	96,
	24,
	2,
	0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	48,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x36080,
	0x1200
}; 
_PIC_INFO picP11 = { //力中英文切换
	'0',
	96,
	24,
	2,
	0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	48,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x37280,
	0x1200
}; 
_PIC_INFO picP12 = { //力峰值中英文切换
	'0',
	144,
	24,
	2,
	0,72,0,0,0,0,0,0,0,0,0,0,0,0,0,
	72,72,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x38480,
	0x1B00
}; 
_PIC_INFO picP13 = { //速度中英文切换
	'0',
	128,
	24,
	2,
	0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,
	64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x39F80,
	0x1800
}; 
_PIC_INFO picP14 = { //位移中英文切换
	'0',
	96,
	24,
	2,
	0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	48,48,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0x3B780,
	0x1200
}; 

_ZONE			zone0;
_ZONE			zoneD0;
_ZONE			zoneD1;
_ZONE			zoneD2;
_ZONE			zoneD3;
_ZONE			zoneP0;
_ZONE			zoneP1;
_ZONE			zoneP2;
_ZONE			zoneP3;
_ZONE			zoneP4;
_ZONE			zoneP11;
_ZONE			zoneP12;
_ZONE			zoneP13;
_ZONE			zoneP14;
#define		LCD_BUF_SIZE 		3500
u8 lcd_buf_A[LCD_BUF_SIZE]; 
u8 lcd_buf_B[LCD_BUF_SIZE]; 
u8	buf_ready = 0;
extern uint8_t lcd_busy;
void Init_Zone(void)
{

	zoneD0.No = '0';
	zoneD0.x = 48;
	zoneD0.y = 34;
	zoneD0.width = 24;
	zoneD0.heigth = 90;
	zoneD0.pic_info = &picD0;     //zone0区，显示图片0的内容
	
	
	zoneD1.No = '0';
	zoneD1.x = 48;
	zoneD1.y = 188;
	zoneD1.width = 24;
	zoneD1.heigth = 90;
	zoneD1.pic_info = &picD1;     //zone0区，显示图片0的内容
	
	zoneD2.No = '0';
	zoneD2.x = 95;
	zoneD2.y = 78;
	zoneD2.width = 48;
	zoneD2.heigth = 182;
	zoneD2.pic_info = &picD2;     //zone0区，显示图片0的内容
	
	zoneD3.No = '0';
	zoneD3.x = 160;
	zoneD3.y = 78;
	zoneD3.width = 48;
	zoneD3.heigth = 182;
	zoneD3.pic_info = &picD3;     //zone0区，显示图片0的内容
	
	zoneP0.No = '0';
	zoneP0.x = 40;
	zoneP0.y = 135;
	zoneP0.width = 40;
	zoneP0.heigth = 40;
	zoneP0.pic_info = &picP0;     //zone0区，显示图片0的内容
	
	zoneP1.No = '0';
	zoneP1.x = 116;
	zoneP1.y = 265;
	zoneP1.width = 24;
	zoneP1.heigth = 48;
	zoneP1.pic_info = &picP1;     //zone0区，显示图片0的内容
	
	zoneP2.No = '0';
	zoneP2.x = 6;
	zoneP2.y = 88;
	zoneP2.width = 24;
	zoneP2.heigth = 64;
	zoneP2.pic_info = &picP2;     //zone0区，显示图片0的内容
	
	zoneP3.No = '0';
	zoneP3.x = 6;
	zoneP3.y = 233;
	zoneP3.width = 24;
	zoneP3.heigth = 80;
	zoneP3.pic_info = &picP3;     //zone0区，显示图片0的内容
	
	zoneP4.No = '0';
	zoneP4.x = 180;
	zoneP4.y = 265;
	zoneP4.width = 24;
	zoneP4.heigth = 48;
	zoneP4.pic_info = &picP4;     //zone0区，显示图片0的内容
	
	zoneP11.No = '0';
	zoneP11.x = 116;
	zoneP11.y = 16;
	zoneP11.width = 24;
	zoneP11.heigth = 48;
	zoneP11.pic_info = &picP11;     //zone0区，显示图片0的内容
	
	zoneP12.No = '0';
	zoneP12.x = 6;
	zoneP12.y = 16;
	zoneP12.width = 24;
	zoneP12.heigth = 72;
	zoneP12.pic_info = &picP12;     //zone0区，显示图片0的内容
	
	zoneP13.No = '0';
	zoneP13.x = 6;
	zoneP13.y = 169;
	zoneP13.width = 24;
	zoneP13.heigth = 64;
	zoneP13.pic_info = &picP13;     //zone0区，显示图片0的内容
	
	zoneP14.No = '0';
	zoneP14.x = 180;
	zoneP14.y = 16;
	zoneP14.width = 24;
	zoneP14.heigth = 48;
	zoneP14.pic_info = &picP14;     //zone0区，显示图片0的内容
}

void Show_backGround(void)
{
//	LCD_ShowPic_FromFlash_DMA(0,0,240,320,0,240*320);
	
	u8 *pbuf_A;
	u8 *pbuf_B;
	u8 *pbuf;
	u8	buf_sel = 0;
	u32		range = 0;
	u32	ept_len = 0;
	u16 len = 0;
	u32		flash_addr = 0;
	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	
	range = 320*240*2;
	ept_len = range;
	flash_addr = 0;
	
	User_LCD_Write_Prepare(0, 0, 240, 320);   //设置需要显示的LCD窗口大小等 
	
	while(ept_len)
	{
		if(ept_len>LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_len;
		
		pbuf = buf_sel ? pbuf_B : pbuf_A;
		W25QXX_DMA_Read(pbuf, flash_addr, len);
		while(buf_ready==0);
		buf_ready = 0;
		while(lcd_busy);
		User_LCD_WriteRAM(pbuf, len); 
		flash_addr += len;
		ept_len -= len;
	}
}


//接收的数字为DVB码
void LCD_ShowZone(_ZONE *zone, u32 dvb, uint8_t alignDir) 
{
	char str[10];
	Dvb2String(str,dvb);  		
	Show_Num(zone, str, alignDir);   
}

void Show_Num(_ZONE *zone, char *p , uint8_t alignDir)  
{
	u32	flash_addr = 0;
	u32 len = 0;
	u32	count = 0;  
	u32 add = 0;
	u8 *pbuf_A;
	u8 *pbuf_B;
	u8 *pbuf;
	u32 tempColCount = 0;
	u32 	valColCount = 0;
	char *pTemp= p;
	u32	zone_range =0;
	u8  strLen = 0;
	u16 ept_len = 0;
	bool buf_sel = 0;
	
	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	mymemset((void *)pbuf_A,0,LCD_BUF_SIZE);             //缓冲区清空
	mymemset((void *)pbuf_B,0,LCD_BUF_SIZE);             //缓冲区清空

	zone_range = zone->heigth * zone->width *2;   //ZONE的容量大小，如果超过了这个容量，需要减少剩余的字符
	
	while((*pTemp<='~')&&(*pTemp>=' '))//判断是不是非法字符!
	{
		switch(*pTemp++)
		{
			case '0'	:	tempColCount += zone->pic_info->entry_width[0]; 	break;
	  	case '1'	: tempColCount += zone->pic_info->entry_width[1]; 	break;
			case '2'	: tempColCount += zone->pic_info->entry_width[2];		break;
			case '3'	: tempColCount += zone->pic_info->entry_width[3];		break;
			case '4'	: tempColCount += zone->pic_info->entry_width[4];		break;
			case '5'	: tempColCount += zone->pic_info->entry_width[5];		break;
			case '6'	: tempColCount += zone->pic_info->entry_width[6];		break;
			case '7'	: tempColCount += zone->pic_info->entry_width[7];		break;
			case '8'	: tempColCount += zone->pic_info->entry_width[8];		break;
			case '9'	: tempColCount += zone->pic_info->entry_width[9];		break;
			case '-'	: tempColCount += zone->pic_info->entry_width[10];	break;
			case '.'	: tempColCount += zone->pic_info->entry_width[11];	break;
			case ':'	: tempColCount += zone->pic_info->entry_width[12];		break;			
			default:	break;
		}
		if(tempColCount > zone->heigth) break;    //如果实际计算的数据大于显示区域宽度，省略当前及后面的字符
		valColCount = tempColCount;                 		//需要显示的列数
		strLen ++;																			//需要显示的字符数
	}
	
	switch(alignDir)            	//根据对齐方式，计算前面留空的buf大小
	{
		case ALIGN_LEFT : 	ept_len = 0; 																									break;
		case ALIGN_RIGHT :	ept_len = zone_range - valColCount * zone->width * 2;					break;
		case ALIGN_CENTER : ept_len = ((zone->heigth - valColCount)/2) * zone->width*2; 	break;
		default: 			ept_len = 0;	break;
	}
	User_LCD_Write_Prepare(zone->x, zone->y, zone->width, zone->heigth);   //设置需要显示的LCD窗口大小等
	
	while(ept_len)           		 //填充空白区域
	{
		if(ept_len>LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_len;
		
		User_LCD_WriteRAM(pbuf, len);
		while(lcd_busy);
		pbuf += len;	 
		ept_len -= len;
	}
	
	while(strLen--)
	{
		switch(*p)
		{
			case '0' :
				flash_addr = zone->pic_info->mem_addr;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[0]*2;
			break; 
			case '1' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[1] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[1]*2;
			break;
			case '2' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[2] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[2]*2;
			break;
			case '3' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[3] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[3]*2;
			break;
			case '4' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[4] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[4]*2;
			break;
			case '5' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[5] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[5]*2;
			break;
			case '6' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[6] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[6]*2;
			break;
			case '7' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[7] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[7]*2;
			break; 
			case '8' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[8] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[8]*2;
			break; 
			case '9' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[9] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[9]*2;
			break; 	
			case '-' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[10] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[10]*2;
			break; 	
			case '.' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[11] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[11]*2;
			break; 
			case ':' :
				flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[12] * zone->pic_info->heigth*2;
				len = zone->pic_info->heigth * zone->pic_info->entry_width[12]*2;
			break; 
		}
//		W25QXX_Read(pbuf, flash_addr, len);         //查询模式读取FLASH  
		pbuf = buf_sel ? pbuf_B : pbuf_A;
		W25QXX_DMA_Read(pbuf, flash_addr, len);
		while(buf_ready==0);
	
		buf_ready = 0;
		while(lcd_busy);
		User_LCD_WriteRAM(pbuf, len); 
		~buf_sel;
		p++; 
	}
	
//	LCD_ShowPic_DMA(zone->x, zone->y, zone->width, zone->heigth, lcd_buf, zone_range);
}

