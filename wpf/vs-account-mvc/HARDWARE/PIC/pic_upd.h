#ifndef __USER_LCD_H__
#define	__USER_LCD_H__

#include "../../SYSTEM/sys/sys.h"

typedef struct
{
	char	No;
	u16		width;								//图片宽度
	u16		heigth;								//图片高度
	u16 	entry_num;						//一张图片中元素的个数
	u16		entry_col_bgn[15];		//每个元素起始列数
	u16 	entry_width[15];			//每个元素的宽度
	
	u32 	mem_addr;							//图片地址
	u32 	mem_size; 						//图片占内存大小
}_PIC_INFO;

typedef struct
{
	char	No;
	u16 	x;										//区域X坐标
	u16		y;										//区域Y坐标
	u16		width;								//区域宽度
	u16		heigth;								//区域高度
	u8		*buf;									//区域颜色数据指针
	_PIC_INFO *pic_info;				//区域对应图片指针
}_ZONE;

void LCD_ShowZone(_ZONE *zone, u32 dvb, u8 alignDir);
void Show_Num(_ZONE *zone, char *p , u8 alignDir); 
#endif