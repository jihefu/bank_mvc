
/**
 * @file Account.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-11-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "Account.h"
#include "PC.h"
#include "../common/ConcurrentQueue.hpp"
#include "../model/Bank.h"
#include "../dao/AccountDao.h"  
#include "../model/BankOpenApi.h"  


extern AccountDao* g_accountDao;

 Account::~Account() {


}

void Account::init() {
	m_state= AccountState:: ACCOUNT_STATE_IDLE;
	OutMoneyChangeEvent();
}


void Account::OutMoneyChangeEvent() {
	AcountEventArgs  acountEvent;
	acountEvent.mId = m_id;
	acountEvent.mVal = m_money;
	acountEvent.mType = MONEY_CHANGE;
	bankSendEvent(acountEvent);
}


/**
 * @brief 取钱
 * @param money 
 */
void Account::drawMoney(uint32_t money) {
	uint32_t permitAmount = min(money, 20);
	permitAmount = min(permitAmount, m_money);
	if (permitAmount == 0) {
		return;
	}
	m_money = m_money - permitAmount;
	OutMoneyChangeEvent();
	return ;
}

/**
 * @brief 存库
 * @param money
 */
void Account::save() {
	AccountPOJO AccountPOJO;
	AccountPOJO.id = m_id;
	AccountPOJO.balance = m_money;
	g_accountDao->update(AccountPOJO);
}


/**
 * @brief 存钱
 * @param money 
 */
void Account::deposiMoney(uint32_t money) {

	uint32_t freeAmount =200-m_money;
	uint32_t permitAmount = min(money, freeAmount);
	if (permitAmount == 0) {
		return;
	}
	m_money = m_money + permitAmount;
	OutMoneyChangeEvent();
	return ;
}

/**
 * @brief 退出
 */
void Account::quit() {
	m_state = AccountState::ACCOUNT_STATE_IDLE;
	m_take_money = 0;
	save();
	lastQuitTime =TimeInMillisecond();
}



uint32_t Account::dealAccountServerMsg(AccountServerMsg accountServerMsg) {
	switch (m_state) {
		 default:{
		      
		 }
		//空闲
		case AccountState::ACCOUNT_STATE_IDLE: {
			switch (accountServerMsg.mType) {
				//取钱
				case AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT: {
					m_state = AccountState::ACCOUNT_STATE_DRAWING;
					drawMoney(accountServerMsg.mVal);
					m_take_money = m_take_money + accountServerMsg.mVal;
					break;
			    }
				//存钱
				case AccountServerType::ACCOUNTSERVER_REQ_PLUS: {
					m_state = AccountState::ACCOUNT_STATE_DEPOSITING;
					deposiMoney(accountServerMsg.mVal);
					break;
				}
			}
			break;
		}//取钱中
		case  AccountState::ACCOUNT_STATE_DRAWING: {
			switch (accountServerMsg.mType) {
				//取钱
				case AccountServerType::ACCOUNTSERVER_REQ_SUBTRACT: {
					m_state = AccountState::ACCOUNT_STATE_DRAWING;
					m_take_money = m_take_money + accountServerMsg.mVal;
					//超出20进入保护态
					if (m_take_money > 20) {
						m_state = AccountState::ACCOUNT_STATE_PROTECTED;
						return m_money;
					}
					drawMoney(accountServerMsg.mVal);
					break;
				}
				//退出
				case AccountServerType::ACCOUNTSERVER_REQ_QUIT: {
					quit();
					break;
				}
			}
			break;
		}//存钱中											 
		case AccountState::ACCOUNT_STATE_DEPOSITING: {
			switch (accountServerMsg.mType) {
				 //存钱
				case AccountServerType::ACCOUNTSERVER_REQ_PLUS: {
					m_state = AccountState::ACCOUNT_STATE_DEPOSITING;
					deposiMoney(accountServerMsg.mVal);
					break;
				}
				//退出
				case AccountServerType::ACCOUNTSERVER_REQ_QUIT: {
					quit();
					break;
				}
			}
			break;
		}//保护态											 
		case AccountState::ACCOUNT_STATE_PROTECTED: {
			//退出
			switch (accountServerMsg.mType) {
				//退出
				case AccountServerType::ACCOUNTSERVER_REQ_QUIT: {
					quit();
					break;
				}
			}
			break;
		}
	}
	return m_money;
}


