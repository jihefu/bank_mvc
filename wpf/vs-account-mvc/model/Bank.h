/**
 * @file Account.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-11-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once

#include <windows.h>
#include "stdint.h"
#include "../common/Event.hpp"
#include "../common/ConcurrentQueue.hpp"
#include "cxthread.h"
#include "Account.h"
#include<map>

using namespace std;



/**
 * @brief 账户用来处理消息的线程
 */
class DealAccountThread :public CxThread {

public:
	DealAccountThread() { }
	virtual ~DealAccountThread() {}
	void Run(void);
};

/**
 * @brief bank状态
 */
enum class BankState
{
	BANK_STATE_WORKING,
	BANK_STATE_CLOSE,
};

/**
 * @brief 银行
 * 
 */
class Bank {
	protected:
		BankState m_state; /*!< 银行状态 */
		DealAccountThread  m_dealAccountThread;     /*!< 处理外部请求消息的线程 */		
		map<uint32_t, Account*> m_account_map;   /*!< 账户的map */
	
	public:
		void start();
		void exit();
		void sendEvent(AcountEventArgs & evt); /*!< 向外发出事件 */
		uint32_t getTotalMoney();             /*!< 银行所有账户余额之和 */
		Account& getAcount(uint32_t id);      /*!< 从m_account_map获取Account */  
		Event<bool, AcountEventArgs> m_Event; /*!< 账户向外发出事件,viewModel要监听此事件 */
		ConcurrentQueue<AccountServerMsg> m_accountServerQueue;   /*!< 银行账户接收外部的消息队列 */
		static Bank& getInstance() {
			static Bank value_; 
			return value_;
		}
    private:
		Bank();
		~Bank();
		Bank(const Bank&);
		Bank& operator=(const Bank&);
};


