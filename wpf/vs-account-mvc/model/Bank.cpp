
#include "../model/Bank.h"
#include "../common/ConcurrentQueue.hpp"
#include "../dao/AccountDao.h"  


extern AccountDao * g_accountDao;

Bank::~Bank() {
	m_dealAccountThread.PostStopSignal();
}

Bank::Bank() {
	
}

void Bank::start() {
	m_dealAccountThread.SetThreadData((DWORD)this);
	m_dealAccountThread.Start();
}


void Bank::exit() {
	m_dealAccountThread.PostStopSignal();
}



void Bank::sendEvent(AcountEventArgs& evt) {
	m_Event.sendEvent(evt);
}


Account& Bank::getAcount(uint32_t accountId) {
	int c = m_account_map.count(accountId);
	if (c == 0) {
		AccountPOJO AccountPOJO= g_accountDao->getByUserId(accountId);
		Account * account = new Account(accountId, AccountPOJO.balance);
		m_account_map[accountId] = account;
	}
	return *m_account_map[accountId];
}


uint32_t Bank::getTotalMoney() {
	uint32_t totalMoney= g_accountDao->getTotalMoney();
	return totalMoney;
}


void DealAccountThread::Run(void) {
	m_running = TRUE;
	uint32_t cleanAccountDelayCount = 0;
	Bank* bank_p = (Bank*)GetThreadData();
	AccountServerMsg msg;
	while (TRUE) {
		//接收消息
		if (bank_p->m_accountServerQueue.pop(msg)){
		    bank_p->getAcount(msg.mId).dealAccountServerMsg(msg);
		}
		Sleep(5);
		if (m_stopSignal) {
			m_stopSignal = FALSE;
			break;
		}
	}
	m_running = FALSE;
}



void bankSendEvent(AcountEventArgs& evt) {
	Bank& g_Bank = Bank::getInstance();
	g_Bank.sendEvent(evt);
}