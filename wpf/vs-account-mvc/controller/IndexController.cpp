#include "../http/httplib.h"
#include <string>
#include <nlohmann/json.hpp>   
#include "../common/ResultUtils.hpp"
#include "../service/UserService.hpp"  
#include "../common/KeyEvent.hpp"
#include "../model/Bank.h"
using namespace httplib;
using json = nlohmann::json;
using namespace std;

extern Server g_svr;
extern Event<bool, KeyEventArgs> g_h5_key_Event;



void  indexController() {


    g_svr.Get("/account/queryMoney", [](const Request& req, Response& res) {
        Bank& g_Bank = Bank::getInstance();
        json j0;
        json j1;
        string idStr = req.get_param_value("id");
        uint32_t accountId=  atoi(idStr.c_str());
        j0["code"] = 0;
        j0["data"] = g_Bank.getAcount(accountId).getMoney();
        string s = j0.dump();
        res.set_content(s, "application/json;charset=utf-8");
    });


    g_svr.Get("/postH5event", [](const Request& req, Response& res) {
        string method = req.get_param_value("method");
        string idStr = req.get_param_value("id");
        uint32_t accountId = atoi(idStr.c_str());
        if (method._Equal("plusMoneyClick")) {
            KeyEventArgs keyArg = KeyEventArgs(accountId, KEY_EVENT_CLICK, KEY_VAL_KEY1);
            g_h5_key_Event.sendEvent(keyArg);
        }


        if (method._Equal("plusMoneyDbClick")) {
            KeyEventArgs keyArg = KeyEventArgs(accountId, KEY_EVENT_DBL_CLICK, KEY_VAL_KEY1);
            g_h5_key_Event.sendEvent(keyArg);
        }

        if (method._Equal("subtractMoneyClick")) {
            KeyEventArgs keyArg = KeyEventArgs(accountId, KEY_EVENT_CLICK, KEY_VAL_KEY2);
            g_h5_key_Event.sendEvent(keyArg);
        }

        if (method._Equal("subtractMoneyDbClick")) {
            KeyEventArgs keyArg = KeyEventArgs(accountId, KEY_EVENT_DBL_CLICK, KEY_VAL_KEY2);
            g_h5_key_Event.sendEvent(keyArg);
        }

        json r;
        r["code"] = 0;
        res.set_content(r.dump(), "application/json;charset=utf-8");
    });


}

