#include "../http/httplib.h"
#include <string>
#include <nlohmann/json.hpp>   
#include "../common/ResultUtils.hpp"
#include "../service/UserService.hpp"  
using namespace httplib;
using json = nlohmann::json;
using namespace std;


extern Server g_svr;


void  accountController() {


    /*
     *  �����˻�
     */
    g_svr.Get("/account/list", [](const Request& req, Response& res) {
        string keywords = req.get_param_value("keywords");
        AccountQuery accountQuery;
        accountQuery.username = keywords;
        m::Result r = UserService::listAll(accountQuery);
        res.set_content(r.dump().c_str(), "application/json;charset=utf-8");
    });


    /*
    *  ��½
    */
    g_svr.Post("/account/login", [](const auto& req, auto& res) {
        json bodyJson = json::parse(req.body);
        string username = bodyJson["username"];
        string password = bodyJson["password"];
        m::Result r = UserService::login(username, password);
        res.set_content(r.dump().c_str(), "application/json;charset=utf-8");
    });


    /*
     *  ע��
     */
    g_svr.Post("/account/regist", [](const auto& req, auto& res) {
        json bodyJson = json::parse(req.body);
        string username = bodyJson["username"];
        string password = bodyJson["password"];
        //username = StringUtils::UtfToGbk(username.c_str());
        m::Result r = UserService::regist(username, password);
        res.set_content(r.dump().c_str(), "application/json;charset=utf-8");
    });



}

