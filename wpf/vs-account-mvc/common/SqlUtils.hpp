#include <string>


using namespace std;

class SqlUtils
{
public:
	static string appendEqCase(const string& sql, const string& k, const string& v) {
		string rsql = sql + " and " + k + " ='" + v + "' ";
		return rsql;
	}

	static string appendLikeCase(const string& sql, const string& k, const string& v) {
		string rsql = sql + " and " + k + " like '%" + v + "%' ";
		return rsql;
	}
};