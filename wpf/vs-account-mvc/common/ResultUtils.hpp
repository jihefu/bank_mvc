#pragma once
#include <string>
#include <nlohmann/json.hpp>   
using json = nlohmann::json;
using namespace std;

namespace m {
	class Result
	{
	public:
	
		int code;
		json data;
		string msg;
		string dump()
		{
			json j;
			j["code"] = code;
			j["msg"] = msg;
			j["data"] = data;
			return j.dump();
		}


	};

	class ResultUtils
	{
	public:
		static Result successResult(json  data = {}) {
			Result r;
			r.code = 0;
			r.data = data;
			r.msg = "success";
			return r;
		}

		static Result failResult(string msg="fail") {
			Result r;
			json  data = {};
			r.code = -1;
			r.data = data;
			r.msg = msg;
			return r;
		}
	

	private:

	};


	

}



