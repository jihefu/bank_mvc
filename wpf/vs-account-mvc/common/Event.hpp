/**
 * @file Event.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-11-28
 *
 * @copyright Copyright (c) 2022
 *
 */
#pragma once

 /**
  * @brief 响应事件函数的类继承此类
  *
  */
class Object
{
};


/**
 * @brief 事件参数模板
 *
 * @tparam rtnTtpe  返回类型
 * @tparam ArgType  参数类型
 */
template<typename rtnTtpe, typename ArgType>
class Event
{
    //使每个事件最多关联响应的函数个数
#define EVENT_LIST_MAX_NUM  (10)
    typedef rtnTtpe(Object::* pMemFunc)(ArgType arg);

public:
    Event()
    {
        m_totalFunc = 0;
        m_obj = 0;
        for (int i = 0; i < EVENT_LIST_MAX_NUM; i++)
        {
            m_func[i] = 0;
        }
    }

    //关联回调成员函数
    template <class _func_type>
    void addEventListener(Object* obj, _func_type func)
    {
        m_obj = obj;
        m_func[m_totalFunc] = static_cast<pMemFunc> (func);
        m_totalFunc++;
    }
    //删除事件关联回调成员函数
    template <class _func_type>

    void removeEventListener(Object* obj, _func_type func)
    {
        if (obj != m_obj)
        {
            return;
        }

        //查找
        int i = 0;
        for (i = 0; i < m_totalFunc; i++)
        {
            if (m_func[i] == func)
            {
                break;
            }
        }

        //移动删除
        for (i; i < m_totalFunc - 1; i++)
        {
            m_func[i] = m_func[i + 1];
        }

        m_func[i] = 0;
        m_totalFunc--;
    }

    //执行关联的回调函数
    void sendEvent(ArgType arg)
    {
        for (int i = 0; i < m_totalFunc; i++)
        {
            if (m_func[i] != 0)
            {
                ((m_obj->*pMemFunc(m_func[i])))(arg);
            }
        }
    }
private:
    Object* m_obj;
    pMemFunc m_func[EVENT_LIST_MAX_NUM];
    int m_totalFunc;
};