#pragma once

#include "Event.hpp"
#include "stdint.h"

enum KeyEventEnum
{
	  KEY_EVENT_DOWN=	0x0001,
	  KEY_EVENT_UP		=0x0002,
	  KEY_EVENT_CLICK		=0x0101,
	  KEY_EVENT_DBL_CLICK	=0x0102,
	  KEY_EVENT_PRESSING	=0x0201,
	  KEY_EVENT_LONG_CLICK	=0x0202
};

enum KeyValEnum
{
		KEY_VAL_KEY0=	  0x0000,
		KEY_VAL_KEY1=	  0x0001,
		KEY_VAL_KEY2=	  0x0002,
		KEY_VAL_KEY3=	  0x0003,
		KEY_VAL_KEY4=	  0x0004,
		KEY_VAL_KEY5=	  0x0005,
		KEY_VAL_KEY6=	  0x0006,
		KEY_VAL_KEY7=	  0x0007
};


class KeyEventArgs {
public:
	KeyEventEnum mType;
	KeyValEnum mVal;
	uint32_t mId;
	KeyEventArgs(uint32_t id, KeyEventEnum type, KeyValEnum val) { mId = id;  mType = type; mVal = val; };
	virtual ~KeyEventArgs() {}
};





