#pragma once

#ifndef _typeh_
#define _typeh_

#include <stdio.h>


typedef int                 BOOL;
typedef unsigned short      WORD;
typedef unsigned long       DWORD;

typedef  long long s64;
typedef  long  s32;
typedef  short int  s16;
typedef char  s8;

typedef unsigned long  u32;
typedef unsigned short int u16;
typedef unsigned char  u8;
typedef unsigned  long long u64;



#endif