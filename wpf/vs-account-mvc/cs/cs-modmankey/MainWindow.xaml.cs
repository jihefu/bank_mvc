﻿using System;
using System.Data;
using System.Timers;
using System.Windows;
using System.Windows.Controls;

using cs_modmankey.Utils;
using cs_modmankey.Views;

namespace cs_modmankey
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private Timer m_timer = new Timer();


        public MainWindow()
        {
            InitializeComponent();
            this.m_timer.Interval = 10;
            this.m_timer.Elapsed += M_timer_Tick;
            m_timer.Start();

            CSVHelper.Test();
        }

        private void M_timer_Tick(object sender, EventArgs e)
        {
            long timeStraps= DateUtils.CurrentTimeMillis();
            drawBtn.OnUiTick(timeStraps);
            deposiBtn.OnUiTick(timeStraps);

        }


        int eventTotalCount = 0;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine($"------------------{eventTotalCount++}------------------------");
            if (sender is ModmanButton)
            {
                ModmanButton modmanBtn = (ModmanButton)sender;
                ModmanBtnRoutedEventArgs modEvent = (ModmanBtnRoutedEventArgs)e;
                string eName = "";
                switch (modEvent.modManKeyEvent)
                {
                    case ModManKeyEvent.EVT_KEY_DOWN: eName = "按下";break;
                    case ModManKeyEvent.EVT_KEY_UP:   eName = "松手"; break;
                    case ModManKeyEvent.EVT_CLICK:    eName = "单击"; break;
                    case ModManKeyEvent.EVT_DBL_CLICK: eName = "双击"; break;
                    case ModManKeyEvent.EVT_PRESSING:  eName = "长按"; break;
                    case ModManKeyEvent.EVT_LONG_CLICK: eName = "长击"; break;
                }
                if (modmanBtn.Name== "drawBtn")
                {
                    Console.WriteLine("存:" + eName);
                }
                if (modmanBtn.Name == "deposiBtn")
                {
                    Console.WriteLine("取:" + eName);
                }
            }
            else
            {
                Console.WriteLine("原始按键"+"点击");
            }
          
        }
    }
}
