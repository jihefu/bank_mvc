#define _CRT_SECURE_NO_WARNINGS

#include <easyx.h>
#include "easyxPort.h"

#include <stdio.h>


#define con_up_posotion      135, 320, 60, 50
#define con_down_posotion     135, 430, 60, 50



DWORD WINAPI keyServerHandel(LPVOID lpParameter);

_WORD_TypeDef mykey_info;

extern _WORD_TypeDef  g_key_info_temp;


bool isInRec(int x0, int y0,int x, int y, int w, int h)
{
    return (x0 >= x && x0 <= x + w && y0 >= y && y0 <= y + h);
}



void mybutton(int x, int y, int w, int h, TCHAR* text)
{
    setbkmode(TRANSPARENT);
    setfillcolor(GREEN);
    fillroundrect(x, y, x + w, y + h, 10, 10);
    // 输出字符串（MBCS 字符集）
    TCHAR s1[] = "黑体";
    settextstyle(30, 0, s1);
    TCHAR s[50] = "hello";
    int tx = x + (w - textwidth(text)) / 2;
    int ty = y + (h - textheight(text)) / 2;
    outtextxy(tx, ty, text);
}


void drawButton()
{
   
    TCHAR ups[50] = "存";
    mybutton(con_up_posotion, ups);

    TCHAR dps[50] = "取";
    mybutton(con_down_posotion, dps);


    //监听按键事件
    CreateThread(NULL, 0, keyServerHandel, NULL, 0, NULL);
}

void displayKey() {
    char buf[80];
    if (bt.one.lcd_ready_s == 0) {
        return;
    }

    sprintf(buf, " KEY:%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",
        g_key_info.one.b15,
        g_key_info.one.b14,
        g_key_info.one.b13,
        g_key_info.one.b12,
        g_key_info.one.b11,
        g_key_info.one.b10,
        g_key_info.one.b9,
        g_key_info.one.b8,
        g_key_info.one.b7,
        g_key_info.one.b6,
        g_key_info.one.b5,
        g_key_info.one.b4,
        g_key_info.one.b3,
        g_key_info.one.b2,
        g_key_info.one.b1,
        g_key_info.one.b0);
    fillroundrect(0, 640, 0 + 320, 640 + 30, 10, 10);
    outtextxy(0, 640, buf);
}

void displayLed(_WORD_TypeDef led_info) {
    char buf[80];
    if (bt.one.lcd_ready_s == 0) {
        return;
    }

    sprintf(buf, " LED:%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d",
        led_info.one.b15,
        led_info.one.b14,
        led_info.one.b13,
        led_info.one.b12,
        led_info.one.b11,
        led_info.one.b10,
        led_info.one.b9,
        led_info.one.b8,
        led_info.one.b7,
        led_info.one.b6,
        led_info.one.b5,
        led_info.one.b4,
        led_info.one.b3,
        led_info.one.b2,
        led_info.one.b1,
        led_info.one.b0);
    fillroundrect(0, 680, 0 + 320, 680 + 30, 10, 10);
    outtextxy(0, 680, buf);
}


void Ming_GUI_init() {

//1.创建绘图窗体 上面320*240  用于显示,下面是按钮
//使用easyx图形库中的initgraph函数创建一个绘图窗口并且设置宽和高
	initgraph(320, 720, EW_SHOWCONSOLE);
	//3.定义逻辑坐标
	//使用easyx图形库中的setorigin函数创建逻辑坐标起点
	setorigin(0, 0);
    //屏幕按钮分割线
	line(0, 241, 320, 241);
    //画按钮
    drawButton();

    g_key_info_temp.all = 0;

    bt.one.lcd_ready_s = 1;
    displayKey();
    displayLed(mykey_info);

}




uint32_t Ming_GUI_Point(uint32_t x, uint32_t y, uint16_t color)
{
     uint8_t r = (color   &  0xf800)>>11;
     uint8_t g = (color   &  0x07E0)>>5;
     uint8_t b = color   &  0x001f;
     COLORREF co = RGB(r << 3, g << 2,b << 3 );
	 putpixel(x, y, co);
	 return 1;
}



void Ming_GUI_close() {

    getchar();
	closegraph();
}


DWORD WINAPI keyServerHandel(LPVOID lpParameter)
{
    ExMessage msg;
    while (true) {
        if (peekmessage(&msg, EM_MOUSE)) {
            switch (msg.message)
            {
                //松开
                case WM_LBUTTONUP:
                    g_key_info.all = 0;
                    displayKey();
                //按下
                case WM_LBUTTONDOWN:

                    if (msg.vkcode == 0 || msg.vkcode == 128) {
                        continue;
                    }
                  
                    if (isInRec(msg.x, msg.y, con_up_posotion))
                    {
                        g_key_info.one.b1 = 1;
                    }
                   
                    if (isInRec(msg.x, msg.y, con_down_posotion))
                    {
                        g_key_info.one.b2 = 1;
                    }
                
                     displayKey();
                    continue;
            default:
                continue;
            }
        }

    }

    return 0;

}



void Ming_GUI_rectangle(uint32_t left, uint32_t top, uint32_t right, uint32_t bottom) {

    rectangle(left, top, right, bottom);

}