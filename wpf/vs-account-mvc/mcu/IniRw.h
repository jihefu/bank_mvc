/**
 * @file IniRw.h
 * @author ���˹�
 * @brief ini�ļ���д
 * @version 0.1
 * @date 2021-05-26
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef __INIRW_H_
#define __INIRW_H_
#include <string>
#include <map>
#include <vector>

using namespace std;

typedef struct __IniPrivateData
{
    string section;
    map<string, string> keyData;
    vector<string> notes;
}IniPrivateData;

typedef vector<IniPrivateData> IniData;

class IniRw
{

public:
    IniRw();
    IniRw(const string iniFile);
    ~IniRw();
    int Read(const string section, const string key, const int defaultval);
    string Read(const string section, const string key, const string defaultval);
    bool Save();
    bool SaveAs(const string otherIniFile);
    void writeInt(const string section, const string key, const int val);
    void writeString(const string section, const string key, const string val);
    static string readString(const string filename, const string section, const string key, const string defaultval);
    static int readInt(const string filename, const string section, const string key, const int defaultval);

public:
    IniData data;
    FILE* fd;
    string iniFile;
};






#endif
