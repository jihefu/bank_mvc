#if !defined(LOG_DEF_H_INC)
#define LOG_DEF_H_INC

#define LAUNCH_STEP			0x0100		// 启动，及以下正常步骤
#define RUN_MSG				0x0200		// 运行时信息
#define RUN_EVENT			0x0300		// 运行中发生指定事件
#define RUN_WARNING			0x0400		// 运行时警告
#define RUN_DEBUG			0x0500		// 运行时跟踪信息
#define ATS_LOADING				0x0600	// ATS正在加载+ATS句柄(下同)
#define ATS_LOAD_ABORT			0x0610	// ATS加载不成功
#define ATS_LOAD_OK				0x0620	// ATS加载成功
#define ATS_STARTING			0x0630	// ATS启动中
#define ATS_STARTED				0x0650	// ATS已经启动
#define ATS_BREAK				0x0660	// ATS(长时间动作)中断
#define ATS_CONTINUE			0x0670	// ATS(中断后)继续
#define ATS_CATCH_CONTROLLER	0x0680	// ATS抓住控制器
#define ATS_RELEASE_CONTROLLER	0x0690	// ATS释放控制器
#define ATS_ACTION			0x0700		// ATS进行中动作+句柄+轴
#define ATS_FINAL_ACTION	0x0D00		// ATS轴最终动作+句柄+轴
#define ATS_ABORT			0x0800		// ATS异常中止
#define ATS_OVER			0x0900		// ATS结束

#define SPECIMEN_CLAMPED	0x0A00		// 试件
#define SPECIMEN_LOADING	0x0A10
#define SPECIMEN_BROKEN		0x0A20
#define SPECIMEN_RELEASED	0x0A30
#define TEST_ELAS_OK		0x0B00		// 侦测参数
#define TEST_FFP_OK			0x0B10
#define HOTLINE_DROPPED		0x0C10		// 通讯掉线
#define HOTLINE_RESUMED		0x0C20		// 通讯恢复
#define API_DEBUG			0x0E00		// 应用调用接口跟踪

#define SYNC_STEP			0x0F00
#define SYNC_GROUP			0x0F10
#define SYNC_READY			0x0F20
#define SYNC_GO				0x0F30
#define SYNC_ABORT			0x0FA0
#define SYNC_OK				0x0FF0

#define LAUNCH_ABORT		0x1000		// 启动异常中止
#define LAUNCH_OK			0x2000		// 启动正常完成
#define CLOSE_STEP			0x4000		// 关闭，及以下步骤
#define CLOSE_OK			0x8000		// 关闭正常完成

#endif