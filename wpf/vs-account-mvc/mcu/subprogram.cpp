#define OS_GLOBALS
#define _CRT_SECURE_NO_WARNINGS
#include "../model/Account.h"
#include "main.h"
#include <stdio.h>
#include "w25qxx.h"
#include "Main_Constant.h"
#include "lcd.h"
#include "sys.h"
#include "user_lcd.h" 
#include "user_key.h" 
#include "../ucos/ucos.h"
#include "dvb.h"
#include "user_demo.h"
#include "../model/Bank.h"
#include "../viewModel/LcdAtmViewModel.h"
#include "../viewModel/H5AtmViewModel.h"
#include "../common/KeyEvent.hpp"




// 按键事件
extern Event<bool, KeyEventArgs> g_key_Event;
// h5事件
extern Event<bool, KeyEventArgs> g_h5_key_Event;

//液晶view
extern LcdAtmViewModel g_lcdAtmViewModel;
// h5 view
extern H5AtmViewModel g_h5AtmViewModel;

uint32_t cur_accountId=1;


//1ms 中断调用一次
void SysTimeTick()
{
	OSTimeTick();
	bt.one.ms1_s = 1;
	g_system_counter++;
	if (g_system_counter >= 5) {
		g_system_counter = 0;
		bt.one.ms5_s = 1;
	}
}



void disPlay() {
	disPlayDvb(CON_ZONE_D2, CON_PIC_ASCII_48, ALIGN_LEFT, CON_BLACK_GREEN_COLOR, 0);
}



void  mcu_init() {

	//串口1接收队列
	spitRingBuffSpitInit(
		&g_uart1SpitRingBuff,
		&g_uart1SpitRingBuff_buffer[0],
		sizeof(g_uart1SpitRingBuff_buffer),
		&g_uart1SpitRingBuff_packet_len_buffer[0],
		sizeof(g_uart1SpitRingBuff_packet_len_buffer)
	);

	//事件队列
	spitRingBuffSpitInit(
		&g_msgMqSpitRingBuff,
		&g_msgMqSpitRingBuff_buffer[0],
		sizeof(g_msgMqSpitRingBuff_buffer),
		&g_msgMqSpitRingBuff_packet_len_buffer[0],
		sizeof(g_msgMqSpitRingBuff_packet_len_buffer)
	);
	//按键初始化	
	userKeyInit();
	

	g_zone_list[CON_ZONE_D2].heigth = 48;
	g_zone_list[CON_ZONE_D2].width = 200;


}



extern  Account g_account; 


void mcu_task()
{
	W25QXX_Init();
	LCD_Init();
	Init_Zone();
	mcu_init();
	g_key_cousumer_task_handle = OSTaskCreate(key_cousumer_task, 0);
	g_key_scan_task_handle = OSTaskCreate(user_key_scan_task, 0);
	while (1) {
		if (bt.one.ms5_s == 1) {
			bt.one.ms5_s = 0;
			disPlay();
		}
		Sleep(1);
	}
}


