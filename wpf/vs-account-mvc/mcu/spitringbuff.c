#include "spitringBuff.h"

#include "stddef.h"

/**
* @brief 写入1个包
* @retval 0 succss   -1fail
*/
int8_t spitRingBuffWriteBuffer(SpitRingBuff_TypeDef* spitRingBuff, uint8_t* data, uint8_t len)
{
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->lock();
#endif
	//包的数量超过容量则不再写入
	if (spitRingBuff->package_count >= spitRingBuff->packet_len_buffer_size)
	{
#if	con_SpitRingBuff_lock_enable
		spitRingBuff->unlock();
#endif
		return -1;
	}
	uint8_t* p = data;
	spitRingBuff->packet_len_buffer[spitRingBuff->p_w_index] = len;
	if (++spitRingBuff->p_w_index >= spitRingBuff->packet_len_buffer_size) spitRingBuff->p_w_index = 0;
	uint8_t i;
	if (spitRingBuff->tail + len < spitRingBuff->ring_buff_size)
	{
		for (i = 0; i < len; ++i)
		{
			spitRingBuff->ring_buff[spitRingBuff->tail] = *p;
			++spitRingBuff->tail;
			++p;
		}
	}
	else
	{
		uint8_t size = spitRingBuff->ring_buff_size - spitRingBuff->tail;
		for (i = 0; i < size; ++i)
		{
			spitRingBuff->ring_buff[spitRingBuff->tail] = *p;
			++spitRingBuff->tail;
			++p;
		}
		spitRingBuff->tail = 0;
		for (i = size; i < len; ++i)
		{
			spitRingBuff->ring_buff[spitRingBuff->tail] = *p;
			++spitRingBuff->tail;
			++p;
		}
	}
	++spitRingBuff->package_count;
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->unlock();
#endif
	return 0;
}

/**
* @brief
* @param  len 读到的长度
* @retval 0 succss   -1fail
*/
int8_t spitRingBuffRead(SpitRingBuff_TypeDef* spitRingBuff, uint8_t* rData, uint8_t* len)
{
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->lock();
#endif
	if (spitRingBuff->package_count == 0)
	{
#if	con_SpitRingBuff_lock_enable
		spitRingBuff->unlock();
#endif
		return -1;
	}
	uint8_t* p = rData;
	*len = spitRingBuff->packet_len_buffer[spitRingBuff->p_r_index];
	if (++spitRingBuff->p_r_index >= spitRingBuff->packet_len_buffer_size) spitRingBuff->p_r_index = 0;
	uint8_t i;

	if (spitRingBuff->head + *len < spitRingBuff->ring_buff_size)
	{
		for (i = 0; i < *len; ++i)
		{
			*p = spitRingBuff->ring_buff[spitRingBuff->head];
			++spitRingBuff->head;
			++p;
		}
	}
	else
	{
		uint8_t size = spitRingBuff->ring_buff_size - spitRingBuff->head;
		for (i = 0; i < size; ++i)
		{
			*p = spitRingBuff->ring_buff[spitRingBuff->head];
			++spitRingBuff->head;
			++p;
		}
		spitRingBuff->head = 0;
		for (i = size; i < *len; ++i)
		{
			*p = spitRingBuff->ring_buff[spitRingBuff->head];
			++spitRingBuff->head;
			++p;
		}
	}
	--spitRingBuff->package_count;
	*p = 0;
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->unlock();
#endif
	return 0;
}




/**
* @brief 仅仅查看队列内容
* @param
* @retval 0 succss   -1fail
*/
int8_t spitRingBuffPreview(SpitRingBuff_TypeDef* spitRingBuff, uint8_t* rData, uint8_t* len)
{
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->lock();
#endif
	//队列为空
	if (spitRingBuff->package_count == 0)
	{
#if	con_SpitRingBuff_lock_enable
		spitRingBuff->unlock();
#endif
		return -1;
	}
	uint8_t i;
	uint8_t* p = rData;
	if (spitRingBuff->head < spitRingBuff->tail) {
		*len = spitRingBuff->tail - spitRingBuff->head;
	}
	else {
		*len = spitRingBuff->ring_buff_size - spitRingBuff->head + spitRingBuff->tail;
	}
	if (spitRingBuff->head + *len < spitRingBuff->ring_buff_size)
	{
		for (i = 0; i < *len; ++i)
		{
			*p = spitRingBuff->ring_buff[spitRingBuff->head + i];
			++p;
		}
	}
	else
	{
		uint8_t size = spitRingBuff->ring_buff_size - spitRingBuff->head;
		for (i = 0; i < size; ++i)
		{
			*p = spitRingBuff->ring_buff[spitRingBuff->head + i];
			++p;
		}
		uint8_t inx = 0;
		for (i = size; i < *len; ++i)
		{
			*p = spitRingBuff->ring_buff[inx++];
			++p;
		}
	}
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->unlock();
#endif
	return 0;
}






void spitRingBuffSpitInit(
	SpitRingBuff_TypeDef* spitRingBuff,
	uint8_t* ring_buff,
	uint8_t ring_buff_size,
	uint8_t* packet_len_buffer,
	uint8_t packet_len_buffer_size
) {
	spitRingBuff->package_count = 0;
	spitRingBuff->head = 0;
	spitRingBuff->tail = 0;
	spitRingBuff->p_r_index = 0;
	spitRingBuff->p_w_index = 0;
	spitRingBuff->ring_buff = ring_buff;
	spitRingBuff->ring_buff_size = ring_buff_size;
	spitRingBuff->packet_len_buffer = packet_len_buffer;
	spitRingBuff->packet_len_buffer_size = packet_len_buffer_size;
	spitRingBuff->ring_buff = ring_buff;
}



#if con_SpitRingBuff_Write_byte_enable
/**
* @brief 写入1个byte
* @retval 0 succss   -1fail
**/
int8_t spitRingBuffWriteChar(SpitRingBuff_TypeDef* spitRingBuff, uint8_t d) {
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->lock();
#endif
	//如果按行读,则写入一个包
	if (spitRingBuff->enableReadLine == 1 && d == '\n') {
		if (spitRingBuff->g_spitringPos)
		{
			spitRingBuffWriteBuffer(spitRingBuff, spitRingBuff->g_spitringBuff, spitRingBuff->g_spitringPos);
		}
		spitRingBuff->g_spitringPos = 0;
#if	con_SpitRingBuff_lock_enable
		spitRingBuff->unlock();
#endif
		return 0;
	}
	if (spitRingBuff->g_spitringPos >= spitRingBuff->g_spitringBuff_size) {
#if	con_SpitRingBuff_lock_enable
		spitRingBuff->unlock();
#endif
		return -1;
	}
	spitRingBuff->g_spitring_timeout_count = 0;
	spitRingBuff->g_spitringBuff[spitRingBuff->g_spitringPos] = d;
	spitRingBuff->g_spitringPos = (spitRingBuff->g_spitringPos) + 1;
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->unlock();
#endif
	return 0;
}


/**
* @brief 1ms 调用1次的分包函数
**/
void spitRingBuffSpitPackHandel(SpitRingBuff_TypeDef* spitRingBuff) {
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->lock();
#endif
	spitRingBuff->g_spitring_timeout_count++;
	//100ms未写入,则分包
	if (spitRingBuff->g_spitring_timeout_count > spitRingBuff->g_spitring_timeout_limit) {
		spitRingBuff->g_spitring_timeout_count = 0;
		if (spitRingBuff->g_spitringPos)
		{
			spitRingBuffWriteBuffer(spitRingBuff, spitRingBuff->g_spitringBuff, spitRingBuff->g_spitringPos);
			spitRingBuff->g_spitringPos = 0;
		}
	}
#if	con_SpitRingBuff_lock_enable
	spitRingBuff->unlock();
#endif

}
#endif