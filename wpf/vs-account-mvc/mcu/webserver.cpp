#define _CRT_SECURE_NO_WARNINGS

#include "../http/httplib.h"
#include "Webserver.h"
#include <nlohmann/json.hpp>   
using namespace httplib;
using json = nlohmann::json;
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <vector> 
#include <string>
#include <map>
#include <sstream>
#include "PC.h"

#include "../controller/controller.h"


using namespace std;



class EventDispatcher {
public:
    EventDispatcher() {
    }

    void wait_event(DataSink* sink) {
        unique_lock<mutex> lk(m_);
        int id = id_;
        cv_.wait(lk, [&] { return cid_ == id; });
        if (sink->is_writable()) { sink->write(message_.data(), message_.size()); }
    }

    void send_event(const string& message) {
        lock_guard<mutex> lk(m_);
        cid_ = id_++;
        message_ = message;
        cv_.notify_all();
    }

private:
    mutex m_;
    condition_variable cv_;
    atomic_int id_{ 0 };
    atomic_int cid_{ -1 };
    string message_;
};

const auto html = R"(
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>SSE demo</title>
</head>
<body>
<script>
const ev1 = new EventSource("event1");
ev1.onmessage = function(e) {
  console.log('ev1', e.data);
}
const ev2 = new EventSource("event2");
ev2.onmessage = function(e) {
  console.log('ev2', e.data);
}
</script>
</body>
</html>
)";




bool icompare_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) == std::tolower(b);
}
string readTxt(string file)
{
    string res;
    ifstream infile;
    infile.open(file.data());   //将文件流对象与文件连接起来 
    assert(infile.is_open());   //若失败,则输出错误消息,并终止程序运行 
    string s;
    while (getline(infile, s))
    {
        res += s+"\n";
    }
    infile.close();             //关闭文件输入流 
    return res;
}

bool compare_pred(unsigned char a, unsigned char b) {
    return std::tolower(a) == std::tolower(b);
}
bool EndsWith(const std::string& str, const std::string& suffix) {

    if (str.size() < suffix.size()) {
        return false;
    }

    std::string tstr = str.substr(str.size() - suffix.size());

    if (tstr.length() == suffix.length()) {
        return std::equal(suffix.begin(), suffix.end(), tstr.begin(), icompare_pred);
    }
    else {
        return false;
    }
}


BOOL DirExists(LPCTSTR szFilePath)
{
    if (::GetFileAttributes(szFilePath) == FILE_ATTRIBUTE_DIRECTORY) {
        return TRUE;
    }
  
    return FALSE;
}

EventDispatcher ed;

Server g_svr;

void starServer(void)
{



    g_svr.set_default_headers({
            { "Access-Control-Allow-Origin", "http://localhost:8888" },
            { "Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE" },
            { "Access-Control-Max-Age", "3600" },
            { "Content-Type", "application/json;charset=utf-8" },
              { "Access-Control-Allow-Credentials", "true" },
              { "Access-Control-Allow-Headers", "lj_token,Content-Type,X-Requested-With" }
        });


    g_svr.Get(R"(/www/(.*?))", [](const Request& req, Response& res) {
        char c[200];
        string s ="."+ req.path;
        strcpy(c, s.c_str());
         int exist= access(c, 0);
         if (exist == -1) {
             res.set_content("1", "text/html");
             return;
         }

        string s1 = readTxt(s);
        string ctType = "text/html";
        if (EndsWith(req.path,"html")) {
            res.set_content(s1, "text/html");
        }
        else  if (EndsWith(req.path, "json")) {
            res.set_content(s1, "application/json");
        }
        else  if (EndsWith(req.path, "jsx")) {
            res.set_content(s1, "application/javascript");
        }
        else if (EndsWith(req.path, "js")) {
            res.set_content(s1, "application/javascript");
        }else if (EndsWith(req.path, "css")) {
            res.set_content(s1, "text/css");
        }
        else {
            res.set_content(s1, "text/html");
        }
        
    });
    g_svr.Get("/test001", [](const Request& req, Response& res) {
        json j0;
        j0["money"] = 5;
        string s = j0.dump();
        sseSend(s.c_str());
        res.set_content(s, "application/json;charset=utf-8");
    });


    g_svr.Get("/modbusData", [](const Request& req, Response& res) {
        json j0;
        json j1;
        for (int i = 0; i <= 4096; i++) {
            if (i % 16 == 0) {
                if (i > 0) {
                  j0.push_back(j1);
                }
                if (i == 4096) {
                    break;
                }
                j1 = {};
            }
        }
        string s = j0.dump();
        res.set_content(s, "application/json;charset=utf-8");
    });

    

    g_svr.Options("/modbusData", [](const Request& req, Response& res) {
        res.set_content("", "application/json;charset=utf-8");
    });
    g_svr.Options("/updateModbusData", [](const Request& req, Response& res) {
        res.set_content("", "application/json;charset=utf-8");
    });

    g_svr.Get("/event1", [&](const Request& /*req*/, Response& res) {
        cout << "\n connected to event1..." << endl;
        res.set_chunked_content_provider("text/event-stream",
            [&](size_t /*offset*/, DataSink& sink) {
            ed.wait_event(&sink);
            return true;
        });
    });
    /*
    thread t([&] {
        int id = 0;
        while (true) {
            this_thread::sleep_for(chrono::seconds(1));
            cout << "send event: " << id << std::endl;
            std::stringstream ss;
            ss << "data: " << id << "\n\n";
            ed.send_event(ss.str());
            id++;
        }
    });
    */
    g_svr.Get("/stop", [&](const Request& req, Response& res) {
        g_svr.stop();
    });



    accountController();
    indexController();


    cout << "webServer listern on:7001" <<endl;
    g_svr.listen("localhost", 7001);
 
}

void  sseSend(const char* msg) {
 
    std::stringstream ss;
    ss << "data: " << msg << "\n\n";
    ed.send_event(ss.str());
}