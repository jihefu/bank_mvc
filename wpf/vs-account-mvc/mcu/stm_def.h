#ifndef STM_DEF_H_INC
#define STM_DEF_H_INC

#define CMD_ECHO				0x0001			// [W]简单应答
#define CMD_ACQ_START			0x0004			// [W]启动隐式数据传送
#define CMD_ACQ_STOP			0x0005			// [W]停止隐式数据传送

#define CMD_RD_FIRM_VER			0x0400			// 读取固件版本，也可用于初始化	
#define CMD_RD_EETABLE			0x0410
#define CMD_WR_EE				0x0420			// 共16个WORD
#define CMD_SAVE_EETABLE		0x0430
#define CMD_RD_ESTRING			0x0440			// 只有一条，最长15个字节
#define CMD_WR_ESTRING			0x0450
#define CMD_RD_ENUMBER			0x0460			// 共4个DWORD
#define CMD_WR_ENUMBER			0x0470
#define CMD_OUT_BUNDLE			0x0480			// [W+..]捆绑输出PAD

#define CMD_RD_MAN_MODEL		0x0401			// 手控器型号
#define CMD_RD_MAN_VER			0x0402			// 手控器版本
#define EVT_KEY_CHANGED			0x0490			// [W+]按键掩码
#define CMD_DISPLAY_LIGHT(p)	(0x04A0+(p))	// [WW]LED灯掩码
#define CMD_DISPLAY_ICON		0x04B0			// [WW]状态图标，组号+图标号
#define CMD_DISPLAY_DVB(i)		(0x04C0+(i))	// [WV+]数码，DVB码
#define CMD_DISPLAY_UNIT(i)		(0x04D0+(i))	// [WW]单位，统一单位编码
#define CMD_DISPLAY_FUNCTION	0x04F0			// [W..]三个功能键名的字符串

#endif