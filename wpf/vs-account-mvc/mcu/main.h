/*
*********************************************************************************************************


*********************************************************************************************************
* File : mcu.h
* By : Minglie
* Date :
*********************************************************************************************************
*/
/* Function header file, all the function be used in program */


#ifndef __main_H
#define __main_H

#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include "PC.h"
#include "../ucos/ucos.h"
#include "Main_Constant.h"

#define GPIOA               "A"
#define GPIOB                "B"
#define GPIOC             "C"
#define GPIOD               "D"
#define GPIOE               "E"
#define GPIOF              "F"
#define GPIO_PIN_0                 0
#define GPIO_PIN_1                1
#define GPIO_PIN_2                2
#define GPIO_PIN_3               3
#define GPIO_PIN_4                4
#define GPIO_PIN_5               5
#define GPIO_PIN_6                6
#define GPIO_PIN_7              7
#define GPIO_PIN_8                8
#define GPIO_PIN_9               9
#define GPIO_PIN_10               10
#define GPIO_PIN_11               11
#define GPIO_PIN_12              12
#define GPIO_PIN_13              13
#define GPIO_PIN_14               14
#define GPIO_PIN_15              15
#define GPIO_PIN_All              16

#define SHOWLOGO
#define TIM3_Disable() ucos_task[tim3TaskIndex].one.enable=0
#define TIM3_Enable() ucos_task[tim3TaskIndex].one.enable= 1 
#define delay_ms(a) Sleep(a)


#ifdef __cplusplus 
extern "C"
{
#endif

OS_EXT      int g_nop_port;
extern  uint8_t tim3TaskIndex; 
OS_EXT _WORD_TypeDef g_key_info_temp;
extern uint8_t ModeBusRtuDeCode();


#ifdef __cplusplus 
}
#endif



#endif
