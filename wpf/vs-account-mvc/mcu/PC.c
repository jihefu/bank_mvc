
/*
*********************************************************************************************************


*********************************************************************************************************
* File : PC.c
* By : Minglie
* Date :
*********************************************************************************************************
*/


#include <sys/timeb.h>
#include <windows.h>
#include <stdio.h>
#include "Main_Constant.h"
#include "Function.h"
#include "../ucos/ucos.h"
#include "PC.h"
#include "Webserver.h"
#include "AppConfig.h"
#include "main.h"

HANDLE hCom;//串口句柄
HANDLE HRead, HWrite, HWebServer,HeatBeatTimeHandel;

uint8_t 	uartRxBuf[4104];
uint8_t 	uartTxBuf[50];
uint8_t 	uartRxBufTemp[50];
uint8_t     tim3Enable;
uint8_t     tim3TaskIndex;


//模拟定时器3的1.5s中断
void tim3_fun_task()
{
    WHILE(1) {
        //关闭定时器3
        ucos_task[tim3TaskIndex].one.enable = 0;
    
       
    }
}

//灯
void led_fun_task()
{
    WHILE(1) {
        if (bt.one.initIsDone == 0) {
            return;
        }
        displayLed(g_led_info.all);
        //printf("Z5=%d \n", g_zon_list[5].param);
        OSTimeDly(200);
    }
}

void pc_mcu_init() {
    OSTaskCreate(led_fun_task,0);
    Sleep(1);
}

void pc_init() {
    serial_init();
    pc_mcu_init();
}


uint16_t uart_write(uint8_t uart_id, const uint8_t* buf, uint16_t size)
{
    RS485_Send_Data(buf, size);
    return size;
}

u8 uart1_send(u8* buf, u16 size) {
    RS485_Send_Data(buf, size);
    return size;
}


void uart_poll_dma_tx(uint8_t uart_id) {


}







//同上   
void CharToTchar(const char* _char, TCHAR* tchar)
{
    int iLength;

    iLength = MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, NULL, 0);
    MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, tchar, iLength);
}


/* fifo上锁函数 */
static void fifo_lock(void)
{

}

/* fifo解锁函数 */
static void fifo_unlock(void)
{
   
}



void serial_init() {

    AppConfig_TypeDef appConfig = getAppConfig();
    u16 BaudRate = _atoi64(appConfig.BaudRate);
    char comname[100];
   // CharToTchar(appConfig.ComPort, comname);
    hCom = CreateFile(appConfig.ComPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    // 获取和设置串口参数
    DCB myDCB;
    GetCommState(hCom, &myDCB);  // 获取串口参数
    fflush(stdout);
    myDCB.BaudRate = 112500;       // 波特率
    myDCB.Parity = NOPARITY;   // 校验位
    myDCB.ByteSize = 8;          // 数据位
    myDCB.StopBits = ONESTOPBIT; // 停止位
    SetCommState(hCom, &myDCB);  // 设置串口参数
    printf("serial_init on %s:%s\n", appConfig.ComPort, appConfig.BaudRate);
}


void pc_serial_sendDat(char dat[]) {

    u32 strLength = strlen(dat);
    WriteFile(hCom, dat, strLength, &strLength, NULL);
    PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);
}

//RS485发送len个字节.
//buf:发送区首地址
//len:发送的字节数(为了和本代码的接收匹配,这里建议不要超过64个字节)
void RS485_Send_Data(u8* buf, u8 len)
{
    WriteFile(hCom, buf, len, &len, NULL);
    PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);

}


DWORD WINAPI threadWebServer(LPVOID lpParameter)
{
    starServer();
    return 0;
}



DWORD WINAPI threadWrite(LPVOID lpParameter)
{
    if (hCom == INVALID_HANDLE_VALUE) {
        puts("打开串口失败");
        return 0;
    }
    DWORD strLength = 0;
    while (1)
    {

        Sleep(1000);
    }
    return 0;
}

DWORD WINAPI heatBeatTime(LPVOID lpParameter)
{
    while (1)
    {
        SysTimeTick();
        SleepUs(1000);
    }
    return 0;
}




DWORD WINAPI threadRead(LPVOID lpParameter)
{
    if (hCom == INVALID_HANDLE_VALUE)   //INVALID_HANDLE_VALUE表示出错，会设置GetLastError
    {
        puts("打开串口失败");
        return 0;
    }
    // 利用错误信息来获取进入串口缓冲区数据的字节数
    DWORD dwErrors;     // 错误信息
    COMSTAT Rcs;        // COMSTAT结构通信设备的当前信息
    int recv_total_size = 0;
    DWORD length = 100;               //用来接收读取的字节数
    while (1) {
   
        ClearCommError(hCom,
            &dwErrors,
            &Rcs);                                // 获取读缓冲区数据长度
        recv_total_size = Rcs.cbInQue;
        ReadFile(hCom, uartRxBuf, recv_total_size, &length, NULL);   //获取字符串
        PurgeComm(hCom, PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT| PURGE_TXABORT);  //清空缓冲区
        if (recv_total_size <50 && recv_total_size>0) {
            for (int i = 0; i < recv_total_size; i++) {
                printf("%02x ", uartRxBuf[i]);
            }
            printf("\n");

            if (
                     (g_mcu_state >=FACTORY_MAIN || recv_total_size > 30)
                     ||
                     (uartRxBuf[0] != g_config_info.modbusSlaverAddress &&uartRxBuf[0] != 0 )
                ) {
                continue;
            }
            spitRingBuffWriteBuffer(&g_uart1SpitRingBuff, uartRxBuf, recv_total_size);
        }

        Sleep(5);
    }

    return 0;
}



void pc_createHandle() {

    //HWrite = CreateThread(NULL, 0, threadWrite, NULL, 0, NULL);
    HRead = CreateThread(NULL, 0, threadRead, NULL, 0, NULL);
    HeatBeatTimeHandel=   CreateThread(NULL, 0, heatBeatTime, NULL, 0, NULL);
    HWebServer = CreateThread(NULL, 0, threadWebServer, NULL, 0, NULL);
}



void pc_closeHandle() {
   // CloseHandle(HRead);
    CloseHandle(HWrite);
    CloseHandle(HeatBeatTimeHandel);
   // CloseHandle(HWebServer);
}




u32 TimeInMillisecond(void) {

    struct timeb time_buffer;
    ftime(&time_buffer);
    return time_buffer.time * 1000LL + time_buffer.millitm;

}


void HAL_GPIO_TogglePin(char* GPIOx, uint16_t GPIO_Pin) {

    static long t1[16] = {0};
    printf("TogglePin.%s:%d:%u \n", GPIOx, GPIO_Pin, TimeInMillisecond() - t1[GPIO_Pin]);
    t1[GPIO_Pin] = TimeInMillisecond();
}




int SleepUs(long us)
{
    //储存计数的联合
    LARGE_INTEGER fre;
    //获取硬件支持的高精度计数器的频率
    if (QueryPerformanceFrequency(&fre))
    {
        LARGE_INTEGER run, priv, curr, res;
        run.QuadPart = fre.QuadPart * us / 1000000;//转换为微妙级
        //获取高精度计数器数值
        QueryPerformanceCounter(&priv);
        do
        {
            QueryPerformanceCounter(&curr);
        } while (curr.QuadPart - priv.QuadPart < run.QuadPart);
        curr.QuadPart -= priv.QuadPart;
        int nres = (curr.QuadPart * 1000000 / fre.QuadPart);//实际使用微秒时间
        return nres;
    }
    return -1;//
}


uint32_t HAL_GetTick() {
    u32 r = TimeInMillisecond();
    return r;
}