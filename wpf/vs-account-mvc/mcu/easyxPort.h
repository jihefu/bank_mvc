
#pragma once
#ifndef easyxPort_h
#define easyxPort_h

#include <stdint.h>

#ifdef __cplusplus 
extern "C"
{
#endif

   #include "Main_constant.h"
   OS_EXT _WORD_TypeDef g_key_info_temp;
	//光标
	typedef struct
	{
		uint16_t		x;
		uint16_t		y;
	}Ming_Cur;


	//光标
	typedef struct
	{
		uint16_t		xmin;
		uint16_t		ymin;
		uint16_t		xmax;
		uint16_t		ymax;
	}Ming_Cur_BOX;



	void Ming_GUI_init();

	void Ming_GUI_close();

	u32 keyScan();

	u32 key_scan();

	void displayLed(_WORD_TypeDef ledStatus);

	void  displayKey();

	//给界面发送消息
	uint32_t Ming_GUI_Point(uint32_t x, uint32_t y, uint16_t color);
	void Ming_GUI_rectangle(uint32_t left, uint32_t top, uint32_t right, uint32_t bottom);

#ifdef __cplusplus 
}
#endif

#endif
