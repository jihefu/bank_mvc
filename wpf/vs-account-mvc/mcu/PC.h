/*
*********************************************************************************************************


*********************************************************************************************************
* File : PC.h
* By : Minglie
* Date :
*********************************************************************************************************
*/

/* Function header file, all the function be used in program */

#ifndef __PC_H
#define __PC_H
#include "stdint.h"
#include "sys.h"

#ifdef __cplusplus 
extern "C"
{
#endif
	  void mcu_task();
	   void SysTimeTick();
	   void  pc_createHandle();
	   void  pc_closeHandle();
		void pc_init();
		void serial_init();
		void flash_init();
		void RS485_Send_Data(u8* buf, u8 len);
		//���غ���ʱ���
		u32 TimeInMillisecond(void);
		//us ��ʱ
		int SleepUs(long us);
		void HAL_GPIO_TogglePin(char* GPIOx, uint16_t GPIO_Pin);
		u8 uart1_send(u8* buf, u16 size);
		uint32_t HAL_GetTick();
#ifdef __cplusplus 
}
#endif


#endif





