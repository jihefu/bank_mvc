#ifndef MAN_DEF_H_INC
#define MAN_DEF_H_INC

// 单个字节手控器指令
#define CID_PANEL_LIGHT		0x2D		// 更新指示灯，1字节位置（前面板/手控器/台面板），2字节位掩码
#define CID_MAN_ICON		0x2E		// 更新手控器液晶单个图标，2字节参数(位置+下标)
#define CID_INQ_MAN_VER		0x2F		// 查询手控器固件版本		
//#define CID_INQ_VER 		0x30		// 查询PCIE版本号
#define CID_MAN_KEY_CHANGED	0x31		// 报告手控器按键变动，掩码，最多8键
#define CID_MAN_DVB_0_L		0x32		// 更新液晶数码
#define CID_MAN_DVB_0_H		0x33
#define CID_MAN_DVB_1_L		0x34
#define CID_MAN_DVB_1_H		0x35
#define CID_MAN_DVB_2_L		0x36
#define CID_MAN_DVB_2_H		0x37
#define CID_MAN_DVB_3_L		0x38
#define CID_MAN_DVB_3_H		0x39
#define CID_MAN_UNIT_0		0x3A		// 更新单位，2字节单位编码
#define CID_MAN_UNIT_1		0x3B
#define CID_MAN_UNIT_2		0x3C
#define CID_MAN_UNIT_3		0x3D

#endif
