#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<fstream>
#include"AppConfig.h"
#include"IniRw.h"
#include <direct.h>

 unsigned short flash_ecbm_modbus_pmd_buf[4096];

bool ReadAllFile(const std::string& strFileName, std::string& strFileData)
{
    std::ifstream in(strFileName, std::ios::in | std::ios::binary);
    if (!in.is_open())
    {
        return false;
    }
    std::istreambuf_iterator<char> beg(in), end;
    strFileData = std::string(beg, end);
    in.seekg(0, std::ios::end);//移动的文件尾部
    int strFileSize = in.tellg();//获取文件的整体大小
    in.close();
    return true;
}



AppConfig_TypeDef getAppConfig() {
    AppConfig_TypeDef at;
    IniRw iniRw= IniRw("appConfig.ini");
    IniData data = iniRw.data;
    map<string, string> keyData= data[0].keyData;
    strcpy(at.ComPort, keyData["ComPort"].c_str());
    strcpy(at.BaudRate, keyData["BaudRate"].c_str());
    return at;
}

IniRw iniRw = IniRw("stm32flash.ini");

unsigned short * app_config_get_ecbm_modbus_pmd_flash() {
    AppConfig_TypeDef at;
    IniData data = iniRw.data;
    map<string, string> keyData = data[0].keyData;
    for (int i = 0; i < 4096; i++) {
        string k = "a" + to_string(i);
        string v = keyData[k];
        char* charNum = (char*)v.data();
        unsigned short vsNum = (unsigned short)atoi(charNum);
        flash_ecbm_modbus_pmd_buf[i] = vsNum;
    }
 
    return flash_ecbm_modbus_pmd_buf;
}

void app_config_set_ecbm_modbus_pmd_flash(unsigned short addr, unsigned short val) {
    if (addr < 4096 && addr >= 0) {
        iniRw = IniRw("stm32flash.ini");
        string k = "a" + to_string(addr);
        unsigned short v = val;
        iniRw.writeInt("flash", k, v);
        iniRw.Save();
    }
}