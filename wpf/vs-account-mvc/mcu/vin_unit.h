#ifndef VIN_UNIT_H
#define VIN_UNIT_H

#define UNIT_M		100
#define UNIT_MM		102
#define UNIT_S		300
#define UNIT_MS		301
#define UNIT_N		1000
#define UNIT_KN		1001
#define UNIT_KGF		1002
#define UNIT_PA		1100
#define UNIT_KPA		1101
#define UNIT_MPA		1102
#define UNIT_BAR		1103
#define UNIT_NM		3200
#define UNIT_KNM		3201
#define UNIT_KGFM	3202
#define UNIT_STRAIN_PER	4101
#define UNIT_STRAIN_U	4102
#define UNIT_MM_P_S	5001
#define UNIT_N_P_S	5500
#define UNIT_KN_P_S	5501

#endif