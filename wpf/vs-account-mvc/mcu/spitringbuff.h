#ifndef __spitringbuff_h__
#define __spitringbuff_h__


#include <stdint.h>

//使用单字节写入
#define con_SpitRingBuff_Write_byte_enable 0

//读写加锁
#define con_SpitRingBuff_lock_enable 0


#ifdef __cplusplus 
extern "C"
{
#endif


typedef struct
{
	//字节数组的头索引	
	uint16_t head;
	//字节数组的尾索引	
	uint16_t tail;
	//队列的数组
	uint8_t* ring_buff;
	uint8_t ring_buff_size;
	//包的写索引
	uint8_t p_r_index;
	//包的读索引
	uint8_t p_w_index;
	//当前包的数量
	uint8_t package_count;
	//存放每个数据包的长度
	uint8_t* packet_len_buffer;
	uint8_t   packet_len_buffer_size;

	//用于单字节写入分包
#if con_SpitRingBuff_Write_byte_enable
	// 暂存单字节写入的buffer
	uint8_t* g_spitringBuff;
	uint8_t   g_spitringBuff_size;
	// 暂存单字节写入的buffer的索引
	uint8_t g_spitringPos;
	//0:用超时时间分包 1:根据换行符分包
	uint8_t enableReadLine;
	//超时计数
	uint8_t g_spitring_timeout_count;
	//分包超时时间
	uint8_t g_spitring_timeout_limit;
#endif
#if	con_SpitRingBuff_lock_enable
	void(*lock)(void);	/* 互斥上锁 */
	void(*unlock)(void);	/* 互斥解锁 */
#endif
}SpitRingBuff_TypeDef;


//读取环形队列1包数据,len会返回读到的长度
int8_t spitRingBuffRead(SpitRingBuff_TypeDef* spitRingBuff, uint8_t* rData, uint8_t* len);

//查看队列中的所有内容,不会进行消费
int8_t spitRingBuffPreview(SpitRingBuff_TypeDef* spitRingBuff, uint8_t* rData, uint8_t* len);


//1ms 调用1次分包函数,spitRingBuffWriteChar 100ms未调用,则分一次包
void spitRingBuffSpitPackHandel(SpitRingBuff_TypeDef* spitRingBuff);
//初始化
void spitRingBuffSpitInit(
	SpitRingBuff_TypeDef* spitRingBuff,
	uint8_t* ring_buff,
	uint8_t  ring_buff_size,
	uint8_t* packet_len_buffer,
	uint8_t  packet_len_buffer_size
);

#if con_SpitRingBuff_Write_byte_enable
//写入1个字节
int8_t spitRingBuffWriteChar(SpitRingBuff_TypeDef* ringBuff, uint8_t d);
#endif

//写入1个包
int8_t spitRingBuffWriteBuffer(SpitRingBuff_TypeDef* ringBuff, uint8_t* data, uint8_t len);

#ifdef __cplusplus 
}
#endif

#endif 

