
/*
*********************************************************************************************************
*********************************************************************************************************
* File : Main_constant.h
* By : Minglie
* Date :
*********************************************************************************************************
*/


#ifndef __Main_Constant_H
#define __Main_Constant_H

#include "vin_unit.h"
#include "man_def.h"
#include "spitringbuff.h"
#include "sys.h"
#include "user_key.h"


//手控器型号
#define CON_MAN_MODEL  0x1108
//手控器固件版本
#define CON_MAN_VER    0x0001
//可用按键
#define  CON_AVAILABLE_KEY 0x00ff
#define  CON_AVAILABLE_KEY_NUM 8
//可用指示灯
#define  CON_AVAILABLE_LED 0x0000
//可用图标数
#define  CON_AVAILABLE_ICON_NUM 3
//可用数码数
#define  CON_AVAILABLE_DVB_NUM  6

// dvb0的单位是否可写
#define  CON_DVB_0_UNIT_WRITABLE 0
// dvb1的单位是否可写
#define  CON_DVB_1_UNIT_WRITABLE 0
// dvb2的单位是否可写
#define  CON_DVB_2_UNIT_WRITABLE 0
// dvb3的单位是否可写
#define  CON_DVB_3_UNIT_WRITABLE 0
// dvb4的单位是否可写
#define  CON_DVB_4_UNIT_WRITABLE 0
// dvb5的单位是否可写
#define  CON_DVB_5_UNIT_WRITABLE 0
// dvb6的单位是否可写
#define  CON_DVB_6_UNIT_WRITABLE 0
// dvb7的单位是否可写
#define  CON_DVB_7_UNIT_WRITABLE 0


//区域数量
#define CON_ZONE_TOTAL_NUM	11
//图片数量
#define CON_IMG_TOTAL_NUM	   11





//序列号地址
#define  CON_MAN_SN_ADDR  0x02
//modBus从机地址的地址
#define  CON_SLAVE_MODBUS_ADDRESS_ADDR    0x01
//区域基地址
#define CON_ZONE_BASE_ADDR	0x00000020
//图片配置基地址
#define CON_IMG_CONFIG_BASE_ADDR	0x000001a0
//24号ASCII解压基地址
#define CON_ASCII24_HELP_BASE_ADDR	0x920
//48号ASCII解压基地址
#define CON_ASCII48_HELP_BASE_ADDR	0xc18





#define CON_ZONE_BACKGROUND		0
#define CON_ZONE_LOGO	  1
#define CON_ZONE_ICON0	  2
#define CON_ZONE_ICON1	  4
#define CON_ZONE_ICON2	  6


#define CON_ZONE_D0		  3
#define CON_ZONE_D1		  5
#define CON_ZONE_D2		  7
#define CON_ZONE_D3		  8
#define CON_ZONE_D4		  9
#define CON_ZONE_D5		  10


#define CON_PIC_LOGO	1
#define CON_PIC_BACKGROUND		0
#define CON_PIC_D0		  9
#define CON_PIC_D1		  9
#define CON_PIC_D2		  10
#define CON_PIC_D3		  10
#define CON_PIC_ICON0		  2
#define CON_PIC_ICON1		  2
#define CON_PIC_ICON2		  3
//ASCII 字库的索引
#define  CON_PIC_ASCII_24	4
#define  CON_PIC_ASCII_48	5

//ASCII 字库的颜色
#define CON_BLACK_YELLO_COLOR 0
#define CON_BLACK_BLUE_COLOR  1
#define CON_BLUE_WHITE_COLOR  2
#define CON_BLACK_GREEN_COLOR  3




#define MING_MEGER2(a,b)		 (a << 8) & 0xff00 | (b & 0xff)
#define MING_MEGER4(a,b,c,d)	 (a << 24) & 0xff000000 | ( b<< 16) & 0xff0000 | ( c<< 8) & 0xff00 |  d & 0xff 
#define MING_MEGERS2(a,b)	     (a << 16) & 0xffff0000 |  b & 0xffff 


#define DEV_UART1	0
#define DEV_UART2	1
#define	LCD_BUF_SIZE 		3500




#ifdef __cplusplus 
extern "C"
{
#endif



#ifdef OS_GLOBALS
#define OS_EXT
#else
#define OS_EXT extern
#endif



typedef union
{
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
		unsigned b9 : 1;
		unsigned b10 : 1;
		unsigned b11 : 1;
		unsigned b12 : 1;
		unsigned b13 : 1;
		unsigned b14 : 1;
		unsigned b15 : 1;
	}one;
	u16 all;
}_WORD_TypeDef;

typedef struct
{
	//modbus 从机地址
	u8	modbusSlaverAddress;
	//手控器序列号
	u32     sn;
}_CONFIG_INFO;


typedef struct
{
	u8		id;
	u8		type;    // type=0是普通图片 , type=1 字库图片
	u16		width;
	u16		heigth;
	u16 	entry_num;	 //一张图片中元素的个数				
	u16		entry_col_bgn[15];		//每个元素起始列数	 
	u16 	entry_width[15];		//每个元素的宽度
	u32 	mem_addr;
	u32 	mem_size;
}_PIC_INFO;

typedef struct
{
	u8	    id;
	u8      type;
	u16 	x;
	u16		y;
	u16		width;
	u16		heigth;
	u8      alignDir;
	u32     param;
	u8      flag;
	_PIC_INFO* pic_info;
}_ZONE;


OS_EXT volatile union bits_decompound
{
	struct struct0
	{
		unsigned ms1_s : 1;     //1ms timer sign bit
		unsigned ms5_s : 1;     //5ms timer sign bit
		unsigned z_flag : 1;   //需要刷新LCD标志
		unsigned is_reply : 1; //是否回复
		unsigned initIsDone : 1;
		unsigned timIsOK : 1; //软件定时器延时时间到
		unsigned timEnable : 1; //软件定时器启用标志
		unsigned online : 1;  //在线状态
		unsigned lcd_ready_s : 1; //lcd初始化完成标志
		unsigned factory_main_init_s : 1;
		unsigned factory_addr_init_s : 1;
		unsigned factory_keys_init_s : 1;
		unsigned factory_demo_init_s : 1;
	}one;
	unsigned char all[2];
}bt;

OS_EXT uint8_t lcd_busy;
//用来自定义显示一个东西
OS_EXT _ZONE g_myzone;

//系统计数器
OS_EXT u8 g_system_counter;


//串口1的接收队列
OS_EXT SpitRingBuff_TypeDef g_uart1SpitRingBuff;
OS_EXT u8                   g_uart1SpitRingBuff_buffer[200];
OS_EXT u8                   g_uart1SpitRingBuff_packet_len_buffer[10];

//每个消息4字节的定长消息
OS_EXT SpitRingBuff_TypeDef g_msgMqSpitRingBuff;
OS_EXT u8                   g_msgMqSpitRingBuff_buffer[64];
OS_EXT u8                   g_msgMqSpitRingBuff_packet_len_buffer[16];

//图片
OS_EXT _PIC_INFO g_pic_list[CON_IMG_TOTAL_NUM];
//区域
OS_EXT _ZONE g_zone_list[CON_ZONE_TOTAL_NUM];
//配置
OS_EXT _CONFIG_INFO    g_config_info;

//按键实时值
OS_EXT _WORD_TypeDef g_key_info;
//本地用的按下消息
OS_EXT _WORD_TypeDef g_key_info_local;

//最新发生的事件编码
OS_EXT u32   g_event_code;

//软件定时器计数
OS_EXT u32   g_time_1ms_count;
//软件定时器计数阈值
OS_EXT u32   g_time_1ms_count_limit;

//demo 数据更新任务
OS_EXT  u8  g_key_cousumer_task_handle;
//按键扫描
OS_EXT  u8  g_key_scan_task_handle;
//灯
OS_EXT _WORD_TypeDef g_led_info;


//串口1收到的字节数
OS_EXT u8 g_uart1RxCount;



//mcu状态
OS_EXT enum mcu_state
{
	BEGIN,
	LOGO,
	WAITONLINE,
	WAITINIT,
	INIT,
	WORK,
	FACTORY_MAIN = 21,
	FACTORY_ADDR = 22,
	FACTORY_KEYS = 23,
	FACTORY_DEMO = 24
} g_mcu_state;



#ifdef __cplusplus 
}
#endif

#endif