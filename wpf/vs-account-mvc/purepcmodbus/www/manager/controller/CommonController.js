const {app, M,common} = window;

//通用数据字典查询
app.get("/gloable_crmhelp_dictionary_list",async (req,res)=>{
    const queryCode=req.params;
    let apiPath=  M.config.baseUrlCrmHelp("/crmhelp/dictionary/listByQueryCode");
    let r = await window.M.request.get(apiPath,{queryCode:queryCode});
    res.send(r.data);
});
