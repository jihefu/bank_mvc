const { Content, Sider,Footer,Header } = Layout;
const {HashRouter  , Route, Link } = ReactRouterDOM;


class App extends React.Component {
    state = {
        collapsed: false,
        userInfo:{
            avatar:"https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/ruyunsuixing/img/default_avatar.png",
            user_name:"未登录"
        }
    };
    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }

    componentDidMount() {
        let userInfo= M.getUserData();
        if(userInfo!=null){
            this.setState({userInfo})
        }
        M.Component.App=this;
    };


    render() {
        return (
            <HashRouter>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider
                        defaultCollapsed={true} collapsible={true}  width={200} style={{ background: '#fff' }}
                    >
                        <Menu theme="light" defaultSelectedKeys={['1']} mode="inline">
                            <Menu.Item key="StatisticsCompanyList">
                                <Link to="/ContractsHeadList">账户</Link>
                            </Menu.Item>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Content style={{ margin: '0 16px' }}>
                            <div style={{ padding: 24, background: '#fff', height: "100%" }}>
                                <Route path="/" component={UserList} />
                            </div>
                        </Content>
                    </Layout>
                </Layout>
            </HashRouter>
        );
    }F
}


