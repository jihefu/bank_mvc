﻿#pragma comment(lib,"Winmm.lib")
#include "Function.h"
#include "PC.h"
#include "../model/Bank.h"
#include "../viewModel/LcdAtmViewModel.h"
#include "../viewModel/H5AtmViewModel.h"
#include "../common/KeyEvent.hpp"
#include "../dao/AccountDaoImpl.hpp"
#include "SqliteUtils.h"
//sql工具
SqliteUtils g_db("D:/workspace/action/obsolete_back_up/bank_mvc/wpf/vs-account-mvc/bank.db");
// 液晶按键事件
 Event<bool, KeyEventArgs> g_key_Event;
// h5事件
 Event<bool, KeyEventArgs> g_h5_key_Event;
//液晶view
 LcdAtmViewModel g_lcdAtmViewModel;
// h5 view
 H5AtmViewModel g_h5AtmViewModel;
AccountDao * g_accountDao =new AccountDaoImpl();

//初始化对象关系
void init() {

	Bank& g_Bank = Bank::getInstance();
	g_Bank.start();
	//液晶页面关联银行
	g_lcdAtmViewModel.setBank(&g_Bank);
	//浏览器页面关联银行
	g_h5AtmViewModel.setBank(&g_Bank);
	//按键事件 注册回调函数
	g_key_Event.addEventListener(&g_lcdAtmViewModel,   &LcdAtmViewModel::onKeyEvent);
	//h5事件 注册回调函数
	g_h5_key_Event.addEventListener(&g_h5AtmViewModel, &H5AtmViewModel::onH5Event);
}



int main()
{

	pc_init();
	init();
	pc_createHandle();
	mcu_task();
	pc_closeHandle();
	return 0;
}