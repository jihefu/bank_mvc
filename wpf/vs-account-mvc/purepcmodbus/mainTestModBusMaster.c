#include "modbusRtuMaster.h"
#include "stdio.h"
#include "windows.h"
#include <sys/timeb.h>
#include <stdint.h>
#include <stdio.h>


HANDLE hCom;//串口句柄


/**
  * 主机发送
*/
uint8_t modbus_uart_send1(uint8_t* buf, uint16_t len) {
    WriteFile(hCom, buf, len, &len, NULL);
    PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);
	return 0;
}



void delayms(uint32_t nms) {
    Sleep(nms);
}


ModbusRtuMasterTypeDef rtuMaster =
{
	.sendData = modbus_uart_send1,
	.delayms = delayms,
};





uint8_t 	uartRxBuf[4104];


DWORD WINAPI threadRead1(LPVOID lpParameter)
{

    hCom = CreateFile("COM7", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    // 获取和设置串口参数
    DCB myDCB;
    GetCommState(hCom, &myDCB);  // 获取串口参数
    fflush(stdout);
    myDCB.BaudRate = 112500;       // 波特率
    myDCB.Parity = NOPARITY;   // 校验位
    myDCB.ByteSize = 8;          // 数据位
    myDCB.StopBits = ONESTOPBIT; // 停止位
    SetCommState(hCom, &myDCB);  // 设置串口参数
    if (hCom == INVALID_HANDLE_VALUE)   //INVALID_HANDLE_VALUE表示出错，会设置GetLastError
    {
        puts("打开串口失败");
        return 0;
    }
    // 利用错误信息来获取进入串口缓冲区数据的字节数
    DWORD dwErrors;     // 错误信息
    COMSTAT Rcs;        // COMSTAT结构通信设备的当前信息
    int recv_total_size = 0;
    DWORD length = 100;               //用来接收读取的字节数
    while (1) {

        ClearCommError(hCom,
            &dwErrors,
            &Rcs);                                // 获取读缓冲区数据长度
        recv_total_size = Rcs.cbInQue;
        ReadFile(hCom, uartRxBuf, recv_total_size, &length, NULL);   //获取字符串
        PurgeComm(hCom, PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXABORT);  //清空缓冲区
        if (recv_total_size < 50 && recv_total_size>0) {
            for (int i = 0; i < recv_total_size; i++) {
                printf("%02x ", uartRxBuf[i]);
            }
            printf("\n");

            uint8_t res[50] = { 0x01 ,0x03, 0x02, 0x00, 0x01, 0x79 ,0x84 };
            MBRTUMasterRecvPackageISRCallback(&rtuMaster, uartRxBuf, recv_total_size);
         }
        Sleep(50);
    }

    return 0;
}



uint8_t ucBuf[10];
uint16_t pusRegBuffer[10];
int main()
{
    HANDLE HRead=  CreateThread(NULL, 0, threadRead1, NULL, 0, NULL);
    Sleep(500);
    int r = ReadHoldingRegisters(&rtuMaster, 1, 0x10, 1, 50, pusRegBuffer);
    while (1)
    {
        Sleep(2000);
    }

	return 0;
}