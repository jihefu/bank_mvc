#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <nlohmann/json.hpp>   
#include "../sqlite/sqlite3.h"
using namespace std;
using json = nlohmann::json;

class SqliteUtils
{
public:
	SqliteUtils();
	~SqliteUtils();
	 SqliteUtils(const string& path);
	int CreateSqlFile(const string& path);
	int CreateTable(const string& sql);
	int Open(const string& path);
	json DoSql(const string& sql);
	int Insert(const string& sql);
	int Delete(const string& sql);
	int Update(const string& sql);
	int FindCurrentTableMaxKey(const string& tableName, const string& strKey, int& nMaxKey);//查找当前表最大主键
	int FindAllData(const string& sql, vector<string>& arrKey, vector<vector<string>>& arrValue);
	string FindSingleVal(const string& sql);

private:
	void Destory();
private:
	sqlite3* pDB;
};

