
#include "SqliteUtils.h"


SqliteUtils::SqliteUtils()
{
	pDB = NULL;
}


SqliteUtils::SqliteUtils(const string& path)
{
	pDB = NULL;
	Open(path);
}


SqliteUtils::~SqliteUtils()
{
	Destory();
}


void SqliteUtils::Destory()
{
	if (pDB)
	{
		sqlite3_close(pDB);
		pDB = NULL;
	}
}

int SqliteUtils::CreateSqlFile(const string& path)
{
	return sqlite3_open(path.c_str(), &pDB);
}

int SqliteUtils::CreateTable(const string& sql)
{
	char* szMsg = NULL;
	return sqlite3_exec(pDB, sql.c_str(), NULL, NULL, &szMsg);
}




int SqliteUtils::Open(const string& path)
{
	return sqlite3_open(path.c_str(), &pDB);
}

int SqliteUtils::Insert(const string& sql)
{
	if (sql.empty()) return -1;

	char* zErrMsg = NULL;
	int ret = sqlite3_exec(pDB, sql.c_str(), NULL, NULL, &zErrMsg);
	if (zErrMsg)
	{
		sqlite3_free(zErrMsg);
	}
	return ret;
}

int SqliteUtils::Delete(const string& sql)
{
	int nCols = 0;
	int nRows = 0;
	char** azResult = NULL;
	char* errMsg = NULL;
	int result = sqlite3_get_table(pDB, sql.c_str(), &azResult, &nRows, &nCols, &errMsg);
	if (result != SQLITE_OK)
	{
		return false;
	}
	if (azResult)
	{
		sqlite3_free_table(azResult);
	}
	if (errMsg)
	{
		sqlite3_free(errMsg);
	}
	return true;
}

int SqliteUtils::Update(const string& sql)
{
	char* zErrMsg = NULL;

	int ret = sqlite3_exec(pDB, sql.c_str(), NULL, NULL, &zErrMsg);
	if (zErrMsg)
	{
		sqlite3_free(zErrMsg);
	}
	return ret;
}

int SqliteUtils::FindCurrentTableMaxKey(const string& tableName, const string& strKey, int& nMaxKey)
{
	nMaxKey = -1;
	if (tableName.empty() || strKey.empty()) return -1;
	string sql = "select * from " + tableName;

	int nCol = -1;
	int nRow = -1;
	int index = -1;
	char** azResult = NULL;
	char* errMsg = NULL;

	int result = sqlite3_get_table(pDB, sql.c_str(), &azResult, &nRow, &nCol, &errMsg);

	index = nCol;

	//取出最新的，对比穿进去的主键串，就是主键Max值
	for (int i = 0; i < nRow; i++)
	{
		for (int j = 0; j < nCol; j++)
		{
			string s1 = azResult[j];
			string s2 = azResult[index];
			if (s1 == strKey)
			{
				nMaxKey = atoi(azResult[index]);
			}
			index++;
		}
	}

	if (azResult)
	{
		sqlite3_free_table(azResult);
	}
	if (errMsg)
	{
		sqlite3_free(errMsg);
	}
	return result;
}

int SqliteUtils::FindAllData(const string& sql, vector<string>& arrKey, vector<vector<string>>& arrValue)
{
	if (sql.empty()) return -1;

	int nCols = 0;
	int nRows = 0;
	char** azResult = NULL;
	char* errMsg = NULL;
	int index = 0;
	const int result = sqlite3_get_table(pDB, sql.c_str(), &azResult, &nRows, &nCols, &errMsg);

	index = nCols;
	arrKey.clear();
	arrKey.reserve(nCols);
	arrValue.clear();
	arrValue.reserve(nRows);

	bool bKeyCaptured = false;
	for (int i = 0; i < nRows; i++)
	{
		vector<string> temp;
		for (int j = 0; j < nCols; j++)
		{
			if (!bKeyCaptured)
			{
				arrKey.push_back(azResult[j]);
			}
			temp.push_back(azResult[index]);
			index++;
		}
		bKeyCaptured = true;
		arrValue.push_back(temp);
	}

	if (azResult)
	{
		sqlite3_free_table(azResult);
	}
	if (errMsg)
	{
		sqlite3_free(errMsg);
	}
	return result;
}

string SqliteUtils::FindSingleVal(const string& sql) {

	std::vector<std::string> arrKey(10);
	vector<vector<string>> arrValue(10);
	FindAllData("SELECT sum(balance) c from t_user", arrKey, arrValue);
	if (arrValue.size() == 1) {
		return	arrValue[0][0];
	}
	return "";
}


json SqliteUtils::DoSql(const string& sql) {
	json rJson;
	if (sql.rfind("inster") == 0 || sql.rfind("INSTER") == 0) {
		int r= Insert(sql);
		rJson["c"] = r;
		return rJson;
	}
	if (sql.rfind("update") == 0 || sql.rfind("UPDATE") == 0) {
		int r = Update(sql);
		rJson["c"] = r;
		return rJson;
	}
	if (sql.rfind("delete") == 0 || sql.rfind("DELETE") == 0) {
		int r = Delete(sql);
		rJson["c"] = r;
		return rJson;
	}
	if (sql.rfind("select") == 0 || sql.rfind("SELECT") == 0) {
		std::vector<std::string> arrKey(10);
		vector<vector<string>> arrValue(10);
		FindAllData(sql, arrKey, arrValue);
		rJson = json::parse(R"([])");;
		if (arrValue.size()>0) {
			for (int  i = 0; i < arrValue.size(); i++)
			{  
				json item;
				for (int j = 0; j < arrKey.size(); j++)
				{
					item[arrKey[j]] = arrValue[i][j];
				}
				rJson.push_back(item);
			}
		}
		return rJson;
	}
	return rJson;
}