

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "../dao/AccountDao.h"  
#include "../common/ResultUtils.hpp"
#include "../model/Bank.h"


using namespace std;

extern uint32_t cur_accountId;
extern AccountDao* g_accountDao;
class UserService
{
   public:
	static m::Result login(const string& username, const string& password) {
		Bank& g_Bank = Bank::getInstance();
		AccountPOJO AccountPOJO = g_accountDao->getByUserName(username);
		if (password == AccountPOJO.password) {
			cur_accountId = AccountPOJO.id;
			g_Bank.getAcount(cur_accountId).init();
			return m::ResultUtils::successResult({ {
				  "id",AccountPOJO.id
				} });
		}
		return m::ResultUtils::failResult();
	}


	static m::Result regist(const string& username, const string& password) {
		AccountPOJO AccountPOJO = g_accountDao->getByUserName(username);
		if (!AccountPOJO.isEmpty()) {
			return m::ResultUtils::failResult("exist");
		}
		AccountPOJO.balance = 0;
		AccountPOJO.username = username;
		AccountPOJO.password = password;
		g_accountDao->inster(AccountPOJO);
		return m::ResultUtils::successResult();
	}


	static m::Result listAll(const AccountQuery& accountQuery) {
		vector<AccountPOJO> list = g_accountDao->listAll(accountQuery);
		json j = UserService::AccountPOJOList2Json(list);
		m::Result r = m::ResultUtils::successResult(j);
		return r;
	}


	static json AccountPOJO2Json(AccountPOJO & AccountPOJO) {
		json j;
		j["id"] = AccountPOJO.id;
		j["username"] = AccountPOJO.username;
		j["password"] = AccountPOJO.password;
		j["balance"] = AccountPOJO.balance;
		return j;
	}


	static json  AccountPOJOList2Json(vector<AccountPOJO> & AccountPOJOList) {
		json jsonArr= json::parse(R"([])");;
		for (auto& item : AccountPOJOList)
		{
			json j= AccountPOJO2Json(item);
			jsonArr.push_back(j);
		}
		return jsonArr;
	}
private:

};

