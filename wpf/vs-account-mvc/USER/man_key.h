#ifndef __demo_man_key_h
#define __demo_man_key_h	

#include "sys.h"

#define EVT_KEY_DOWN	0x0001
#define EVT_KEY_UP		0x0002

#define EVT_CLICK		0x0101
#define EVT_DBL_CLICK	0x0102
#define EVT_PRESSING	0x0201
#define EVT_LONG_CLICK	0x0202

#define KEY0_INDEX	  0x0000
#define KEY1_INDEX	  0x0001
#define KEY2_INDEX	  0x0002
#define KEY3_INDEX	  0x0003
#define KEY4_INDEX	  0x0004
#define KEY5_INDEX	  0x0005
#define KEY6_INDEX	  0x0006
#define KEY7_INDEX	  0x0007




#ifdef __cplusplus 
extern "C"
{
#endif

	void man_key_init();
	void man_key_tick();
	void PostEvent(int srcId, WORD code);
	WORD GetKeyboardMask(void);

#ifdef __cplusplus 
}
#endif

#endif
