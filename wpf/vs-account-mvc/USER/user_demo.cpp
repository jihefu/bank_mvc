#include "../common/KeyEvent.hpp"
#include "user_demo.h"
#include "man_key.h"
#include "../ucos/ucos.h"
#include "math.h"
#include "user_lcd.h"
#include "dvb.h"



//按键事件
extern Event<bool, KeyEventArgs> g_key_Event;

extern uint32_t cur_accountId ;


//demo数据更新任务不耗时,输出页面是否更新  
//indata: 1首次执行,0:非首次执行
//outdata:1界面变化,0 界面无变化 
void key_cousumer_task() {
	u8 keyRxBufTemp[8];
	u8 keyRxCount;
	//获取按键消息
	if (0 == spitRingBuffRead(&g_msgMqSpitRingBuff, &keyRxBufTemp[0], &keyRxCount))
	{
		KeyEventEnum keventEnum = (KeyEventEnum)(MING_MEGER2(keyRxBufTemp[2], keyRxBufTemp[3]));
		KeyValEnum keyVal =        (KeyValEnum)(MING_MEGER2(keyRxBufTemp[0], keyRxBufTemp[1]));
		KeyEventArgs keyArg = KeyEventArgs(cur_accountId, keventEnum, keyVal);
		g_key_Event.sendEvent(keyArg);
	}
	//10ms 
	OSTimeDly(10);
}

