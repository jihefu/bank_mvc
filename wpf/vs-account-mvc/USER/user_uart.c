
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "usart.h" 
#include "fifo.h"
#include "user_uart.h"
#include "w25qxx.h"
#include "key.h"
//const uint8_t c_buf[10] = {0x02,0x52,0x10,0x00,0x00,0x00,0x08,0x00,0x00,0x03};
//const uint8_t c_buf[10] = {0};
uint16_t	frame_error = 0;
extern uint8_t uart_rx_flag;
extern uint8_t  zone0_flag;
extern uint8_t	zone1_flag; 
extern uint8_t  zone2_flag;
extern uint8_t	 zone3_flag;

extern uint32_t  zone0_data;
extern uint32_t  zone1_data;
extern uint32_t  zone2_data;
extern uint32_t  zone3_data;
extern uint8_t 	zoneP0Id;
extern uint8_t 	zoneP1Id;
extern uint8_t 	zoneP2Id;
extern uint8_t 	zoneP3Id;
extern uint8_t 	zoneP4Id;
extern uint8_t 	zoneP11Id;
extern uint8_t 	zoneP12Id;
extern uint8_t 	zoneP13Id;
extern uint8_t 	zoneP14Id;
SerialData sData;             //接收帧结构 
uint8_t 	uartRxBuf[10] = {0};
uint8_t 	uartTxBuf[10] = {0};
extern uint8_t key;
extern uint8_t online;
uint16_t key_ticks = 0;
uint8_t DeCode(void);
/* 串口设备数据结构 */
typedef struct
{
	uint8_t status;		/* 发送状态 */
	fifo tx_fifo;	/* 发送fifo */
	fifo rx_fifo;	/* 接收fifo */
	uint8_t *dmarx_buf;	/* dma接收缓存 */
	uint16_t dmarx_buf_size;/* dma接收缓存大小*/
	uint8_t *dmatx_buf;	/* dma发送缓存 */
	uint16_t dmatx_buf_size;/* dma发送缓存大小 */
	uint16_t last_dmarx_size;/* dma上一次接收数据大小 */
}uart_device_t;

/* 串口缓存大小 */
#define UART1_TX_BUF_SIZE           240
#define UART1_RX_BUF_SIZE           240
#define	UART1_DMA_RX_BUF_SIZE		120
#define	UART1_DMA_TX_BUF_SIZE		120

#define UART2_TX_BUF_SIZE           2
#define UART2_RX_BUF_SIZE           2
#define	UART2_DMA_RX_BUF_SIZE		2
#define	UART2_DMA_TX_BUF_SIZE		2

/* 串口缓存 */
static uint8_t s_uart1_tx_buf[UART1_TX_BUF_SIZE];
static uint8_t s_uart1_rx_buf[UART1_RX_BUF_SIZE];
static uint8_t s_uart1_dmarx_buf[UART1_DMA_RX_BUF_SIZE];
static uint8_t s_uart1_dmatx_buf[UART1_DMA_TX_BUF_SIZE];

static uint8_t s_uart2_tx_buf[UART2_TX_BUF_SIZE];
static uint8_t s_uart2_rx_buf[UART2_RX_BUF_SIZE];
static uint8_t s_uart2_dmarx_buf[UART2_DMA_RX_BUF_SIZE];
static uint8_t s_uart2_dmatx_buf[UART2_DMA_TX_BUF_SIZE];

//static uint8_t	s_spi2_tx_buf[4096];
/* 串口设备实例 */
static uart_device_t s_uart_dev[2] = {0};

/* 测试 */
uint32_t s_UartTxRxCount[4] = {0};


/* fifo上锁函数 */
static void fifo_lock(void)
{
    __disable_irq();
}

/* fifo解锁函数 */
static void fifo_unlock(void)
{
    __enable_irq();
}

/**
 * @brief 串口设备初始化 
 * @param  
 * @retval 
 */
void uart_device_init(uint8_t uart_id)
{
  	if (uart_id == 0)
	{
		/* 配置串口1收发fifo */
		fifo_register(&s_uart_dev[uart_id].tx_fifo, &s_uart1_tx_buf[0], 
                      sizeof(s_uart1_tx_buf), fifo_lock, fifo_unlock);
		fifo_register(&s_uart_dev[uart_id].rx_fifo, &s_uart1_rx_buf[0], 
                      sizeof(s_uart1_rx_buf), fifo_lock, fifo_unlock);
		
		/* 配置串口1 DMA收发buf */
		s_uart_dev[uart_id].dmarx_buf = &s_uart1_dmarx_buf[0];
		s_uart_dev[uart_id].dmarx_buf_size = sizeof(s_uart1_dmarx_buf);
		s_uart_dev[uart_id].dmatx_buf = &s_uart1_dmatx_buf[0];
		s_uart_dev[uart_id].dmatx_buf_size = sizeof(s_uart1_dmatx_buf);
//		bsp_uart1_dmarx_config(s_uart_dev[uart_id].dmarx_buf, 
//							   sizeof(s_uart1_dmarx_buf));/* 只需配置接收模式DMA，发送模式需发送数据时才配置 */
		bsp_uart1_dmarx_start(s_uart_dev[uart_id].dmarx_buf,sizeof(s_uart1_dmarx_buf));
		s_uart_dev[uart_id].status  = 0;
	}
	else if (uart_id == 1)
	{
		/* 配置串口2收发fifo */
		fifo_register(&s_uart_dev[uart_id].tx_fifo, &s_uart2_tx_buf[0], 
                      sizeof(s_uart2_tx_buf), fifo_lock, fifo_unlock);
		fifo_register(&s_uart_dev[uart_id].rx_fifo, &s_uart2_rx_buf[0], 
                      sizeof(s_uart2_rx_buf), fifo_lock, fifo_unlock);
		
		/* 配置串口2 DMA收发buf */
		s_uart_dev[uart_id].dmarx_buf = &s_uart2_dmarx_buf[0];
		s_uart_dev[uart_id].dmarx_buf_size = sizeof(s_uart2_dmarx_buf);
		s_uart_dev[uart_id].dmatx_buf = &s_uart2_dmatx_buf[0];
		s_uart_dev[uart_id].dmatx_buf_size = sizeof(s_uart2_dmatx_buf);
		bsp_uart2_dmarx_start(s_uart_dev[uart_id].dmarx_buf,sizeof(s_uart2_dmarx_buf));
		s_uart_dev[uart_id].status  = 0;
	}
}

/**
 * @brief  串口发送数据接口，实际是写入发送fifo，发送由dma处理
 * @param  
 * @retval 
 */
uint16_t uart_write(uint8_t uart_id, const uint8_t *buf, uint16_t size)
{
	return fifo_write(&s_uart_dev[uart_id].tx_fifo, buf, size);
}

/**
 * @brief  串口读取数据接口，实际是从接收fifo读取
 * @param  
 * @retval 
 */
uint16_t uart_read(uint8_t uart_id, uint8_t *buf, uint16_t size)
{
	return fifo_read(&s_uart_dev[uart_id].rx_fifo, buf, size);
}

/**
 * @brief  串口dma接收完成中断处理
 * @param  
 * @retval 
 */
void user_uart_dmarx_done_isr(uint8_t uart_id)
{
  	uint16_t recv_total_size;
  	uint16_t recv_size;
	
		if(uart_id == 0)
	{
	  	recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart1_get_dmarx_buf_remain_size();
	}
	else if (uart_id == 1)
	{
		recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart2_get_dmarx_buf_remain_size();
	}
	recv_size = s_uart_dev[uart_id].dmarx_buf_size - s_uart_dev[uart_id].last_dmarx_size;
    s_UartTxRxCount[uart_id*2+1] += recv_size;
	fifo_write(&s_uart_dev[uart_id].rx_fifo, 
				   (const uint8_t *)&(s_uart_dev[uart_id].dmarx_buf[s_uart_dev[uart_id].last_dmarx_size]), recv_size);

	s_uart_dev[uart_id].last_dmarx_size = 0;
}
/**
 * @brief  串口dma接收缓存大小一半数据中断处理
 * @param  
 * @retval 
 */
void user_uart_dmarx_half_done_isr(uint8_t uart_id)
{
  	uint16_t recv_total_size;
  	uint16_t recv_size;
	
	if(uart_id == 0)
	{
	  	recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart1_get_dmarx_buf_remain_size();
	}
	else if (uart_id == 1)
	{
		recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart2_get_dmarx_buf_remain_size();
	}
	recv_size = recv_total_size - s_uart_dev[uart_id].last_dmarx_size;
	s_UartTxRxCount[uart_id*2+1] += recv_size;
    
	fifo_write(&s_uart_dev[uart_id].rx_fifo, 
				   (const uint8_t *)&(s_uart_dev[uart_id].dmarx_buf[s_uart_dev[uart_id].last_dmarx_size]), recv_size);
	s_uart_dev[uart_id].last_dmarx_size = recv_total_size;
}

/**
 * @brief  串口空闲中断处理
 * @param  
 * @retval 
 */
void user_uart_dmarx_idle_isr(uint8_t uart_id)
{
  	uint16_t recv_total_size;
  	uint16_t recv_size;
	
	if(uart_id == 0)
	{
	  	recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart1_get_dmarx_buf_remain_size();
	}
	else if (uart_id == 1)
	{
		recv_total_size = s_uart_dev[uart_id].dmarx_buf_size - bsp_uart2_get_dmarx_buf_remain_size();
	}
	recv_size = recv_total_size - s_uart_dev[uart_id].last_dmarx_size;
	s_UartTxRxCount[uart_id*2+1] += recv_size;
	fifo_write(&s_uart_dev[uart_id].rx_fifo, 
				   (const uint8_t *)&(s_uart_dev[uart_id].dmarx_buf[s_uart_dev[uart_id].last_dmarx_size]), recv_size);
	s_uart_dev[uart_id].last_dmarx_size = recv_total_size;
	
	online = DeCode();
	uart_rx_flag = 1;
}

/**
 * @brief  串口dma发送完成中断处理
 * @param  
 * @retval 
 */
void user_uart_dmatx_done_isr(uint8_t uart_id)
{
 	s_uart_dev[uart_id].status = 0;	/* DMA发送空闲 */
}

/**
 * @brief  循环从串口发送fifo读出数据，放置于dma发送缓存，并启动dma传输
 * @param  
 * @retval 
 */
void uart_poll_dma_tx(uint8_t uart_id)
{
  	uint16_t size = 0;
	
	if (0x01 == s_uart_dev[uart_id].status)
    {
        return;
    }
	size = fifo_read(&s_uart_dev[uart_id].tx_fifo, s_uart_dev[uart_id].dmatx_buf,
					 s_uart_dev[uart_id].dmatx_buf_size);
	if (size != 0)
	{	
      s_UartTxRxCount[uart_id*2+0] += size;
	  	if (uart_id == 0)
		{
          s_uart_dev[uart_id].status = 0x01;	/* DMA发送状态 */
					bsp_uart1_dmatx_start(s_uart_dev[uart_id].dmatx_buf, size);
//				bsp_uart1_dmatx_start((uint8_t *)c_buf, 10);
		}
		else if (uart_id == 1)
		{
            s_uart_dev[uart_id].status = 0x01;	/* DMA发送状态,必须在使能DMA传输前置位，否则有可能DMA已经传输并进入中断 */
//						bsp_uart2_dmatx_start(s_uart_dev[uart_id].dmatx_buf, size);
		}
	}
}

//extern uint8_t	uartRxBuf[];	 
void Save_Frame(void)
{
}

uint8_t DeCode(void) 
{ 
	uint8_t	picZoneSel = 0;
	static uint8_t ticks = 0;
	uint16_t verify_data = 0;
	fifo_read(&s_uart_dev[0].rx_fifo, uartRxBuf, 10); 
		sData.head = uartRxBuf[0];
		sData.cmd = uartRxBuf[1];
		sData.par = uartRxBuf[2];
		sData.data = (uartRxBuf[3]<<24) + (uartRxBuf[4] << 16) + (uartRxBuf[5] << 8) + uartRxBuf[6];
		sData.verify = (uartRxBuf[7] << 8) + uartRxBuf[8];
		sData.end = uartRxBuf[9];
			
//		uart_rx_flag = 0; 
		if(sData.head != 0x02 || sData.end != 0x03) {
			frame_error++;
			return 0;
		}		
		verify_data = sData.cmd + sData.par + (sData.data & 0xff)+(sData.data>>8 & 0xff)+(sData.data>>16 & 0xff)+(sData.data>>24 & 0xff);   //帧和验证
//		if(sData.verify != verify_data) {      			 //和验证
//			frame_error++;
//			return 0;
//		}
		if(sData.cmd == 'W'){
			switch(sData.par) {
				case 0x00:		zone0_data = sData.data;	zone0_flag = 1 ; break;
				case 0x01: 		zone1_data = sData.data;	zone1_flag = 1 ; break;
				case 0x02:		zone2_data = sData.data;	zone2_flag = 1 ; break;
				case 0x03:		zone3_data = sData.data; 	zone3_flag = 1 ; break;
				case 0x04:  	picZoneSel = sData.data >> 8;
											switch(picZoneSel) {
												case 0x00 : zoneP0Id = sData.data & 0xff + 0x30;   break;
												case 0x01 : zoneP1Id = sData.data & 0xff + 0x30; 	 break;  
												case 0x02 : zoneP2Id = sData.data & 0xff + 0x30;	 break; 
												case 0x03 : zoneP3Id = sData.data & 0xff + 0x30;	 break;
												case 0x04 : zoneP4Id = sData.data & 0xff + 0x30;	 break;
												case 0x11 : zoneP11Id = sData.data & 0xff + 0x30;	 break;
												case 0x12 : zoneP12Id = sData.data & 0xff + 0x30;	 break;
												case 0x13 : zoneP13Id = sData.data & 0xff + 0x30;	 break;
												case 0x14 : zoneP14Id = sData.data & 0xff + 0x30;	 break;	//写背景图，这时候屏幕是看不到背景的，在这段时间内切换中英文等	break;
											}
											break;
									
			}
			return 1;
		}		
		else if(sData.cmd == 'R'){
			switch(sData.par) 
			{
				key_ticks++;
				case 0x10:	uartTxBuf[0] = sData.head;
										uartTxBuf[1] = sData.cmd;
										uartTxBuf[2] = sData.par;
										uartTxBuf[3] = 0; 
										uartTxBuf[4] = 0;
										uartTxBuf[5] = 0;
										uartTxBuf[6] = key;
										verify_data  = sData.cmd + sData.par + uartTxBuf[3]+uartTxBuf[4]+uartTxBuf[5]+uartTxBuf[6];
										uartTxBuf[7] = verify_data & 0xff;
										uartTxBuf[8] = verify_data >>8 & 0xff;
										uartTxBuf[9] = sData.end;
										uart_write(DEV_UART1, uartTxBuf, 10);
										uart_poll_dma_tx(DEV_UART1);
										break; 
			}
		}
		return 0;
}
