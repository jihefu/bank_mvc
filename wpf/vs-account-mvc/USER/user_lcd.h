#ifndef __USER_LCD_H__
#define	__USER_LCD_H__
#include "sys.h"
#include "Main_constant.h"

#ifdef __cplusplus 
extern "C"
{
#endif

void Init_Zone();
//num ��ͼƬ���
void User_Lcd_ShowPic(_ZONE* zone, u8 num);


void User_Lcd_ShowZipPic(_ZONE* zone, u8 num);


void User_Lcd_ShowNum(_ZONE* zone, u32 dvb, u8 alignDir);

void User_Lcd_ShowDvbSringNum(
	_ZONE* zone,
	u32 dvb,
	u8 asciiType,
	u8 alignDir,
	u8 colorMode
);

void User_Lcd_ShowUnitSringNum(
	_ZONE* zone,
	u32 param,
	u8 asciiType,
	u8 alignDir,
	u8 colorMode
);

void User_Lcd_Clear(void);

void LCD_ShowZone(_ZONE* zone, u32 dvb, u8 alignDir);
void Show_Num(
	_ZONE* zone,
	char* p,
	u8 alignDir
);

void Show_String(
	_ZONE* zone,
	char* p,
	u8 asciiType,
	u8 alignDir,
	u8 colorMode
);
void disPlayDvb(u8 zoneId, u8 asciiType, u8 alignDir, u8 colorMode, u8 force);
void disPlayUnit(u8 zoneId, u8 asciiType, u8 alignDir, u8 colorMode, u8 force);
void disPlayImg(u8 zoneId, u8 force);
void updateZoneV(u8 zoneId, u32 param);

void lcdDisPlayMoney(u32 money);

#ifdef __cplusplus 
}
#endif


#endif