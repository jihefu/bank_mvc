
#ifndef _USER_UART_H_
#define _USER_UART_H_

#include <stdbool.h>
#include <stdint.h>
#include "main.h"

#define DEV_UART1	0
#define DEV_UART2	1

extern void uart_device_init(uint8_t uart_id);
extern uint16_t uart_write(uint8_t uart_id, const uint8_t *buf, uint16_t size);
extern uint16_t uart_read(uint8_t uart_id, uint8_t *buf, uint16_t size); 
extern void uart_dmatx_done_isr(uint8_t uart_id);
extern void uart_poll_dma_tx(uint8_t uart_id);
extern void WriteData_To_Flash(uint8_t uart_id);
//串口接收帧结构
//head：	串口帧头      0x02,其他数据不允许出现0x3C3C的连续数   
//cmd:		串口命令   
//par			参数号
//data		32位数据
//verify	验证数据，和验证
//end			串口帧尾
typedef struct{
	uint8_t		head;        
	uint8_t 		cmd;
	uint8_t		par;
	uint32_t 	data;
	uint16_t		verify; 
	uint8_t		end;   
} SerialData;

#define	UART_BUF_LENGTH		32    //UART  DMA缓冲区长度，为最大帧的2倍
#endif 
