#include "user_lcd.h"
#include "w25qxx.h"
#include "lcd.h"
#include "dvb.h"
#include "malloc.h" 

extern uint8_t uartRxBuf[4104];


u8 lcd_buf_A[LCD_BUF_SIZE];
//3500
u8* lcd_buf_B;
//500
u8* lcd_buf_C;

//压缩辅助的4个数组
u16 entry_col_bgn_with_zip_buf[380];
u8	buf_ready = 0;

u16 black_yello_point_table_buf[16] = {
				0,  8384, 16736,
				20992, 27232, 31456,
				35648, 39808, 44000,
				46144, 50304, 52448,
				56608, 58720, 62880,
				64992
};
u16 black_blue_point_table_buf[16] = {
		  0,  131,  262,  361,
		459,  525,  623, 2736,
		2802, 2867, 2933, 2966,
		5079, 5145, 5210, 5243
};
u16 blue_white_point_table_buf[16] = {
		  16400, 20785, 25106,
		29395, 33620, 37878,
		40055, 44280, 46425,
		50650, 52795, 54940,
		59133, 61278, 63423,
		65535
};

u16 black_green_point_table_buf[16] = {
	0,  288,  512,  672,
	832,  960, 1088, 3264,
	3392, 3488, 3584, 5728,
	5824, 5920, 6016, 6112
};



extern uint8_t lcd_busy;



void Init_Zone(void)
{
	//uartRxBuf 50往后的空间只在写字库时用到,这里和液晶共用
	lcd_buf_B = &uartRxBuf[100];
	lcd_buf_C = &uartRxBuf[3600];
	//个性配置
	W25QXX_DMA_Read_Sync(lcd_buf_A, 0, 32);
	g_config_info.modbusSlaverAddress = lcd_buf_A[1];
	g_config_info.sn = MING_MEGER4(lcd_buf_A[2], lcd_buf_A[3], lcd_buf_A[4], lcd_buf_A[5]);
	//区域配置,16个字节1个图片配置
	W25QXX_DMA_Read_Sync(lcd_buf_A, CON_ZONE_BASE_ADDR, 16 * CON_ZONE_TOTAL_NUM);
	for (int i = 0; i < CON_ZONE_TOTAL_NUM; i++) {
		g_zone_list[i].id = lcd_buf_A[i * 16];
		g_zone_list[i].type = lcd_buf_A[i * 16 + 1];
		g_zone_list[i].x = MING_MEGER2(lcd_buf_A[i * 16 + 2], lcd_buf_A[i * 16 + 3]);
		g_zone_list[i].y = MING_MEGER2(lcd_buf_A[i * 16 + 4], lcd_buf_A[i * 16 + 5]);
		g_zone_list[i].width = MING_MEGER2(lcd_buf_A[i * 16 + 6], lcd_buf_A[i * 16 + 7]);
		g_zone_list[i].heigth = MING_MEGER2(lcd_buf_A[i * 16 + 8], lcd_buf_A[i * 16 + 9]);
		g_zone_list[i].alignDir = lcd_buf_A[i * 16 + 10];
		g_zone_list[i].param = MING_MEGER4(lcd_buf_A[i * 16 + 11], lcd_buf_A[i * 16 + 12], lcd_buf_A[i * 16 + 13], lcd_buf_A[i * 16 + 14]);
		g_zone_list[i].pic_info = &g_pic_list[lcd_buf_A[i * 16 + 15]];
	}
	//图片配置,80个字节一个图片配置
	W25QXX_DMA_Read_Sync(lcd_buf_A, CON_IMG_CONFIG_BASE_ADDR, 80 * CON_IMG_TOTAL_NUM);
	for (int i = 0; i < CON_IMG_TOTAL_NUM; i++) {
		g_pic_list[i].id = lcd_buf_A[i * 80];
		g_pic_list[i].type = lcd_buf_A[i * 80 + 1];
		g_pic_list[i].width = MING_MEGER2(lcd_buf_A[i * 80 + 2], lcd_buf_A[i * 80 + 3]);
		g_pic_list[i].heigth = MING_MEGER2(lcd_buf_A[i * 80 + 4], lcd_buf_A[i * 80 + 5]);
		g_pic_list[i].entry_num = MING_MEGER2(lcd_buf_A[i * 80 + 6], lcd_buf_A[i * 80 + 7]);
		for (u8 j = 0; j < 15; j++)
		{
			g_pic_list[i].entry_col_bgn[j] = MING_MEGER2(lcd_buf_A[i * 80 + 8 + j * 2], lcd_buf_A[i * 80 + 8 + j * 2 + 1]);
		}
		for (u8 j = 0; j < 15; j++)
		{
			g_pic_list[i].entry_width[j] = MING_MEGER2(lcd_buf_A[i * 80 + 38 + j * 2], lcd_buf_A[i * 80 + 38 + j * 2 + 1]);
		}
		g_pic_list[i].mem_addr = MING_MEGER4(lcd_buf_A[i * 80 + 68], lcd_buf_A[i * 80 + 69], lcd_buf_A[i * 80 + 70], lcd_buf_A[i * 80 + 71]);
		g_pic_list[i].mem_size = MING_MEGER4(lcd_buf_A[i * 80 + 72], lcd_buf_A[i * 80 + 73], lcd_buf_A[i * 80 + 74], lcd_buf_A[i * 80 + 75]);
	}

	bt.one.lcd_ready_s = 1;
}


void Show_backGround(void)
{


}

void User_Lcd_ShowPic(_ZONE* zone, uint8_t num)
{
	u8* pbuf_A;
	u8* pbuf_B;
	u8* pbuf;
	u8	buf_sel = 0;
	u32	ept_len = 0;
	u16 len = 0;
	u32	flash_addr = 0;
	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	if (num >= zone->pic_info->entry_num) {
		return;
	}
	mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);             //缓冲区清空
	mymemset((void*)pbuf_B, 0, LCD_BUF_SIZE);             //缓冲区清空

	ept_len = zone->heigth * zone->width * 2;
	flash_addr = zone->pic_info->mem_addr + ept_len * num;          //根据图片标号，计算需要显示的图片内存地址

	User_LCD_Write_Prepare(zone->x, zone->y, zone->width, zone->heigth);   //设置需要显示的LCD窗口大小等	

	while (ept_len)
	{
		if (ept_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_len;

		pbuf = buf_sel ? pbuf_B : pbuf_A;
		W25QXX_DMA_Read(pbuf, flash_addr, len);
		while (buf_ready == 0);
		buf_ready = 0;
		while (lcd_busy);
		User_LCD_WriteRAM(pbuf, len);
		flash_addr += len;
		ept_len -= len;
	}
	while (lcd_busy);
}


/**
* lcd_buf_B 存放压缩数据
* lcd_buf_A 存放解压的数据
**/

void User_Lcd_ShowZipPic(_ZONE* zone, uint8_t num)
{
	u8* pbuf_A;
	u8* pbuf_B;
	u8* pbuf;
	u8	buf_sel = 0;
	u32	ept_len = 0;
	u32	zip_len = 0;
	u16 len = 0;
	u32	flash_addr = 0;
	u16 zipByteIndex = 0;
	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	if (num >= zone->pic_info->entry_num) {
		return;
	}
	//暂未实现精灵图的压缩
	if (num >0) {
		return;
	}
	ept_len = zone->heigth * zone->width * 2;
	zip_len = zone->pic_info->mem_size;
	flash_addr = zone->pic_info->mem_addr;
	User_LCD_Write_Prepare(zone->x, zone->y, zone->width, zone->heigth);   //设置需要显示的LCD窗口大小等	
	while (zip_len)
	{
		if (zip_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = zip_len;

		W25QXX_DMA_Read(lcd_buf_B, flash_addr, len);
		while (buf_ready == 0);
		buf_ready = 0;
		//上面的len指的是压缩后的len,下面将压缩后len长的数据解压写入液晶
		zipByteIndex = 0;
		//解压缩 ,4个字节为一组
		for (int i = 0; i < len / 4; i++)
		{
			u8 h = lcd_buf_B[4 * i];
			u8 l = lcd_buf_B[4 * i + 1];
			u16 hlCount = lcd_buf_B[4 * i + 2] * 256 + lcd_buf_B[4 * i + 3];
			for (int j = 0; j < hlCount; j++) {
				lcd_buf_A[zipByteIndex++] = h;
				lcd_buf_A[zipByteIndex++] = l;
				//lcd_buf_A 大小够3500 了,写一次液晶
				if (zipByteIndex >= LCD_BUF_SIZE) {
					while (lcd_busy);
					User_LCD_WriteRAM(lcd_buf_A, LCD_BUF_SIZE);
					zipByteIndex = 0;
				}
			}
		}
		//将余下的数据写入液晶
		while (lcd_busy);
		User_LCD_WriteRAM(lcd_buf_A, zipByteIndex);
		flash_addr += len;
		zip_len -= len;
	}
	while (lcd_busy);
}






//接收的数字为DVB码
void User_Lcd_ShowNum(_ZONE* zone, u32 dvb, uint8_t alignDir)
{
	char str[10];
	Dvb2String(str, dvb);
	Show_Num(zone, str, alignDir);
}

void Show_Num(_ZONE* zone, char* p, uint8_t alignDir)
{
	u32	flash_addr = 0;
	u32 len = 0;
	u32	count = 0;
	u32 add = 0;
	u8* pbuf_A;
	u8* pbuf_B;
	u8* pbuf;
	u32 tempColCount = 0;
	u32 	valColCount = 0;
	char* pTemp = p;
	u32	zone_range = 0;
	u8  strLen = 0;
	u16	ept_front_len = 0;					//对齐方式中，前面需要留空的空间
	u16	ept_back_len = 0;						//对齐方式中，后面需要留空的空间
	bool buf_sel = 0;

	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);             //缓冲区清空
	mymemset((void*)pbuf_B, 0, LCD_BUF_SIZE);             //缓冲区清空

	zone_range = zone->heigth * zone->width * 2;   //ZONE的容量大小，如果超过了这个容量，需要减少剩余的字符

	while ((*pTemp <= '~') && (*pTemp >= ' '))//判断是不是非法字符!
	{
		switch (*pTemp++)
		{
		case '0':	tempColCount += zone->pic_info->entry_width[0]; 	break;
		case '1': tempColCount += zone->pic_info->entry_width[1]; 	break;
		case '2': tempColCount += zone->pic_info->entry_width[2];		break;
		case '3': tempColCount += zone->pic_info->entry_width[3];		break;
		case '4': tempColCount += zone->pic_info->entry_width[4];		break;
		case '5': tempColCount += zone->pic_info->entry_width[5];		break;
		case '6': tempColCount += zone->pic_info->entry_width[6];		break;
		case '7': tempColCount += zone->pic_info->entry_width[7];		break;
		case '8': tempColCount += zone->pic_info->entry_width[8];		break;
		case '9': tempColCount += zone->pic_info->entry_width[9];		break;
		case '-': tempColCount += zone->pic_info->entry_width[10];	break;
		case '.': tempColCount += zone->pic_info->entry_width[11];	break;
		case ':': tempColCount += zone->pic_info->entry_width[12];		break;
		default:	break;
		}
		if (tempColCount > zone->width) break;    //如果实际计算的数据大于显示区域宽度，省略当前及后面的字符
		valColCount = tempColCount;                 		//需要显示的列数
		strLen++;																			//需要显示的字符数
	}

	switch (alignDir)            	//根据对齐方式，计算前面留空的buf大小
	{
	case ALIGN_LEFT:
		ept_front_len = 0;
		ept_back_len = zone_range - valColCount * zone->heigth * 2;
		break;
	case ALIGN_RIGHT:
		ept_front_len = zone_range - valColCount * zone->heigth * 2;
		ept_back_len = 0;
		break;
	case ALIGN_CENTER:
		ept_front_len = ((zone->width - valColCount) / 2) * zone->heigth * 2;
		ept_back_len = zone_range - ept_front_len - valColCount * zone->heigth * 2;
		break;
	}
	User_LCD_Write_Prepare(zone->x, zone->y, zone->width, zone->heigth);   //设置需要显示的LCD窗口大小等

	while (ept_front_len)           		 //填充空白区域
	{
		if (ept_front_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_front_len;

		User_LCD_WriteRAM(pbuf, len);

		ept_front_len -= len;
	}

	while (strLen--)
	{
		switch (*p)
		{
		case '0':
			flash_addr = zone->pic_info->mem_addr;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[0] * 2;
			break;
		case '1':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[1] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[1] * 2;
			break;
		case '2':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[2] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[2] * 2;
			break;
		case '3':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[3] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[3] * 2;
			break;
		case '4':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[4] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[4] * 2;
			break;
		case '5':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[5] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[5] * 2;
			break;
		case '6':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[6] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[6] * 2;
			break;
		case '7':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[7] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[7] * 2;
			break;
		case '8':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[8] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[8] * 2;
			break;
		case '9':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[9] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[9] * 2;
			break;
		case '-':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[10] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[10] * 2;
			break;
		case '.':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[11] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[11] * 2;
			break;
		case ':':
			flash_addr = zone->pic_info->mem_addr + zone->pic_info->entry_col_bgn[12] * zone->pic_info->heigth * 2;
			len = zone->pic_info->heigth * zone->pic_info->entry_width[12] * 2;
			break;
		}
		W25QXX_Read(pbuf, flash_addr, len);     //查询模式读取FLASH  
		pbuf = buf_sel ? pbuf_B : pbuf_A;
		buf_ready = 0;
		User_LCD_WriteRAM(pbuf, len);

		~buf_sel;
		p++;
	}
	mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);             //缓冲区清空
	mymemset((void*)pbuf_B, 0, LCD_BUF_SIZE);             //缓冲区清空
	while (ept_back_len)           		 //填充空白区域
	{
		if (ept_back_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_back_len;

		User_LCD_WriteRAM(pbuf, len);
		while (0);
		ept_back_len -= len;
	}
}



void User_Lcd_ShowDvbSringNum(_ZONE* zone, u32 dvb, u8 asciiType, uint8_t alignDir, uint8_t colorMod)
{
	char str[10];
	Dvb2String(str, dvb);
	Show_String(zone, str, asciiType, alignDir, colorMod);
}



void User_Lcd_ShowUnitSringNum(_ZONE* zone, u32 param, u8 asciiType, u8 alignDir, u8 colorMode)
{
	char* str;
	switch (param)
	{
	case UNIT_M: str = "m"; break;
	case UNIT_MM: str = "mm"; break;
	case UNIT_S: str = "s"; break;
	case UNIT_MS: str = "ms"; break;
	case UNIT_N: str = "n"; break;
	case UNIT_KN: str = "kn"; break;
	case UNIT_KGF: str = "kfg"; break;
	case UNIT_PA: str = "pa"; break;
	case UNIT_KPA: str = "kpa"; break;
	case UNIT_MPA: str = "map"; break;
	case UNIT_BAR: str = "bar"; break;
	case UNIT_NM: str = "nm"; break;
	case UNIT_KNM: str = "knm"; break;
	case UNIT_KGFM: str = "kgfm"; break;
	case UNIT_STRAIN_PER: str = "per"; break;
	case UNIT_STRAIN_U: str = "u"; break;
	case UNIT_MM_P_S: str = "mm/s"; break;
	case UNIT_N_P_S: str = "N/s"; break;
	case UNIT_KN_P_S: str = "kN/s"; break;
	default: {
		str = "";
		break;
	}
	}
	Show_String(zone, str, asciiType, alignDir, colorMode);
}




void Show_String(
	_ZONE* zone,
	char* p,
	u8 asciiType,
	u8 alignDir,
	u8 colorMode
) {
	u32	flash_addr = 0;
	u32 len = 0;
	u32 len_zip = 0;
	u32	count = 0;
	u32 add = 0;
	u8* pbuf_A;
	u8* pbuf_B;
	u8* pbuf;
	u16* entry_col_bgn_p;
	u16* entry_width_p;
	u16* entry_col_bgn_zip_p;
	u16* entry_width_zip_p;
	u16* point_table_p;
	u32 tempColCount = 0;
	u32 	valColCount = 0;
	char* pTemp = p;
	u32	zone_range = 0;
	u8  strLen = 0;
	u16	ept_front_len = 0;					//对齐方式中，前面需要留空的空间
	u16	ept_back_len = 0;						//对齐方式中，后面需要留空的空间
	bool buf_sel = 0;
	u16 zipByteIndex = 0;
	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	pbuf_B = lcd_buf_B;
	if (pTemp[0] == '\0') {
		return;
	}
	mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);             //缓冲区清空
	mymemset((void*)pbuf_B, 0, LCD_BUF_SIZE);             //缓冲区清空
	if (asciiType == CON_PIC_ASCII_24) {
		W25QXX_Read((u8*)entry_col_bgn_with_zip_buf, CON_ASCII24_HELP_BASE_ADDR, 760);
	}
	else
	{
		W25QXX_Read((u8*)entry_col_bgn_with_zip_buf, CON_ASCII48_HELP_BASE_ADDR, 760);
	}

	switch (colorMode)
	{
	case CON_BLACK_YELLO_COLOR:point_table_p = black_yello_point_table_buf; break;
	case CON_BLACK_BLUE_COLOR:point_table_p = black_blue_point_table_buf; break;
	case CON_BLUE_WHITE_COLOR:point_table_p = blue_white_point_table_buf; break;
	case CON_BLACK_GREEN_COLOR:point_table_p = black_green_point_table_buf; break;
	default:
		point_table_p = black_yello_point_table_buf;
		break;
	}

	entry_col_bgn_p = &entry_col_bgn_with_zip_buf[0];
	entry_width_p = &entry_col_bgn_with_zip_buf[190];
	entry_col_bgn_zip_p = &entry_col_bgn_with_zip_buf[95];
	entry_width_zip_p = &entry_col_bgn_with_zip_buf[285];

	zone_range = zone->heigth * zone->width * 2;   //ZONE的容量大小，如果超过了这个容量，需要减少剩余的字符
	while ((*pTemp <= '~') && (*pTemp >= ' '))//判断是不是非法字符!
	{
		char  c = *pTemp++;
		tempColCount += entry_width_p[c - ' '];
		if (tempColCount > zone->width) break;    //如果实际计算的数据大于显示区域宽度，省略当前及后面的字符
		valColCount = tempColCount;                 		//需要显示的列数
		strLen++;																			//需要显示的字符数
	}
	switch (alignDir)            	//根据对齐方式，计算前面留空的buf大小
	{
	case ALIGN_LEFT:
		ept_front_len = 0;
		ept_back_len = zone_range - valColCount * zone->heigth * 2;
		break;
	case ALIGN_RIGHT:
		ept_front_len = zone_range - valColCount * zone->heigth * 2;
		ept_back_len = 0;
		break;
	case ALIGN_CENTER:
		ept_front_len = ((zone->width - valColCount) / 2) * zone->heigth * 2;
		ept_back_len = zone_range - ept_front_len - valColCount * zone->heigth * 2;
		break;
	}
	User_LCD_Write_Prepare(zone->x, zone->y, zone->width, zone->heigth);   //设置需要显示的LCD窗口大小等
	if (colorMode == CON_BLACK_YELLO_COLOR || colorMode == CON_BLACK_BLUE_COLOR || colorMode == CON_BLACK_GREEN_COLOR) {
		mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);       //缓冲区清空
	}
	else if (colorMode == CON_BLUE_WHITE_COLOR) {
		for (int i = 0; i < ept_front_len / 2; i++)
		{
			pbuf_A[2 * i] = 0x40;
			pbuf_A[2 * i + 1] = 0x10;
		}
	}
	while (ept_front_len)           		 //填充空白区域
	{
		if (ept_front_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_front_len;
		User_LCD_WriteRAM(pbuf_A, len);
		ept_front_len -= len;
	}
	while (strLen--)
	{
		flash_addr = g_pic_list[asciiType].mem_addr + entry_col_bgn_zip_p[*p - ' '];
		len_zip = entry_width_zip_p[*p - ' '];
		len = g_pic_list[asciiType].heigth * entry_width_p[*p - ' '] * 2;
		W25QXX_Read(lcd_buf_B, flash_addr, len_zip);     //查询模式读取FLASH  
		zipByteIndex = 0;
		for (int i = 0; i < len_zip / 3; i++)
		{
			u8 pointIndex = lcd_buf_B[3 * i];
			u16 pointValue = point_table_p[pointIndex];
			u8 h = pointValue >> 8 & 0xff;
			u8 l = pointValue & 0xff;
			u16 hlCount = lcd_buf_B[3 * i + 1] * 256 + lcd_buf_B[3 * i + 2];
			for (int j = 0; j < hlCount; j++) {
				pbuf[zipByteIndex++] = h;
				pbuf[zipByteIndex++] = l;
			}
		}
		//pbuf = buf_sel ? pbuf_B : pbuf_A;
		buf_ready = 0;
		User_LCD_WriteRAM(pbuf, len);
		//~buf_sel;
		p++;
	}
	if (colorMode == CON_BLACK_YELLO_COLOR || colorMode == CON_BLACK_BLUE_COLOR || colorMode == CON_BLACK_GREEN_COLOR) {
		mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);       //缓冲区清空
	}
	else if (colorMode == CON_BLUE_WHITE_COLOR) {
		for (int i = 0; i < ept_back_len / 2; i++)
		{
			pbuf_A[2 * i] = 0x40;
			pbuf_A[2 * i + 1] = 0x10;
		}
	}
	while (ept_back_len)           		 //填充空白区域
	{
		if (ept_back_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_back_len;

		User_LCD_WriteRAM(pbuf_A, len);
		while (lcd_busy);
		ept_back_len -= len;
	}
}




void User_Lcd_Clear(void)
{
	u8* pbuf_A;
	u8* pbuf_B;
	u8* pbuf;
	u8	buf_sel = 0;
	u32	ept_len = 0;
	u16 len = 0;
	u32	flash_addr = 0;

	pbuf = lcd_buf_A;
	pbuf_A = lcd_buf_A;
	mymemset((void*)pbuf_A, 0, LCD_BUF_SIZE);             //缓冲区清空 

	User_LCD_Write_Prepare(0, 0, 320, 240);   //设置需要显示的LCD窗口大小等 	
	ept_len = 320 * 240 * 2;   //ZONE的容量大小，如果超过了这个容量，需要减少剩余的字符

	while (ept_len)           		 //填充空白区域
	{
		if (ept_len > LCD_BUF_SIZE)
			len = LCD_BUF_SIZE;
		else
			len = ept_len;

		User_LCD_WriteRAM(pbuf, len);
		while (lcd_busy);
		ept_len -= len;
	}
}



//显示dvb数
void disPlayDvb(u8 zoneId, u8 asciiType, u8 alignDir, u8 colorMode, u8 force) {
	if (g_zone_list[zoneId].flag || force) {
		g_zone_list[zoneId].flag = 0;
		while (lcd_busy);
		g_myzone = g_zone_list[zoneId];
		User_Lcd_ShowDvbSringNum(&g_myzone, g_myzone.param, asciiType, alignDir, colorMode);
	}
}
//显示单位
void disPlayUnit(u8 zoneId, u8 asciiType, u8 alignDir, u8 colorMode, u8 force) {
	if (g_zone_list[zoneId].flag || force) {
		g_zone_list[zoneId].flag = 0;
		while (lcd_busy);
		g_myzone = g_zone_list[zoneId];
		g_myzone.pic_info = &g_pic_list[asciiType];
		User_Lcd_ShowUnitSringNum(&g_myzone, g_myzone.param, asciiType, alignDir, colorMode);
		//Ming_GUI_rectangle(g_myzone.x, g_myzone.y, g_myzone.x + g_myzone.width, g_myzone.y + g_myzone.heigth);
	}
}


//显示图片
void disPlayImg(u8 zoneId, u8 force) {
	if (g_zone_list[zoneId].flag || force) {
		g_zone_list[zoneId].flag = 0;
		while (lcd_busy); User_Lcd_ShowPic(&g_zone_list[zoneId], g_zone_list[zoneId].param & 0xff);
	}
}

void updateZoneV(u8 zoneId, u32 param) {
	g_zone_list[zoneId].flag = 1;
	g_zone_list[zoneId].param = param;
}

void lcdDisPlayMoney(u32 money) {
	updateZoneV(CON_ZONE_D2, Float2Dvb(money, 0));
}


