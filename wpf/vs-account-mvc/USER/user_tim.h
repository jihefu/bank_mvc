#ifndef __USER_TIM_H__
#define	__USER_TIM_H__
#include "sys.h"
extern void TIM3_Enable(void);
extern void TIM3_Disable(void);
extern void TIM6_Enable(void);
extern void TIM6_Disable(void);
extern void TIM7_Enable(void);
extern void TIM7_Disable(void);
#endif