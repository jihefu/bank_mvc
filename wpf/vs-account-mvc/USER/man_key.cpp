#include "main.h"
#include "man_key.h"
#include "Main_Constant.h"

/*
按键事件产生规则：
第一层：
1.按下事件KEY_DOWN，必然一对一后续对应抬起事件KEY_UP。

第二层，按下-抬起组合产生CLICK、LONG_CLICK、DBL_CLICK其中一种事件。
2.按下-抬起时间小于等于300毫秒，正常情况下产生CLICK；超出则产生LONG_CLICK。
3.上述2正常产生CLICK的例外，如果紧接着再次按下，且前后两次按下时间小于等于400毫秒，则产生DBL_CLICK。简化起见，不论第二次的抬起时间。
4.上述2产生LONG_CLICK，按下从300毫秒开始每隔100毫秒产生PRESSING事件，直到抬起。
*/



// 外部接口函数，便利适配外部输入输出


// 内部状态定义
#define STT_IDLE						0
#define STT_WAITING_CLICK_UP			1
#define STT_WAITING_DBL_CLICK_DOWN		2
#define STT_WAITING_DBL_CLICK_UP		3
#define STT_LONG_PRESSING				10






class ManKey {
protected:
	int		m_index;
	int		m_state;
	DWORD	m_downMs;
	DWORD	m_pressingCount;
public:
	ManKey(int index) { m_index = index; m_state = STT_IDLE; }
	ManKey() { }
	void setMIndex(int	inx) { m_index = inx; }
	virtual ~ManKey() {}
	virtual void OnUiTick(DWORD ms);
};


void ManKey::OnUiTick(DWORD ms)
{
	BOOL keyPressState = (GetKeyboardMask() & (0x0001 << m_index)) ? 1 : 0;
	switch (m_state) {
	case STT_IDLE:									// 缺省处于空闲状态
	default:
		if (keyPressState == TRUE) {				// 一切从按下开始
			m_downMs = ms;
			PostEvent(m_index, EVT_KEY_DOWN);
			m_state = STT_WAITING_CLICK_UP;
		}
		break;
	case STT_WAITING_CLICK_UP:
		if (ms - m_downMs > 300) {					// 超时转到长按状态，同时产生第一个按住事件
			PostEvent(m_index, EVT_PRESSING);
			m_pressingCount = 1;
			m_state = STT_LONG_PRESSING;
		}
		else if (keyPressState == FALSE) {			// 完成一次点击，但需要延迟区分CLICK还是DBL_CLICK
			m_state = STT_WAITING_DBL_CLICK_DOWN;
		}
		break;
	case STT_WAITING_DBL_CLICK_DOWN:
		if (ms - m_downMs > 400) {					// 超出400毫秒没等到第二次按下，判断为CLICK结束，回归空闲
			PostEvent(m_index, EVT_CLICK);
			PostEvent(m_index, EVT_KEY_UP);
			m_state = STT_IDLE;
		}
		else if (keyPressState == TRUE) {
			m_state = STT_WAITING_DBL_CLICK_UP;		// 遭遇再次按下，判断为DBL_CLICK，后续DBL_CLICK_UP
		}
		break;
	case STT_WAITING_DBL_CLICK_UP:
		if (keyPressState == FALSE) {				// 简单等到再次抬起，结束DBL_CLICK，回归空闲
			PostEvent(m_index, EVT_DBL_CLICK);
			PostEvent(m_index, EVT_KEY_UP);
			m_state = STT_IDLE;
		}
		break;
	case STT_LONG_PRESSING:
		if (keyPressState == FALSE) {				// 长按期间等到抬起，结束LONG_CLICK，回归空闲
			PostEvent(m_index, EVT_LONG_CLICK);
			PostEvent(m_index, EVT_KEY_UP);
			m_state = STT_IDLE;
		}
		else if (ms - m_downMs > 300 + m_pressingCount * 20) {
			PostEvent(m_index, EVT_PRESSING);
			m_pressingCount++;
		}
		break;
	}
}


static ManKey manKey[CON_AVAILABLE_KEY_NUM];

void man_key_init() {

	for (int i = 0; i < CON_AVAILABLE_KEY_NUM; i++)
	{
		manKey[i].setMIndex(i);
	}
}


void man_key_tick() {
	uint32_t ms = HAL_GetTick();
	for (int i = 0; i < CON_AVAILABLE_KEY_NUM; i++)
	{
		manKey[i].OnUiTick(ms);
	}
}

