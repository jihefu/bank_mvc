
#ifndef _USER_KEY_H_
#define _USER_KEY_H_

#include "sys.h"
#include "Main_constant.h"

#ifdef __cplusplus 
extern "C"
{
#endif

void user_key_scan_task();
void user_key_server();
void userKeyInit();
void user_key_scan_task();

#ifdef __cplusplus 
}
#endif

#endif 
