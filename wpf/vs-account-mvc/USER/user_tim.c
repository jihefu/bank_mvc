#include "user_tim.h"  
#include "key.h"
#include "tim.h"
#include "main.h"
#include "gpio.h"
#include "lcd.h"
extern uint8_t tim3IsOK;
uint8_t oldKey = 0;
uint8_t newKey = 0;
extern uint8_t key;
void TIM3_Enable(void)
{
	HAL_TIM_Base_Start_IT(&htim3); 
}

void TIM3_Disable(void)
{
	HAL_TIM_Base_Stop_IT(&htim3);
}  

void TIM6_Enable(void)
{
	HAL_TIM_Base_Start_IT(&htim6); 
}

void TIM6_Disable(void)
{
	HAL_TIM_Base_Stop_IT(&htim6);
}  

void TIM7_Enable(void)
{
	HAL_TIM_Base_Start_IT(&htim7); 
}

void TIM7_Disable(void)
{
	HAL_TIM_Base_Stop_IT(&htim7);
}  

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	
  if(htim->Instance == TIM3)
  {
		TIM3_Disable();              //�ر�TIM3��ʱ��
		tim3IsOK = 1;
  }
		
  if(htim->Instance == TIM6)
  {
		newKey = KEY_Scan(0); 
		if(newKey)
		{
			key = newKey;
			oldKey = newKey;
			TIM7_Enable();
			TIM6_Disable();
		}
		else
			key = newKey;
  }
  if(htim->Instance == TIM7)
  {
			TIM6_Enable();
			TIM7_Disable();
  }
}