﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Newtonsoft.Json.Linq;

namespace VtcPositionDemo.Driver
{
    public class Config : DependencyObject
    {
        public static DependencyProperty QRCodeUrlProperty;
        public string QRCodeUrl
        {
            #region
            get
            {
                return (string)GetValue(QRCodeUrlProperty);
            }
            set
            {
                SetValue(QRCodeUrlProperty, value);
            }
            #endregion
        }

      

        public event DependencyPropertyChangedEventHandler OnDependencyPropertyChanged;
        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            #region
            Config sn = (Config)d;

            if (sn.OnDependencyPropertyChanged != null)
            {
                sn.OnDependencyPropertyChanged(d, e);
            }
            #endregion
        }

      


        public void LoadFile()
        {
          
        }

        public void SaveFile()
        {


        }

    }
}
