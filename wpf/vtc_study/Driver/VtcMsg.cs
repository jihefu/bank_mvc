﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtcPositionDemo.Driver
{
    public class VtcMsg
    {
        public ushort MsgId { get; set; }
        public uint Ms { get; set; }
        public string Msg { get; set; }
    }
}
