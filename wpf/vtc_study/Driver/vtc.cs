﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VtcPositionDemo.Driver
{
    unsafe public class vtc
    {
        public static uint INVALID_HANDLE = 0x80000000;

        public static int LAUNCH_START = 0x0000;

        public static int LAUNCH_STEP = 0x0100;     // 启动，及以下正常步骤
        public static int RUN_MSG = 0x0200;     // 运行时信息
        public static int RUN_EVENT = 0x0300;   // 运行中发生指定事件
        public static int RUN_WARNING = 0x0400;     // 运行时警告
        public static int RUN_DEBUG = 0x0500;       // 运行时跟踪信息

        // 从左到右4位十六进制，第二位最重要，后两位从属，除 ATS_ACTION和ATS_FINAL_ACTION外，最后一位是引擎识别号
        public static int ATS_LOADING = 0x0600;     // ATS正在加载
        public static int ATS_LOAD_ABORT = 0x0610;      // ATS加载不成功
        public static int ATS_LOAD_OK = 0x0621;     // ATS加载成功
        public static int ATS_STARTING = 0x0630;        // ATS启动中
        public static int ATS_STARTED = 0x0651;     // ATS已经启动
        public static int ATS_BREAK = 0x0660;       // ATS(长时间动作)中断
        public static int ATS_CONTINUE = 0x0670;        // ATS(中断后)继续

        public static int ATS_CATCH_CONTROLLER = 0x0680;    // ATS抓住控制器
        public static int ATS_RELEASE_CONTROLLER = 0x0690;  // ATS释放控制器
        public static int ATS_SIMU_CLAMPED = 0x06A0;
        public static int ATS_SIMU_FREE = 0x06B0;
        public static int ATS_SIMU_LOADING = 0x06C0;
        public static int ATS_SIMU_BROKEN = 0x06D0;
        public static int ATS_SIMU_RELEASED = 0x06E0;
        public static int ATS_ACTION = 0x0700;      // // ATS进行中动作+句柄+轴
        public static int ATS_FINAL_ACTION = 0x0D00;        // ATS轴最终动作+句柄+轴
        public static int ATS_ABORT = 0x0800;       // ATS异常中止
        public static int ATS_OVER = 0x0900;        // ATS结束
        public static int ATS_OVER1 = 0x0901;        // ATS结束
        public static int API_DEBUG = 0x0A00;   // 应用调用接口跟踪
        public static int HOTLINE_DROPPED = 0x0B00;     // 通讯掉线
        public static int HOTLINE_RESUMED = 0x0C00;		// 通讯恢复


        public static int ATS_LOAD_FAil = 0x0610;  // 加载失败
        public static int ATS_OK = 0x0900;
        public static int ATS_PUSH_BTN = 0x0710;

        public static int SYNC_STEP = 0x0F00;
        public static int SYNC_NUMBER = 0x0F10;
        public static int SYNC_WAIT = 0x0F20;
        public static int SYNC_GO = 0x0F30;
        public static int SYNC_ABORT = 0x1F00;
        public static int SYNC_OK = 0x2F00;

        public static int LAUNCH_ABORT = 0x1000;		// 返回值:出错后导致启动异常，中断退出
        public static int LAUNCH_OK = 0x2000;       // 返回值:启动正常完成并退出
        public static int CLOSE_STEP = 0x4000;		// 关闭步骤
        public static int CLOSE_OK = 0x8000;		// 返回值:关闭完成

        public static int STOP_HOLD = 1;			// 闭环保持停在当前反馈值，偶用，过渡暂停
        public static int STOP_HOLD_DEF = 3;			// 闭环保持停在缺省通道的当前值，最常用，高压切换，第一保护模式
        public static int STOP_LOCK = 5;			// 开环停，锁止，低压切换，第二保护模式
        public static int STOP_PROTECT = 7;			// 开环停，附带保护措施，开环停的升级
        public static int STOP_POWER_OFF = 9;			// 开环停电，


        public static uint P2P_SQUARE = 0;		// 方波
        public static uint P2P_TRIANGLE = 1;		// 三角波
        public static uint P2P_SAWTOOTH = 2;		// 锯齿波
        public static uint P2P_INV_SAWTOOTH = 3;	// 逆锯齿波
        public static uint P2P_COS = 4;		// 余弦波
        public static uint P2P_MSQUARE = 5;		// 运动方波
        public static uint P2P_MTRIANGLE = 6;		// 运动三角波
        public static uint P2P_MSAWTOOTH = 7;		// 运动锯齿波
        public static uint P2P_MINV_SAWTOOTH = 8;		// 运动逆锯齿波
        public static uint P2P_MHALF_SIN = 9;		// 半正弦波

        // 调试接口
        public static int SOFTREG_EXPIRED_LICENSE = -4;
        public static int SOFTREG_INVALID_DATE = -3;
        public static int SOFTREG_APP_NOT_REG = -2;
        public static int SOFTREG_INVALID_REG_CODE = -1;
        public static int SOFTREG_FULL_LICENSE = 0;

        [StructLayout(LayoutKind.Sequential)]
        public struct ProductInfo
        {
            public uint dvModelCode;						// d前缀表示DVB码
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] modelName;
            public uint dvConfigVer;
            public uint dvUser;
            public uint dvOsver;
            public uint dvSn;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] manufacturer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] device;
        }

        public static uint MAX_CARD_NUM = 8;
        public static uint MAX_ACQ_NUM = 16;
        public static uint MAX_CTRL_NUM = 8;


        public static uint EEPI_AD_MODE = 0;		// 原PARAM0
        public static uint EEPI_PULSE_MODE = 1;		// 脉冲模式0/1/2/3，[50-200]/[550-700]模式1/3下的PWM基频
        public static uint EEPI_DA_VIB_FREQ = 2;		// DA伺服颤振频率
        public static uint EEPI_DA_VIB_AMP = 3;		// DA伺服颤振幅值
        public static uint EEPI_SSI_MODE = 4;    // [0000S]个位SSI模式
        public static uint EEPI_SPWM_AC_AMP = 5;		// SPWM交流幅值
        public static uint EEPI_HOURS = 6;		// 使用过的小时数
        public static uint EEPI_EMP_NO = 7;		// 原PARAM7，用于查看最后操作的员工工号

        [StructLayout(LayoutKind.Sequential)]
        public struct CardInfo
        {
            public ushort bcdModel;
            public ushort bcdVer;			// BCD码威程卡型号及固件版本
            public Int32 sn;
            public Int32 type;
            public Int32 user;
            public Int32 endDate;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public ushort[] hardConfig;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] appName;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] appRegCode;

            public Int32 acqNum;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public ushort[] bcdAcqChnlTypes;    // bcdAcqChnlTypes[MAX_ACQ_NUM];
            public Int32 ctrlNum;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public ushort[] bcdCtrlChnlTypes;   // bcdCtrlChnlTypes[MAX_CTRL_NUM]
        }

        /// <summary>
        /// 从Vtc消息里，解析出Ats消息和对应的引擎号,轴
        /// </summary>
        /// <param name="vtcMsgId"></param>
        /// <param name="hAts"></param>
        /// <param name="atsMsgId"></param>
        /// <param name="axi"></param>
        /// <returns></returns>
        public static bool TryGetAtsMsg(ushort vtcMsgId, out int hAts, out ushort atsMsgId, out int axi)
        {
            #region
            if (vtcMsgId >= vtc.ATS_FINAL_ACTION && vtcMsgId < (vtc.ATS_FINAL_ACTION + 0x0100))
            {
                atsMsgId = (ushort)(vtcMsgId & 0x0F00);// 第3位是Ats消息
                var h = vtcMsgId & 0x00F0;// 第2位是句柄
                hAts = h >> 4;
                axi = vtcMsgId & 0x000F;// 最后一位是轴
                return true;
            }
            else if (vtcMsgId >= vtc.ATS_ACTION && vtcMsgId < (vtc.ATS_ACTION + 0x0100))
            {
                atsMsgId = (ushort)(vtcMsgId & 0x0F00);// 第3位是Ats消息
                var h = vtcMsgId & 0x00F0;// 第2位是句柄
                hAts = h >> 4;
                axi = vtcMsgId & 0x000F;// 最后一位是轴
                return true;
            }
            else if (vtcMsgId >= vtc.ATS_LOADING && vtcMsgId < (vtc.ATS_OVER + 0x0100))
            {
                atsMsgId = (ushort)(vtcMsgId & 0x0FF0);// 第2,3位是Ats消息
                hAts = vtcMsgId & 0x000F;// 第1位是句柄
                axi = 0;
                return true;
            }
            hAts = 0;
            atsMsgId = 0;
            axi = 0;
            return false;
            #endregion
        }

        public delegate void LogCallback(ushort msgId, uint ms, string msg);
        public delegate void DigitalCallback(int pobj, string tag, int onState);

        [DllImport("vtc.dll")]
        public static extern uint VtcGetDllVer();
        [DllImport("vtc.dll")]
        public static extern uint VtcCountDevices();    // 获取所有能找到的设备\卡
        [DllImport("vtc.dll")]
        public static extern short VtcLaunch(LogCallback fpLog);
        [DllImport("vtc.dll")]
        public static extern short VtcCreate(string jconfig, LogCallback fpLog);    // 根据jconfig配置文件直接创建一个驱动，而不是加载本地配置文件
        [DllImport("vtc.dll")]
        public static extern ushort VtcGetLogEvent(byte[] msgBuf, uint bufLen);    // 轮询模式
        [DllImport("vtc.dll")]
        public static extern void VtcClose(uint msout);
        [DllImport("vtc.dll")]
        public static extern void VtcGetProductInfo(IntPtr pInfo);



        // 测量采集
        [DllImport("vtc.dll")]
        public static extern int VtcGetVinList(byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern uint VtcSubscribeForVin(string vinTag);
        [DllImport("vtc.dll")]
        public static extern uint VtcGetMaxScale(uint hVin);
        [DllImport("vtc.dll")]
        public static extern int VtcGetUnitCode(uint hVin);
        [DllImport("vtc.dll")]
        public static extern void VtcClearToZero(uint hVin, double vled);
        [DllImport("vtc.dll")]
        public static extern int VtcTakeSnapshot();
        [DllImport("vtc.dll")]
        public static extern uint VtcGetTime(int i);
        [DllImport("vtc.dll")]
        public static extern uint VtcGetVin(uint hVin, int i);
        [DllImport("vtc.dll")]
        public static extern double VtcGetFloatVin(uint hVin, int i);   // 直接读出浮点数据

        /// <summary>
        /// 块读模式只能在卡1000Hz频率下实现，默认点间隔时间都是1ms
        /// </summary>
        /// <param name="hVin"></param>
        /// <param name="blockBuffer"></param>
        /// <param name="maxN"></param>
        /// <returns></returns>
        [DllImport("vtc.dll")]
        public static extern int VtcGetFloatVinBlock(uint hVin, double[] blockBuffer, int maxN);
        [DllImport("vtc.dll")]
        public static extern int VtcGetDigitalList(byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern uint VtcSubscribeForDigital(string digitalTag);
        [DllImport("vtc.dll")]
        public static extern int VtcGetDigitalState(uint hDigital, int i);
        [DllImport("vtc.dll")]
        public static extern void VtcAddDigitalListener(uint hDigital, int pobj, DigitalCallback fpOnDigital);
        [DllImport("vtc.dll")]
        public static extern int VtcOnQuickTimer(); //驱动外部UI接口，由应用软件定时调用


        // 开关控制
        [DllImport("vtc.dll")]
        public static extern int VtcGetSwitchList(byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern int VtcIsSwitchUsed(string tag);
        [DllImport("vtc.dll")]
        public static extern uint VtcUseSwitch(string tag);
        [DllImport("vtc.dll")]
        public static extern void VtcSwitchOn(uint hSwitch, int b);
        [DllImport("vtc.dll")]
        public static extern int VtcGetSwitchState(uint hSwitch, int i);

        // 虚拟控制器
        [DllImport("vtc.dll")]
        public static extern int VtcGetControllerList(byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern uint VtcUseController(string tag);
        [DllImport("vtc.dll")]
        public static extern uint VtcGetMotorFeedVin(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern uint VtcGetDefDispVin(uint hCtrl); // 获得默认位移通道        
        [DllImport("vtc.dll")]
        public static extern int VtcGetMotiWayList(uint hCtrl, int[] ways);
        [DllImport("vtc.dll")]
        public static extern int VtcGetClosedVinList(uint hCtrl, byte[] tagList, int motiWay);
        [DllImport("vtc.dll")]
        public static extern void VtcSetMotiWay(uint hCtrl, int way);

        // 仿真器
        [DllImport("vtc.dll")]
        public static extern void VtcClampSpecimen(uint hCtrl, string jspecimen);
        [DllImport("vtc.dll")]
        public static extern void VtcReleaseSpecimen(uint hCtrl);

        // 控制指令
        [DllImport("vtc.dll")]
        public static extern void VtcCtrlStop(uint hCtrl, int level);
        [DllImport("vtc.dll")]
        public static extern void VtcCtrlOpen(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern void VtcCtrlMotion(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern void VtcCtrlClosed(uint hCtrl, uint hFeed);

        // 外源
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefMan(uint hCtrl, double gain);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefExt(uint hCtrl, uint hRef, double gain);

        // 开环专用
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefActMove(uint hCtrl, double velo);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefActFullMove(uint hCtrl, double uniVelo);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefActHold(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefActTurn(uint hCtrl, double turnVelo);//开环动作，在当前开环输出上叠加个速度

        // 闭环可用
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenWait(uint hCtrl, double t);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenStep(uint hCtrl, double step);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenHold(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSlope(uint hCtrl, double velo);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSlopeTo(uint hCtrl, double speed, double target);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenHoldEx(uint hCtrl, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSlopeEx(uint hCtrl, double velo, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSlopeToEx(uint hCtrl, double speed, double target, double aAcce);

        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSlopePass(uint hCtrl, double speed, double target, double passSpeed, double aAcce);//但到达目标后不是保持，而是切换到passSpeed慢速

        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenHalfSin(uint hCtrl, double halfT, double height);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSinUp(uint hCtrl, double T, double amp, int nT);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSinAway(uint hCtrl, double T, double amp, int nT);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenMultiStep(uint hCtrl, int nPoint, double[] steps, double[] holdTime);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenMultiSlopeHold(uint hCtrl, int nPoint, double[] goTime, double[] targets, double[] holdTime);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenArbi(uint hCtrl, double intvTime, int nPoint, double[] dBuf);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenPolyline(uint hCtrl, int nPoint, double[] tCoor, double[] posiCoor);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenPolyCosline(uint hCtrl, int nPoint, double[] tCoor, double[] posiCoor);

        // 运动可用
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenHome(uint hCtrl, double backVelo, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenStride(uint hCtrl, double t, double stroke, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenTrigStride(uint hCtrl, double t, double stroke);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenPass(uint hCtrl, double t, double stroke, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenMove(uint hCtrl, double velo, double stroke, double aAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenBrake(uint hCtrl, double aFollow);
        // 附带现场前馈自整定
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenTestMove(uint hCtrl, double aSpeed, double stroke);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenTestCos(uint hCtrl, double t, double dist);
        // 周期波形与循环
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenSinWave(uint hCtrl, double freq, double mid, double amp);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenP2pWave(uint hCtrl, int p2pType, double freq, double from, double to);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenArbiWave(uint hCtrl, double freq, int nPoint, double[] posiCoor);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenPolylineWave(uint hCtrl, double freq, int nPoint, double[] tCoor, double[] posiCoor);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenWaveNum(uint hCtrl, uint num);
        [DllImport("vtc.dll")]
        public static extern void VtcSetRefGenCycleNum(uint hCtrl, uint num);

        // 二维控制
        [DllImport("vtc.dll")]
        public static extern uint VtcCreate2dSequence();
        [DllImport("vtc.dll")]
        public static extern void VtcAddWait(uint h2dSeq, double t);
        [DllImport("vtc.dll")]
        public static extern void VtcAddLineStride(uint h2dSeq, double t, double xstroke, double ystroke, double lineAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcAddArcStride(uint h2dSeq, double t, double r, double launchAngle, double turningAngle, double lineAcce);
        [DllImport("vtc.dll")]
        public static extern void VtcSetSwitchAction(uint h2dSeq, uint hSwitch, int onState);
        [DllImport("vtc.dll")]
        public static extern void VtcCtrl2dMotion(uint hCtrlX, uint hCtrlY, uint h2dRefGen);

        // ATS控制
        [DllImport("vtc.dll")]
        public static extern void AtLoad(string script);
        [DllImport("vtc.dll")]
        public static extern void AtGetAxisList(byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern void AtGetActionList(int axisIndex, byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern int AtGetAxisButtonList(int axisIndex, byte[] nameList);
        [DllImport("vtc.dll")]
        public static extern int AtGetActionButtonList(int axisIndex, int actionIndex, byte[] nameList);


        [DllImport("vtc.dll")]
        public static extern void AtStart(string script);
        [DllImport("vtc.dll")]
        public static extern void AtPushButton(string btName);
        [DllImport("vtc.dll")]
        public static extern void AtTerminate();

        // ATS并行控制
        [DllImport("vtc.dll")]
        public static extern uint AtsLoad(string script);

        [DllImport("vtc.dll")]
        public static extern int AtsGetAxisList(uint hAts, byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern int AtsGetActionList(uint hAts, int axisIndex, byte[] tagList);
        [DllImport("vtc.dll")]
        public static extern int AtsGetAxisButtonList(uint hAts, int axisIndex, byte[] nameList);
        [DllImport("vtc.dll")]
        public static extern int AtsGetActionButtonList(uint hAts, int axisIndex, int actionIndex, byte[] nameList);
        [DllImport("vtc.dll")]
        public static extern void AtsStart(uint hAts, string script);
        [DllImport("vtc.dll")]
        public static extern void AtsPushButton(uint hAts, string btName);
        [DllImport("vtc.dll")]
        public static extern void AtsTerminate(uint hAts);
        [DllImport("vtc.dll")]
        public static extern int AtsGetParamPack(uint hAts);
        [DllImport("vtc.dll")]
        public static extern int AtsGetRuntimePack(uint hAts);

        // 手控器
        [DllImport("vtc.dll")]
        public static extern int VtcGetManEvent(IntPtr paEvent);

        // 标定接口
        [DllImport("vtc.dll")]
        public static extern int VtcGetVinFullClassName(uint hVin, byte[] nameBuf);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateZero(uint hVin, double vled);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateGain(uint hVin, double vled, double vstd);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateToScale(uint hVin, double vled);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateFromScale(uint hVin, double vstd);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateToVeriScales(uint hVin, double[] vstdScales, double[] vleds, int n);   //标准值为标定点，输入软件值 往标准值标，用于检定
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateFromVeriScales(uint hVin, double[] vstds, double[] vledScales, int n);// 软件值为标定点，输入的时标准值，用于自保压标定
        [DllImport("vtc.dll")]
        public static extern int VtcInsertPoint(uint hVin, double newScale);
        [DllImport("vtc.dll")]
        public static extern int VtcRemovePoint(uint hVin, int iScale);
        [DllImport("vtc.dll")]
        public static extern int VtcExportPolyMap(uint hVin, double[] scales, int[] ads);
        [DllImport("vtc.dll")]
        public static extern int VtcExportBiMap(uint hVin, double* pRes, int* pZero);
        [DllImport("vtc.dll")]
        public static extern void VtcCalibrateLoadDeformation(uint hVin, double[] loadCol, double[] dispCol, int n);

        // 独立查询接口(无需VtcLaunch()) 
        [DllImport("vtc.dll")]
        public static extern int VtcCountCards();   // 获取实际打开的设备\卡数量
        [DllImport("vtc.dll")]
        public static extern void VtcGetCardInfo(int ci, IntPtr pCardInfo);
        [DllImport("vtc.dll")]
        public static extern void VtcGetConfigFile(byte[] filename);

        public static int REGSTATE_EXPIRED_LICENSE = -4;
        public static int REGSTATE_INVALID_DATE = -3;
        public static int REGSTATE_APP_NOT_REG = -2;
        public static int REGSTATE_INVALID_REG_CODE = -1;
        public static int REGSTATE_FULL_LICENSE = 0;

        [DllImport("vtc.dll")]
        public static extern int VtcGetAppRegState(string appName);
        [DllImport("vtc.dll")]
        public static extern int VtcRereg(int newRegCode, int operKey, byte[] errMsg);
        //[DllImport("vtc.dll")]
        //public static extern void VtcUpload(string filepath, LogCallback fpLog);// v2.75取消
        //[DllImport("vtc.dll")]
        //public static extern void VtcDownload(string filepath, LogCallback fpLog);

        // 调试接口
        [DllImport("vtc.dll")]
        public static extern int VtcGetActuatorPack(uint hCtrl);
        [DllImport("vtc.dll")]
        public static extern int VtcGetActiveStrategyPack(uint hCtrl);// 或者当前活动通道的调试参数
        [DllImport("vtc.dll")]
        public static extern int VtcGetStrategyPack(uint hCtrl, uint hFeed);// 获取指定通道的调试参数
        [DllImport("vtc.dll")]
        public static extern int VtcGetAtsParamPack();
        [DllImport("vtc.dll")]
        public static extern int VtcGetAtsRuntimePack();    // 获取Ats实时运行参数表，v2.75增加

        [DllImport("vtc.dll")]
        public static extern int VtcGetAtsVarTagList(byte[] varNameList);
        [DllImport("vtc.dll")]
        public static extern int VtcGetAtsVarTag(string varName, byte[] tagBuf);
        [DllImport("vtc.dll")]
        public static extern void VtcSetAtsVarTag(string varName, string tag);

        //[DllImport("vtc.dll")]
        //public static extern void PackGetClass(int hPack, byte[] className);// 20200324 删除
        [DllImport("vtc.dll")]
        public static extern int PackGetParamList(int hPack, byte[] nameList);
        [DllImport("vtc.dll")]
        public static extern uint PackGetParam(int hPack, string pname);
        [DllImport("vtc.dll")]
        public static extern void PackSetParam(int hPack, string pname, uint dv);
        [DllImport("vtc.dll")]
        public static extern int WatchIterateNext(int h);
        [DllImport("vtc.dll")]
        public static extern void WatchGetName(int h, byte[] nameBuf);
        [DllImport("vtc.dll")]
        public static extern void WatchGetContent(int h, byte[] contBuf);
        [DllImport("vtc.dll")]
        public static extern void TestCall(int funcId, double arg);
    }
}
