﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VtcPositionDemo.Driver
{
    public class Data
    {
        public const uint EODVB = 0x80000000;
        public Data()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }
        public static string Data_FixDot(double data, int dot)
        {
            #region
            if (dot < 0)
            {
                return data.ToString();
            }
            string fm = "F" + dot.ToString();
            return data.ToString(fm);
            #endregion
        }

        public static string Data_ParseDvbToString(uint dvb)
        {
            #region 解析一个dvb码为原码，并用字符串的方式返回
            if (Data.Data_DvbIsInteger(dvb) != 0)
            {
                return Data_Dvb2Int(dvb).ToString();
            }
            else
            {
                double dv = Data_Dvb2Float(dvb);
                int dot = Data_DotOfDvb(dvb);
                return Data_FixDot(dv, dot);
            }
            #endregion
        }

        [DllImport("data.dll")]
        public static extern int Data_GetVer();

        [DllImport("data.dll")]
        public static extern int Data_DvbIsPositive(uint dvb);

        [DllImport("data.dll")]
        public static extern int Data_DvbIsInteger(uint dvb);

        [DllImport("data.dll")]
        public static extern int Data_DotOfDvb(uint dvb);

        [DllImport("data.dll")]
        public static extern int Data_Dvb2Int(uint dvb);

        [DllImport("data.dll")]
        public static extern uint Data_Int2Dvb(int iv);

        [DllImport("data.dll")]
        public static extern double Data_Dvb2Float(uint dvb);


        [DllImport("data.dll")]
        public static extern uint Data_Float2Dvb(double fv, int dot);

        [DllImport("data.dll")]
        public static extern uint Data_FreeFloat2Dvb(double fv);

        [DllImport("data.dll")]
        public static extern uint Data_DvbReform(uint dvb, int dot);

        [DllImport("data.dll")]
        public static extern int Data_DvbCompare(uint dvb1, uint dvb2);

        [DllImport("data.dll")]
        public static extern void Data_Dvb2String(char[] buf, uint dvb);

        [DllImport("data.dll")]
        public static extern uint Data_String2Dvb(string str);

        [DllImport("data.dll")]
        public static extern int Data_GetDvbCount(uint[] dvbs);

        [DllImport("data.dll")]
        public static extern int Data_Dvbs2String(char[] buf, uint[] dvbs);

        [DllImport("data.dll")]
        unsafe public static extern int Data_String2Dvbs(uint[] dvbs, char* str);

        [DllImport("data.dll")]
        public static extern short Data_Int2Bcd(int iv);

        [DllImport("data.dll")]
        public static extern int Data_Bcd2Int(short bcd);

        [DllImport("data.dll")]
        public static extern void Data_Bcd2String(char[] buf, short bcd);

        [DllImport("data.dll")]
        unsafe public static extern short Data_String2Bcd(char* str);
    }
}
