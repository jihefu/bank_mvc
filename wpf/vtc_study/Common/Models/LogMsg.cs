﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VtcPositionDemo.Driver;

namespace VtcPositionDemo.Common.Models
{
    public class LogMsg
    {
        public string msg { get; set; }

        public string data { get; set; }

        public int code { get; set; }


        public LogMsg(int code,string msg,string data)
        {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }


        public static LogMsg ERROR(string msg)
        {
            return new LogMsg(-1, msg, null);
        }


        public static LogMsg HttpServerLog(string msg)
        {
            return new LogMsg(-1, "httpServer:"+ msg, null);
        }


        public static LogMsg INFO(string msg)
        {
            return new LogMsg(0, msg,"");
        }

        public static LogMsg SUCCESS(string msg,string data)
        {
            return new LogMsg(0, msg, data);
        }

        public static LogMsg TIP(string msg)
        {
            return new LogMsg(10, msg, null);
        }
    }

    public class ChnlMsg
    {
        public long ms;
        public double v;

        public ChnlMsg(long ms, double v)
        {
            this.ms = ms;
            this.v = v;
        }
    }

    public class RobotPushBtnChangeMsg
    {

        public String v;

        public RobotPushBtnChangeMsg(String v)
        {
            this.v = v;
        }
    }


}
