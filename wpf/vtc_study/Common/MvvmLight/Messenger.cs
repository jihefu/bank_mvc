﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtcPositionDemo.Common.MvvmLight
{
    public static class Messenger
    {

        public static ConcurrentDictionary<Type, List<Action<object>>> _handles = new ConcurrentDictionary<Type, List<Action<object>>>();

        public static void Register<T>(Action<object> action)
        {
            if (!_handles.ContainsKey(typeof(T)))
            {
                _handles[typeof(T)] = new List<Action<object>>();
            }
            _handles[typeof(T)].Add(action);
        }

        public static void Unregister()
        {
            _handles.Clear();
        }


        public static void Send<T>(T obj)
        {
            if (_handles.ContainsKey(typeof(T)))
            {
                foreach(var action in _handles[typeof(T)])
                {
                    action.Invoke(obj);
                }
            }
        }

    }
}
