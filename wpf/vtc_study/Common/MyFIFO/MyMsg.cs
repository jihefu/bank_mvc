﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFIFO
{

    /// <summary>
    /// 用于显示的通道值
    /// </summary>
    public class  ChannelDisMsg{
        public long ms { get; set; }
        public double loadAd { get; set; }
        public double elongAd { get; set; }
        public double dispAd { get; set; }
        public ChannelDisMsg()
        {

        }
        public ChannelDisMsg(long ms, double loadAd, double elongAd, double dispAd)
        {
            this.ms = ms;
            this.loadAd = loadAd;
            this.elongAd = elongAd;
            this.dispAd = dispAd;
        }
    }

    /// <summary>
    /// 用于在fifo中传输的值
    /// </summary>
    public struct ChannelTransmitMsg
    {
        public UInt32 ms;
        public UInt32 loadAd;
        public UInt32 elongAd;
        public UInt32 dispAd;

        public ChannelTransmitMsg(uint ms, uint loadAd, uint elongAd, uint dispAd)
        {
            this.ms = ms;
            this.loadAd = loadAd;
            this.elongAd = elongAd;
            this.dispAd = dispAd;
        }

    
    }



    /// <summary>
    /// 消息
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyMsg<T>
    {
        public string msg { get; set; }
        public T data { get; set; }
        public int code { get; set; }

        public MyMsg(int code, string msg, T data)
        {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }
        public static MyMsg<T> ERROR(string msg)
        {
            return new MyMsg<T>(-1, msg, default(T));
        }

        public static MyMsg<T> Msg(int code,T data)
        {
            return new MyMsg<T>(-1, "", data);
        }

        public static MyMsg<T> Success(T data)
        {
            return new MyMsg<T>(0, "", data);
        }

    }
}
