﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

using ThoughtWorks.QRCode.Codec;

namespace VtcPositionDemo.Common.Utils
{
    public static class QRCodeHelper
    {


        public static System.Drawing.Bitmap GetBitmap(string CodeContent, int QrSize = 2)
        {

            //创建二维码生成类
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();

            QrSize = QrSize <= 0 ? 2 : QrSize;
            //设置编码模式
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            //设置编码测量度
            qrCodeEncoder.QRCodeScale = QrSize;

            //设置编码版本
            qrCodeEncoder.QRCodeVersion = 0;
            //设置编码错误纠正
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            System.Drawing.Bitmap image = qrCodeEncoder.Encode(CodeContent, Encoding.UTF8);
            return image;
        }


        public static BitmapSource ToBitmapSource(System.Drawing.Bitmap image)
        {
            IntPtr ptr = image.GetHbitmap();//obtain the Hbitmap

            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap

            (

                ptr,

                IntPtr.Zero,

                Int32Rect.Empty,

                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions()

            );

            return bs;

        }


    }
}
