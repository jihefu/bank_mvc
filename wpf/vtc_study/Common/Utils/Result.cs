﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace VtcPositionDemo.Common.Utils
{

   




    public class ResultJObject
    {
        public int code { get; set; }

        public JObject data { get; set; }

        public string msg { get; set; }
    }


    public class ResultArray
    {
        public int code { get; set; }

        public JArray data { get; set; }

        public string msg { get; set; }
    }

    public class Result<T>
    {
        public int code { get; set; }

        public T data { get; set; }

        public string msg { get; set; }
    }



    public static class CodeEnum
    {
       public static int SUCCESS = 0;
        public static int ERR = -1;
    }
}
