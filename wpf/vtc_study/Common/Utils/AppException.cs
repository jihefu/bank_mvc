﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtcPositionDemo.Common.Utils
{
    class AppException : ApplicationException
    {

        public AppException(string message) : base(message)
        {
        }
    }
}
