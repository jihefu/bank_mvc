﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace VtcPositionDemo.Common.Utils
{
    /**
    * 配置类 
    **/
    public class Config
    {


        public static JObject devConfig = new JObject {
               new JProperty("crmHost", "https://actiondev.langjie.com"),
                new JProperty("crmHelpHost", "https://crmhelpdev.langjie.com"),
               new JProperty("plcmcontrollerHost", "https://plcmcontrollerdev.langjie.com"),
               new JProperty("vtcJsonFile", "Resources/2380045.vtc.json"),
              new JProperty("atsJsonFile", "Resources/wdw-v.move.ats.json"),

        };
        public static JObject prodConfig = new JObject {
              new JProperty("crmHost", "https://wx.langjie.com"),
              new JProperty("crmHelpHost", "https://crmhelp.langjie.com"),
              new JProperty("plcmcontrollerHost", "https://plcmcontroller.langjie.com"),
              new JProperty("vtcJsonFile", "Resources/2380045.vtc.json"),
              new JProperty("atsJsonFile", "Resources/y_very.ats.json"),
        };






        public static string GetValue(string key)
        {
            if (ConfigUtil.env == ENV.dev)
            {
                return (string)Config.devConfig.GetValue(key);
            }
            else
            {
                return (string)Config.prodConfig.GetValue(key);
            }
        }

        public static JObject GetConfig()
        {
            if (ConfigUtil.env == ENV.dev)
            {
                return devConfig;
            }
            else
            {
                return prodConfig;
            }


        }


    }
}

