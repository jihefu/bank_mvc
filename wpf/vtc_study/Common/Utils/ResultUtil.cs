﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace VtcPositionDemo.Common.Utils
{
    public static class ResultUtil
    {

        public  static string ErrorResultStr(int code,string msg)
        {
            var r = new Result<string>();
            r.code = code;
            r.msg = msg;

            string s= JsonConvert.SerializeObject(r);
            return s;
        }


    }
}
