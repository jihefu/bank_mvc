﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using VtcPositionDemo.Common.MvvmLight;

namespace VtcPositionDemo.Common.Utils
{
    public class ContextUtil
    {

        public static Dictionary<string, ViewModelBase> beanMap = new Dictionary<string, ViewModelBase>();
        public static ViewModelBase getBean(String beanName)
        {
            ViewModelBase bean = beanMap[beanName];
            return bean;
        }


        public static Dictionary<string, Page> beanPageMap = new Dictionary<string, Page>();
        public static Page getBeanPage(String beanName)
        {
            Page page = beanPageMap[beanName];
            return page;
        }



        public static Dictionary<string, Window> beanWindowMap = new Dictionary<string, Window>();
        public static Window getBeanWindow(String beanName)
        {
            Window win = beanWindowMap[beanName];
            return win;
        }



    }
}
