﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace VtcPositionDemo.Common.Utils
{




    public class ConfigUtil
    {
        /// <summary>
        /// 是用硬件还是仿真
        /// </summary>
        public static VtcHsType vtcHsType = VtcHsType.hard;
        /// <summary>
        /// 接口用本地还是远程
        /// </summary>
        public static ENV env = ENV.prod;
        /// <summary>
        /// 微信连还是桌面连
        /// </summary>
        public static SocketIoENV socketIoEnv = SocketIoENV.wx;

        //公共连接还是私有连接 1 共享 0 私有
        public static int SocketIoIsShare = 0;

        //socket 对应的应用名
        public static SocketIoApp socketioApp =  SocketIoApp.geren_skq;

        /**
         * 机器码
         */
        public static string socketioMachineCode = ConfigUtil.socketioApp + "01";


        public static string getVtcJson()
        {
            string vtcJsonStr= HttpUtils.Get("https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/play-jar/config/0.vtc.json");
            return vtcJsonStr;
        }

        public static string getAtsJson()
        {
            string atsJsonStr = HttpUtils.Get("https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/play-jar/config/code01.ats.json");
            return atsJsonStr;
        }


        public static string rootPath = "";

        public static string crmHost
        {
            get
            {
                return Config.GetValue("crmHost");
            }
        }


        public static string plcmcontrollerHost
        {
            get
            {
                return Config.GetValue("plcmcontrollerHost");
            }
        }

      
        public static string crmHelpHost
        {
            get
            {
                return Config.GetValue("crmHelpHost");
            }
        }


        public static string atsJsonFile
        {
            get
            {
                return Config.GetValue("atsJsonFile");
            }
        }

        public static string vtcJsonFile
        {
            get
            {
                return Config.GetValue("vtcJsonFile");
            }
        }


    }


    public enum VtcHsType
    {
        hard,
        soft
    }
    public enum ENV
    {
        dev,
        prod
    }

    public enum SocketIoENV
    {
        wx,
        local
    }

    public enum SocketIoApp
    {
        /**
         * 软件仿真
         */
        simu_skq,
        /**
         * 硬件仿真
         */
        hard_skq,
        /**
         * 个人手控器
         */
        geren_skq
    }
}   