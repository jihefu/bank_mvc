﻿using System;


using VtcPositionDemo.ViewModel;

namespace VtcPositionDemo.Viewmodel.Dto
{

    public class BtnColorEnum
    {
        public static string ENABLE = "red";
        public static string DISABLE = "white";
    }



    public class BtnStatus
    {

        public MainWindowState mainWindowState;

        public BtnStatus(MainWindowState windowState)
        {
            this.mainWindowState = windowState;
        }

        public string UP
        {
            get {
                return GetBtnColorByBtnName("UP");
            }
        }

        public string DOWN
        {
            get
            {
                return GetBtnColorByBtnName("DOWN");
            }
        }


        public string STOP
        {
            get
            {
                return GetBtnColorByBtnName("STOP");
            }
        }

        public string START
        {
            get
            {
                return GetBtnColorByBtnName("START");
            }
        }


        public string POSITION
        {
            get
            {
                return GetBtnColorByBtnName("POSITION");
            }
        }


        public string GetBtnColorByBtnName(String btnName)
        {
            switch (btnName)
            {
                case "UP": return mainWindowState == MainWindowState.UP ? BtnColorEnum.ENABLE : BtnColorEnum.DISABLE;
                case "DOWN": return mainWindowState == MainWindowState.DOWN ? BtnColorEnum.ENABLE : BtnColorEnum.DISABLE;
                case "STOP": return mainWindowState == MainWindowState.STOP ? BtnColorEnum.ENABLE : BtnColorEnum.DISABLE;
                case "START": return mainWindowState == MainWindowState.START ? BtnColorEnum.ENABLE : BtnColorEnum.DISABLE;
                case "POSITION": return mainWindowState == MainWindowState.POSITION ? BtnColorEnum.ENABLE : BtnColorEnum.DISABLE;
            }
            return BtnColorEnum.DISABLE;
        }


    }
}
