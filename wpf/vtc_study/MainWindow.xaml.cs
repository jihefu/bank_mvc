﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using VtcPositionDemo.Common.Models;
using VtcPositionDemo.Common.Utils;
using VtcPositionDemo.Driver;
using VtcPositionDemo.ViewModel;
using VtcPositionDemo.Views;


namespace VtcPositionDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        public static MainWindow instance = null;
        public static MainWindowViewModel gmodel = null;

        public MainWindowViewModel vm { set; get; }



        public MainWindow()
        {
            InitializeComponent();
            this.ContentRendered += WindowThd_ContentRendered;
            MainWindowViewModel model = new MainWindowViewModel(this);
            this.vm = model;
            this.DataContext = model;
            gmodel = model;
            MainWindow.instance = this;

          
        }



        void WindowThd_ContentRendered(object sender, EventArgs e)

        {

            gmodel.RunStartHandel("");

        }


        protected override void OnClosed(EventArgs e)
        {
            Environment.Exit(0);
        }

   
    }
}
