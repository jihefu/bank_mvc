﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MyFIFO;

using Telerik.Windows.Data;

using VtcPositionDemo.Driver;
using VtcPositionDemo.Models;
using VtcPositionDemo.ViewModel;

namespace VtcPositionDemo.Views
{
    /// <summary>
    /// UserControlWavw.xaml 的交互逻辑
    /// </summary>
    public partial class UserControlWavw : UserControl
    {



        public RadObservableCollection<TData> m_liveList = new RadObservableCollection<TData>();


        private double m_maxVal = 0;
      

        public MainWindowViewModel vm { set; get; }

    
        public MyFifo<ChannelTransmitMsg> chanelMsgWaveQueue = new MyFifo<ChannelTransmitMsg>((UInt32)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ChannelTransmitMsg))), 500);

        private Timer m_timer = new Timer();


        public UserControlWavw()
        {
            InitializeComponent();
            this.m_liveList.Add(new TData(0, 0));
   
            this.motorChartData_b.ItemsSource = this.m_liveList;
            this.m_timer.Interval = 200;
            this.m_timer.Elapsed += M_timer_Tick;
            m_timer.Start();
        }



        private void M_timer_Tick(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                List<ChannelTransmitMsg> channelTransmitMsgs = new List<ChannelTransmitMsg>();
                ChannelTransmitMsg chanelMsg2 = new ChannelTransmitMsg(0, 0, 0, 0);
                int c = this.chanelMsgWaveQueue.PopUnit(ref channelTransmitMsgs, 20);
                if (c > 0)
                {
                    chanelMsg2 = channelTransmitMsgs[c - 1];
                }
                if (chanelMsg2.ms != 0)
                {
                    //chanelMsg2.ms = chanelMsg2.ms % 10001;
                    double xval =(double)chanelMsg2.ms / 1000;
                    double yval = Data.Data_Dvb2Float(chanelMsg2.dispAd);
                    TData td = new TData(xval, yval);
                    Console.WriteLine(xval);
                    this.m_liveList.Add(td);
                    DataRow dr = MainWindow.gmodel.dataTableCsv.NewRow();

                    if (MainWindow.gmodel.isSavaLog)
                    {
                        dr["ms"] = chanelMsg2.ms;
                        dr["disp"] = yval;
                        MainWindow.gmodel.dataTableCsv.Rows.Add(dr);
                    }

                    this.motorChartHAxis_b.Maximum = Math.Min(0, Math.Abs(xval - 5));
                    this.motorChartHAxis_b.Maximum = Math.Max(10, Math.Abs(xval + 5));
                    m_maxVal = Math.Max(m_maxVal, yval);
                    this.motorChartVAxis_b.Maximum= Math.Max(30, Math.Abs(m_maxVal + 10));
                }
            }
          );
        }

      
    }
}
