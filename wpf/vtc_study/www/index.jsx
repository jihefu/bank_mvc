class VtcMethedList extends React.Component {

    constructor(props) {
        super(props);
        this.keyWordsRefContent = "";
        M.Component.ModbusSourceList = this;
    }

    state = {
        dataSource: [],
        lineTabData: [],
        pagination: {   //搜索参数
            current: 1,
            pageSize: 30,
            keywords: '',
            order: '',
            filter: {},
            total: 0,
            showSizeChanger: true,
            pageSizeOptions: ['1', '50', '100', '500'],
            onShowSizeChange: (current, pageSize) => {
                const {pagination} = this.state;
                pagination.pageSize = pageSize;
                this.setState({
                    pagination
                });
            },
        },
        selectedRowKeys: [],
        selectedRows: [],
    }

    columns = [
        {
            title: '方法',
            render: (text, row, index) => {
                return <span>{row}</span>
            }
        },{
            title: '操作',
            key: 'operation',
            fixed: 'right',
            width: 300,
            render: (text, row, index) => {
                return this.actionRender(text, row, index);
            },
        }
    ];


    async componentDidMount() {
        this.fetch();
    }

    actionRender(text, row, index) {
        return <p>
            <a>执行</a>
        </p>;
    }

    //表格点击
    async handleTableClick(record, index, e) {

        if(e.target.innerHTML=='执行'){
            alert(e.target.innerHTML)

        }
    }

    async fetch() {
        let keywords= this.state.pagination.keywords;
        let methedList = await MIO.vtcMethedList({keywords})
        this.setState({dataSource: methedList})
    }


    handleTableChange(pagination) {
        this.state.pagination.current = pagination.current;
        this.setState({
            pagination: this.state.pagination
        }, () => {
            console.log(this.state.pagination)
        });
    }

    rowSelection = () => {
        const {selectedRowKeys} = this.state;
        return {
            selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    selectedRowKeys,
                });
            },
            getCheckboxProps: record => ({
                name: record.id + "",
            }),
            onSelect: (record, selected, selectedRows, nativeEvent) => {
                let {selectedRows: globalSelectedRows} = this.state;
                if (selected) {
                    if (globalSelectedRows.length === 0) {
                        globalSelectedRows.push(record);
                    } else {
                        for (let i = 0; i < globalSelectedRows.length; i++) {
                            const items = globalSelectedRows[i];
                            if (items.id != record.id && i === globalSelectedRows.length - 1) {
                                globalSelectedRows.push(record);
                            }
                        }
                    }
                } else {
                    globalSelectedRows = globalSelectedRows.filter(items => items.id != record.id);
                }
                this.setState({
                    selectedRows: globalSelectedRows,
                });
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
                let {selectedRows: globalSelectedRows, selectedRowKeys} = this.state;
                const keyArr = changeRows.map(items => items.id);
                if (selected) {
                    selectedRowKeys = [...selectedRowKeys, ...keyArr];
                    globalSelectedRows = [...globalSelectedRows, ...changeRows];
                } else {
                    selectedRowKeys = selectedRowKeys.filter(key => keyArr.indexOf(key) === -1);
                    globalSelectedRows = globalSelectedRows.filter(items => keyArr.indexOf(items.id) === -1);
                }
                this.setState({
                    selectedRowKeys,
                    selectedRows: globalSelectedRows,
                });
            }
        }
    }


    handleSearch(v) {
        let pagination = this.state.pagination;
        pagination.keywords = this.keywordsRef.state.value;
        pagination.current = 1;
        this.setState({
            pagination,
            selectedRowKeys: [],
            selectedRows: [],
        }, () => {
            this.fetch()
        });
    }

    render() {
        return (
            <div>
                <Input style={{width: 600}} ref={el => this.keywordsRef = el}></Input> <Button
                onClick={this.handleSearch.bind(this)}>搜索</Button>
                <Table
                    rowKey={(record, index) => record.id}
                    dataSource={this.state.dataSource}
                    columns={this.columns}
                    pagination={this.state.pagination}
                    onChange={this.handleTableChange.bind(this)}
                    rowSelection={this.rowSelection()}
                    onRow={(record, index) => ({
                        onClick: (e) => this.handleTableClick(record, index, e)
                    })}
                />
            </div>

        )

    }

}
