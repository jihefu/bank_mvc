﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VirtualBank.Common.Entity;
using VirtualBank.Common.Events;
using VirtualBank.Views;

namespace VirtualBank
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private IEventAggregator _eventAggregator;
        private Bank _bank;
        private string AccountID;

        public MainWindow()
        {
            InitializeComponent();
            _eventAggregator = new EventAggregator();
            _eventAggregator.GetEvent<LoginResultEvent>().Subscribe(Login);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            AccountID = tBox_UserName.Text;
            LoginMsg msg = new LoginMsg(AccountID, passwordBox.Password);            
            _eventAggregator.GetEvent<LoginEvent>().Publish(msg);            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _bank=new Bank(_eventAggregator);
        }

        private void Login(bool r)
        {
            if(r)
            {
                Window win = new BankServerWin(AccountID, _eventAggregator);
                win.Show();
            }
            else
            {
                MessageBox.Show("登陆失败，该账户未开户");
            }
        }
    }
}
