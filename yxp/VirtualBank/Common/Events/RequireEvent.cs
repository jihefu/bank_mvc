﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualBank.Common.Entity;

namespace VirtualBank.Common.Events
{
    public class RequireEvent:PubSubEvent<RequireMsg>
    {
    }
}
