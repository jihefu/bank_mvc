﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBank.Common.Entity
{
    public class AccountInfoMsg
    {
        public string Id;
        public int Dallor;
        public BalanceState BalanceState;
        public AccountState AccountState;

        public AccountInfoMsg()
        {

        }

        public AccountInfoMsg(string id, int dallor, BalanceState balanceState, AccountState accountState)
        {
            Id = id;
            Dallor = dallor;
            BalanceState = balanceState;
            AccountState = accountState;
        }

        public void SetValue(string id, int dallor, BalanceState balanceState, AccountState accountState)
        {
            Id = id;
            Dallor = dallor;
            BalanceState = balanceState;
            AccountState = accountState;
        }
    }
}
