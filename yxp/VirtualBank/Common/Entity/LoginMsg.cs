﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBank.Common.Entity
{
    public class LoginMsg
    {
        public string ID;
        public string Password;

        public LoginMsg(string iD, string password)
        {
            ID = iD;
            Password = password;
        }

        public LoginMsg()
        {

        }
    }
}
