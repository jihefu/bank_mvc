﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBank.Common.Entity
{
    public class RequireMsg
    {
        public string Id;
        public ServiceMsg Msg;

        public RequireMsg(string id, ServiceMsg msg)
        {
            Id = id;
            Msg = msg;
        }

        public RequireMsg()
        {

        }
    }
}
