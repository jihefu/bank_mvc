﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBank.Common.Entity
{
    public enum ServiceType
    {
        REQ_DRAW,
        REQ_DEPO,
        REQ_QUIT
    }

    public class ServiceMsg
    {
        public ServiceType ServiceCode;
        public int Value;

        public ServiceMsg()
        {

        }

        public ServiceMsg(ServiceType serverCode, int value)
        {
            ServiceCode = serverCode;
            Value = value;
        }

        public void SetValue(ServiceType serverCode, int value)
        {
            this.ServiceCode = serverCode;
            this.Value = value;
        }
    }
}
