﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VirtualBank.Common.Events;

namespace VirtualBank.Views
{
    /// <summary>
    /// BankServerWin.xaml 的交互逻辑
    /// </summary>
    public partial class BankServerWin : Window
    {
        IEventAggregator eventAggregator;
        string Id;
        Window window;
        public BankServerWin(string id,IEventAggregator ea)
        {
            InitializeComponent();
            Id = id;
            eventAggregator = ea;
            this.Title = Id;
            eventAggregator.GetEvent<BankCloseEvent>().Subscribe(close, ThreadOption.UIThread,false, a => a.Equals(Id));
            window = new ATMWin(Id, eventAggregator);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
            window.Show();
        }

        private void close(string arg)
        {
            this.Close();
            window.Close();
        }
    }
}
