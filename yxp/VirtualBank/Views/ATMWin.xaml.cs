﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using VirtualBank.ViewModels;

namespace VirtualBank.Views
{
    /// <summary>
    /// LongPressButtonWin.xaml 的交互逻辑
    /// </summary>
    public partial class ATMWin : Window
    {
        public ATMWin(string id,IEventAggregator ea)
        {
            InitializeComponent();
            this.DataContext = new ATMWinViewModel(id,ea);                      
        }
    }
}
