﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using VirtualBank.Common.Entity;
using System.Windows.Interop;
using System.Security.Principal;
using Prism.Mvvm;
using Prism.Events;
using VirtualBank.Common.Events;

namespace VirtualBank.ViewModels
{
    public class ATMWinViewModel : BindableBase
    {
        public ATMWinViewModel(string id,IEventAggregator ea)
        {
            ClickCommand = new DelegateCommand<string>(Click);
            DoubleClickCommand = new DelegateCommand<string>(DoubleClick);
            LongPressCommand = new DelegateCommand<string>(LongPress);
            LongPressReleaseCommand = new DelegateCommand<string>(LongPressRelease);
            eventAggregator = ea;
            AccountId = id;
            Init();
        }

        private void Init()
        {
            eventAggregator.GetEvent<AccountInfoEvent>()
                .Subscribe(AccountMsgHandler, ThreadOption.PublisherThread, false, a => a.Id.Equals(AccountId));
            eventAggregator.GetEvent<QueryAccountEvent>().Publish(AccountId);
        }

        private IEventAggregator eventAggregator;
        private string m_AccountId;
        public string AccountId
        {
            get { return m_AccountId; }
            set { SetProperty(ref m_AccountId, value); }
        }

        private int m_count = 0;

        private int m_Balance;
        public int Balance
        {
            get { return m_Balance; }
            set { SetProperty(ref m_Balance, value); }
        }

        private bool m_IsDrawEnable;
        public bool IsDrawEnable
        {
            get { return m_IsDrawEnable; }
            set { SetProperty(ref m_IsDrawEnable, value); }
        }

        private bool m_IsDepoEnable;
        public bool IsDepoEnable
        {
            get { return m_IsDepoEnable; }
            set { SetProperty(ref m_IsDepoEnable, value); }
        }

        public DelegateCommand<string> ClickCommand { get; set; }
        private void Click(string args)
        {
            OneTimeHandler(args, 1);
            ServiceMsg msg = new ServiceMsg(ServiceType.REQ_QUIT, 0);
            eventAggregator.GetEvent<RequireEvent>().Publish(new RequireMsg(AccountId, msg));
            
        }

        public DelegateCommand<string> DoubleClickCommand { get; set; }
        private void DoubleClick(string args)
        {
            OneTimeHandler(args, 10);
            ServiceMsg msg = new ServiceMsg(ServiceType.REQ_QUIT, 0);
            eventAggregator.GetEvent<RequireEvent>().Publish(new RequireMsg(AccountId, msg));
        }

        public DelegateCommand<string> LongPressCommand { get; set; }
        private void LongPress(string args)
        {
            m_count++;
            ServiceMsg msg = new ServiceMsg();
            switch (args)
            {
                case "+":
                    msg.SetValue(ServiceType.REQ_DEPO, m_count);
                    break;
                case "-":
                    msg.SetValue(ServiceType.REQ_DRAW, m_count);
                    break;
            }
            eventAggregator.GetEvent<RequireEvent>().Publish(new RequireMsg(AccountId, msg));
        }

        public DelegateCommand<string> LongPressReleaseCommand { get; set; }
        private void LongPressRelease(string args)
        {
            ServiceMsg msg = new ServiceMsg(ServiceType.REQ_QUIT, 0);
            eventAggregator.GetEvent<RequireEvent>().Publish(new RequireMsg(AccountId, msg));
            m_count = 0;
        }

        private void OneTimeHandler(string args, int n)
        {
            ServiceMsg msg = new ServiceMsg();
            switch (args)
            {
                case "+":
                    msg.SetValue(ServiceType.REQ_DEPO, n);
                    break;
                case "-":
                    msg.SetValue(ServiceType.REQ_DRAW, n);
                    break;
            }
            eventAggregator.GetEvent<RequireEvent>().Publish(new RequireMsg(AccountId, msg));
        }

        private void AccountMsgHandler(AccountInfoMsg msg)
        {
            Balance = msg.Dallor;
            BalanceChange(msg.BalanceState);
        }

        private void BalanceChange(BalanceState state)
        {
            switch(state)
            {
                case BalanceState.FULL:
                    IsDepoEnable = false;
                    IsDrawEnable = true;
                    break;
                case BalanceState.EMPTY:
                    IsDepoEnable = true;
                    IsDrawEnable = false;
                    break;
                case BalanceState.NORMAL:
                    IsDepoEnable = true;
                    IsDrawEnable = true;
                    break;
            }
        }
    }
}
