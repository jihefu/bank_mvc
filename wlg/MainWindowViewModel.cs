﻿using FixedStorageBank.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedStorageBank.ViewModels
{
    public class MainWindowViewModel:BindableBase
    {
		public int Amount = 0;//单次存入的金额
		private int money;
    
        Account account = new Account();
        public MainWindowViewModel()
		{
            
            ButtonCommand = new DelegateCommand<string>(Buttonoperation);
            RadioButtonCommand = new DelegateCommand<string>(RadioButtonoperation);
        }
	
		public int Money
		{
			get { return money; }
			set
			{
				SetProperty(ref money, value);
			}

		}
        public DelegateCommand<string> ButtonCommand { get; set; }
		private void Buttonoperation(string arg)
		{
			if (arg == "deposit")
			{
				Money=account.Deposit( Amount);
			
			}
			else if (arg == "withdraw")
			{
				Money=account.WithDraw(Amount);

			}

		}

        public DelegateCommand<string> RadioButtonCommand { get; set; }
		private void RadioButtonoperation(string arv)
		{
			switch (arv)
			{
				case "1":
					Amount = 1;
					break;
                case "5":
					Amount = 5;
                    break;
                case "10":
					Amount = 10;
                    break;
            }
       
		}


    }	
}
